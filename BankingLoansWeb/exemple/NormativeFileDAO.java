/*
 * NormativeFileDAO.java
 *
 * Copyright (c) 1999-2010 Software Solutions Group. All rights reserved.
 *
 * This software is the confidential and proprietary information of Software Solutions Group. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with Software Solutions Group.
 *      
 * Version Info:
 * 14.02.2010 MST Basic version.
 */

package ro.ssg.ulex.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import ro.ssg.ulex.dao.def.INormativeFileDAO;
import ro.ssg.ulex.entity.Category;
import ro.ssg.ulex.entity.IssuingAuthority;
import ro.ssg.ulex.entity.NormativeFile;
import ro.ssg.ulex.entity.NormativeFileState;
import ro.ssg.ulex.entity.NormativeFileType;
import ro.ssg.ulex.services.impl.NormativeFileTypeServices;

public class NormativeFileDAO implements INormativeFileDAO {

	public NormativeFileDAO() {

	}

	/**
	 * Implements the delete operation for Normative objects
	 */
	public void delete(Long id) {

		boolean success = false;

		Session session = null;
		Transaction tx = null;

		try {
			SessionFactory sessionFactory = new Configuration().configure()
					.buildSessionFactory();
			session = sessionFactory.openSession();

			tx = session.beginTransaction();
			String hql = "delete from NormativeFile com where com.id =" + id;
			Query query = session.createQuery(hql);
			int row = query.executeUpdate();
			if (row == 0) {
				System.out
						.println("Deleted operation failed due an unknown cause!");
			} else {
				System.out.println("Deleted Row: " + row);
			}

			tx.commit();

		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception e1) {
					System.out.println(e.getMessage());
				}
			}
		} finally {
			session.flush();
			session.close();

		}

	}

	/**
	 * Return the list with all the Normative objects from database
	 */
	public ArrayList<NormativeFile> findAll() {
		ArrayList<NormativeFile> normativeFiles = null;
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {
			
			String SQL_QUERY = "from NormativeFile com";
			Query query = session.createQuery(SQL_QUERY);

			normativeFiles = (ArrayList<NormativeFile>) query.list();

			
		} catch (HibernateException e1) {
			System.out.println(e1.getMessage());
		} finally {
			session.close();
		}
		return normativeFiles;
	}

	/**
     * 
     */
	public ArrayList<NormativeFile> findByDate(Date startDate, Date finalDate) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Return the Normative object for a specified id
	 */
	public NormativeFile findById(Long id) {

		NormativeFile normFile = new NormativeFile();
		Session session = null;

		try {
			SessionFactory sessionFactory = new Configuration().configure()
					.buildSessionFactory();
			session = sessionFactory.openSession();

			String SQL_QUERY = "from NormativeFile com where com.id=" + id;
			Query query = session.createQuery(SQL_QUERY);

			for (Iterator it = query.iterate(); it.hasNext();) {
				normFile = (NormativeFile) it.next();
			}
			System.out.println("Am gasit " + normFile.getTitluAct());
			session.close();
		} catch (HibernateException e1) {
			System.out.println(e1.getMessage());
		} finally {
		}
		return normFile;
	}

	/**
	 * Return the Normative file for a specified query
	 */
	public ArrayList<NormativeFile> simpleSearchQuery(String queryString) {
	    
            ArrayList<NormativeFile> normativeFiles = null;
            Session session = HibernateUtil.getSessionFactory().openSession();

            try {
                    
                    String SQL_QUERY = queryString;
                    //from NormativeFile nf where nf.nrAct='167' and nf.tipAct.numeTip='Lege' and nf.emitentAct.idEmitent='276'
                    Query query = session.createQuery(SQL_QUERY);

                    normativeFiles = (ArrayList<NormativeFile>) query.list();
                    System.out.println("RESULTS SIZE " + normativeFiles.size());

                    
            } catch (HibernateException e1) {
                    System.out.println(e1.getMessage());
            } finally {
                    session.close();
            }
            return normativeFiles;
    }

	/**
	 * Return the Normative file for a specified name
	 */
	public ArrayList<NormativeFile> findByName(String attribute) {

		ArrayList<NormativeFile> normFile = null;
		Session session = null;

		try {
			SessionFactory sessionFactory = new Configuration().configure()
					.buildSessionFactory();
			session = sessionFactory.openSession();

			String SQL_QUERY = "from NormativeFile com where com.titluAct = '"
					+ attribute + "'";
			Query query = session.createQuery(SQL_QUERY);
			normFile = (ArrayList<NormativeFile>) query.list();

			session.close();
		} catch (HibernateException e1) {
			System.out.println(e1.getMessage());
		} finally {
		}
		return normFile;
	}
	
	public ArrayList<NormativeFile> findByNumeAct(String attribute) {

		ArrayList<NormativeFile> normFile = null;
		Session session = null;

		try {
			SessionFactory sessionFactory = new Configuration().configure()
					.buildSessionFactory();
			session = sessionFactory.openSession();

			String SQL_QUERY = "from NormativeFile com where com.numeAct=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0, attribute);
			normFile = (ArrayList<NormativeFile>) query.list();

			session.close();
		} catch (HibernateException e1) {
			System.out.println(e1.getMessage());
		} finally {
		}
		return normFile;
	}

	/**
	 * Save the normativeFile object into database
	 */
	public Long save(NormativeFile normativeFile) {

		boolean success = false;

		System.out.println("Inainte de add");
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {

			transaction = session.beginTransaction();

			NormativeFile nf1 = new NormativeFile();
			NormativeFile nf2 = new NormativeFile();
			// NormativeFileType nType = new NormativeFileType();
			// nType.setNumeTip("Test Lates");

			ArrayList<NormativeFileType> nType = new NormativeFileTypeDAO()
					.findByName("Testdd");

			nf1.setNumeAct("TEST4");
			nf1.setTipAct(nType.get(0));
			// nf1.setTipAct(nType);

			nf2.setNumeAct("TEST5");
			nf2.setTipAct(nType.get(0));
			// nf2.setTipAct(nType);

			session.save(nf1);
			session.save(nf2);

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		System.out.println("Dupa de add");
		return normativeFile.getIdAct();
	}

	public ArrayList<NormativeFile> findByType(String type) {

		ArrayList<NormativeFile> normFile = null;
		Session session = HibernateUtil.getSessionFactory().openSession();

		try {

			String SQL_QUERY = "from NormativeFile com where com.tipAct.numeTip like '%" + type + "%'";
			Query query = session.createQuery(SQL_QUERY);
			//query.setParameter(0, type);
			normFile = (ArrayList<NormativeFile>) query.list();

			session.close();
		} catch (HibernateException e1) {
			System.out.println(e1.getMessage());
		} finally {
		}
		return normFile;
	}

	
	public void update(NormativeFile object) {

        boolean success = false;
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            session.update(object);
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();
        }
    }
	
	public void create(NormativeFile object) {

//        boolean success = false;

        System.out.println("Inainte de adaugare ");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        
        try {

            transaction = session.beginTransaction();
            
            NormativeFileType normativeType = new NormativeFileType();
            String normativeTypeName = object.getTipAct().getNumeTip();
            
            ArrayList<NormativeFileType> nType = new NormativeFileTypeDAO().findByName(normativeTypeName);
            if (nType.size()>0){
                normativeType = nType.get(0);
                object.setTipAct(normativeType);
            } 
            
            IssuingAuthority normativeEmitent = new IssuingAuthority();
            String normativeEmitentName = object.getEmitentAct().getNumeEmitent();
            
            ArrayList<IssuingAuthority> nEmitent = new IssuingAuthorityDAO().findByName(normativeEmitentName);
            if (nEmitent.size()>0){
                normativeEmitent = nEmitent.get(0);
                object.setEmitentAct(normativeEmitent);
            }
            
            Category normativeCat = new Category();
            String normativeCatName = object.getIncadrare().getNumeIncadrare();
            
            ArrayList<Category> nCat = new CategoryDAO().findByName(normativeCatName);
            if (nCat.size()>0){
            	normativeCat = nCat.get(0);
                object.setIncadrare(normativeCat);
            }

            session.save(object);
            
            transaction.commit();
            } catch (HibernateException e) {
                transaction.rollback();
                e.printStackTrace();
            } finally {
                session.close();
            }
        System.out.println("Dupa de add");
       
    }

}
