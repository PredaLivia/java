package ro.star.archiver.beans;

import ro.star.archiver.entity.Author;

public class MostActiveBean {

    private Author author = new Author();
    private int tweetsNo;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getTweetsNo() {
        return tweetsNo;
    }

    public void setTweetsNo(int tweetsNo) {
        this.tweetsNo = tweetsNo;
    }

}
