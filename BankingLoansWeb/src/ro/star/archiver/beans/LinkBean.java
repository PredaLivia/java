package ro.star.archiver.beans;

import java.util.ArrayList;

import ro.star.archiver.entity.Tweet;

public class LinkBean {

    private ArrayList<Tweet> tweetList = new ArrayList<Tweet>();
    private String link;
    private int number;

    public ArrayList<Tweet> getTweetList() {
        return tweetList;
    }
    public void setTweetList(ArrayList<Tweet> tweetList) {
        this.tweetList = tweetList;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
}
