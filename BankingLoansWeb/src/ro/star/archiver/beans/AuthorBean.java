package ro.star.archiver.beans;

public class AuthorBean {

	private String username = new String();
	private Long userID;
	private String countTweets = new String();
	
	public AuthorBean (){
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getCountTweets() {
		return countTweets;
	}

	public void setCountTweets(String countTweets) {
		this.countTweets = countTweets;
	}
	
	
	
}
