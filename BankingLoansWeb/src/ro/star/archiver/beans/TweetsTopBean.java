package ro.star.archiver.beans;

public class TweetsTopBean {
    private String title = new String();
    private String description = new String();
    private String image = new String();
    private String internalID = new String();
    private int retweetsNo;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getInternalID() {
        return internalID;
    }

    public void setInternalID(String internalID) {
        this.internalID = internalID;
    }

    public int getRetweetsNo() {
        return retweetsNo;
    }

    public void setRetweetsNo(int retweetsNo) {
        this.retweetsNo = retweetsNo;
    }

}
