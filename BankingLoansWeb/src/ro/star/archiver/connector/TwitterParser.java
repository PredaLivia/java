package ro.star.archiver.connector;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.validator.routines.UrlValidator;

import parsing.Parser;
import parsing.ParsingEvents;
import ro.star.archiver.dao.AuthorDAO;
import ro.star.archiver.dao.TagDAO;
import ro.star.archiver.dao.TweetDAO;
import ro.star.archiver.dao.TweetLinkDAO;
import ro.star.archiver.dao.TweetRetweetDAO;
import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.Tag;
import ro.star.archiver.entity.Tweet;
import ro.star.archiver.entity.TweetLink;
import ro.star.archiver.entity.TweetRetweet;

public class TwitterParser implements ParsingEvents {

    LinkedHashSet<String> tweetList = new LinkedHashSet<String>();
    LinkedHashSet<String> userList = new LinkedHashSet<String>();
    private LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
    private String tagTweetId = null;
    private String mainTag = null;

    public static final String EXPAND = "Expand";
    public static final String VIEW_MEDIA = "View media";
    public static final String VIEW_VIDEO = "View video";
    public static final String VIEW_SUMMARY = "View summary";
    public static final String VIEW_CONVERSATION = "View conversation";
    public static final String VIEW_PHOTO = "View photo";
    public static final String VIEW_ALBUM = "View album";

    // private int contor = 0;

    public TwitterParser() {

    }

    public void parseTag(String tag) throws Exception {
        if (tag.startsWith("#")) {
            mainTag = tag.toUpperCase();
        } else {
            mainTag = "#" + tag.toUpperCase();
        }

        TagDAO tagDao = new TagDAO();
        Tag tagObj = tagDao.findByTagName(mainTag);

        if (tagObj == null) {
            tagObj = new Tag();
            tagObj.setName(mainTag);
            tagDao.save(tagObj);
        }

        String page = "";
        String url = "https://twitter.com/search/realtime?q=%23" + mainTag.replace("#", "") + "&src=typd";
        try {
            page = downloadPage(url);
        } catch (Exception e) {
            System.out.println("Exception reading page: " + e.getMessage());
            return;
        }

        // System.out.println(page);
        Parser parser = new Parser();
        if (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            tagTweetId = parser.extract("-", "\"", page, actionFirst.SKIP, actionLast.SKIP);
        } else {
            System.out.println("No start tweet ID found for tag '" + tag + "'");
        }

        getTagTweets(mainTag, tagTweetId);
    }

    public void getTagTweets(String tag, String tweetStartId) throws Exception {
        tag = tag.replace("#", "");

        while (tagTweetId != null) {
            String previousTweetId = tagTweetId;
            System.out.println("Se intra pentru " + tagTweetId);
            String pageSource = getTagNextPage(tag, tagTweetId);
            // System.out.println(pageSource);
            // writeInFile("d://TweetFiles_10.12/" + tag + "_" + tagTweetId +
            // ".txt", pageSource);

            Set<String> v = new LinkedHashSet<String>();
            v = parsePage(pageSource);
            tweetList.addAll(v);
            if (previousTweetId.equalsIgnoreCase(tagTweetId)) {
                tagTweetId = null;
            }
        }

        System.out.println("Parsed tweets: " + tweetList.size());
    }

    public void parseTagDeep(String tag) throws Exception {
        if (tag.startsWith("#")) {
            mainTag = tag.toUpperCase();
        } else {
            mainTag = "#" + tag.toUpperCase();
        }

        TagDAO tagDao = new TagDAO();
        Tag tagObj = tagDao.findByTagName(mainTag);

        if (tagObj == null) {
            tagObj = new Tag();
            tagObj.setName(mainTag);
            tagDao.save(tagObj);
        }

        String page = "";
        String url = "https://twitter.com/search/realtime?q=%23" + mainTag.replace("#", "") + "&src=typd";
        try {
            page = downloadPage(url);
        } catch (Exception e) {
            System.out.println("Exception reading page: " + e.getMessage());
            return;
        }

        // System.out.println(page);
        Parser parser = new Parser();
        if (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            tagTweetId = parser.extract("-", "\"", page, actionFirst.SKIP, actionLast.SKIP);
        } else {
            System.out.println("No start tweet ID found for tag '" + tag + "'");
        }

        getTagUsers(mainTag);
    }

    public void getTagUsers(String tag) throws Exception {
        System.out.println("Parsare utilizatori pentru tag-ul " + tag);
        tag = tag.replace("#", "");

        while (tagTweetId != null) {
            String previousTweetId = tagTweetId;
            System.out.println("Se intra pentru " + tagTweetId);
            String pageSource = getTagNextPage(tag, tagTweetId);
            // System.out.println(pageSource);
            // writeInFile("d://TweetFiles_10.12/" + tag + "_" + tagTweetId +
            // ".txt", pageSource);

            Set<String> v = new LinkedHashSet<String>();
            v = parseTagPage(pageSource);
            userList.addAll(v);
            if (previousTweetId.equalsIgnoreCase(tagTweetId)) {
                tagTweetId = null;
            }

        }
        System.out.println("Numar useri: " + userList.size());
        System.out.println(userList);

        Iterator<String> iterator = userList.iterator();

        while (iterator.hasNext()) {
            parseUser(iterator.next());
        }
    }

    public void getTagUsersFromFile(String dirName) {
        mainTag = "#MMTM12";

        TagDAO tagDao = new TagDAO();
        Tag tagObj = tagDao.findByTagName(mainTag);

        if (tagObj == null) {
            tagObj = new Tag();
            tagObj.setName(mainTag);
            tagDao.save(tagObj);
        }

        File dir = new File(dirName);

        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".txt");
            }
        });

        LinkedHashSet<String> users = new LinkedHashSet<String>();

        System.out.println("Number of files: " + files.length);
        System.out.println("Processing file " + files[0].getAbsolutePath());

        for (int i = 0; i < files.length; i++) {
            System.out.println("Processing file " + files[0].getAbsolutePath());
            try {
                String fileContent = readFile(files[i].getAbsolutePath());
                Set<String> v = new LinkedHashSet<String>();
                v = parseTagPage(fileContent);
                users.addAll(v);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Numar useri: " + users.size());
        System.out.println(users);

        Iterator<String> iterator = users.iterator();

        while (iterator.hasNext()) {
            try {
                String user = iterator.next();
                parseUser(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        reader.close();

        return stringBuilder.toString();
    }

    public String getTagNextPage(String tag, String tweetID) throws Exception {

        String pageSource = new String();
        String httpsURL = "https://twitter.com/i/search/timeline?src=typd&type=recent&include_available_features=1&include_entities=1&max_id="
                + tweetID + "&q=" + tag;

        URL myurl = new URL(httpsURL);
        HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        // String header = new String();

        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            pageSource += inputLine;
        }

        return pageSource;
    }

    public Set<String> parseTagPage(String page) {
        Parser parser = new Parser();

        Set<String> v = new LinkedHashSet<String>();

        while (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            tagTweetId = parser.extract("-", "\\\"", page, actionFirst.SKIP, actionLast.SKIP);
            String userName = parser.extract("data-screen-name=\\\"", "\\\"", page, actionFirst.SKIP, actionLast.SKIP);

            // System.out.println("i " + i + ":" + userName);

            v.add(userName);
        }
        System.out.println(v.size() + " users: " + v);
        return v;
    }

    public void parseUser(String user) throws Exception {
        System.out.println("Parsare tweet-uri pentru utilizatorul " + user);
        String page = "";
        String firstTweet = null;
        String url = "https://twitter.com/" + user;
        try {
            page = downloadPage(url);
        } catch (Exception e) {
            System.out.println("Exception reading page: " + e.getMessage());
            return;
        }

        Parser parser = new Parser();
        if (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            firstTweet = parser.extract("-", "\"", page, actionFirst.SKIP, actionLast.SKIP);
        } else {
            System.out.println("No start tweet found");
        }

        getUserTweets(user, firstTweet);
    }

    public void getUserTweets(String user, String tweetStart) throws Exception {

        // String tweetStart = "266609304793128960";
        // pentru teste
        // String tweetStart = "225507546641858560";
        // tweetStart = "268030727755542528";

        while (tweetStart != null) {

            System.out.println("Se intra pentru " + tweetStart + "###########################");
            String pageSource = getUserNextPage(user, tweetStart);
            // System.out.println(pageSource);
            // writeInFile("d://TweetFilesTest/" + tweetStart + ".txt",
            // pageSource);

            Set<String> v = new LinkedHashSet<String>();
            v = parsePage(pageSource);

            tweetList.addAll(v);
            if (v.size() == 20) {
                tweetStart = (String) v.toArray()[v.size() - 1];
            } else {
                tweetStart = null;
            }

        }
        System.out.println("Numar twetturi: " + tweetList.size());
        System.out.println("Numar twetturi map: " + map.size());
        System.out.println(map.toString());

    }

    public String getUserNextPage(String user, String tweetID) throws Exception {

        String pageSource = new String();
        // String httpsURL =
        // "https://twitter.com/i/search/timeline?src=typd&type=recent&include_available_features=1&include_entities=1&max_id="+tweetID+"&q=mmtm12";
        String httpsURL = "https://twitter.com/i/profiles/show/" + user
                + "/timeline/with_replies?include_available_features=1&include_entities=1&max_id=" + tweetID;

        URL myurl = new URL(httpsURL);
        HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        // String header = new String();

        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            pageSource += inputLine;
        }

        return pageSource;
    }

    public Set<String> parsePage(String page) {
        Parser parser = new Parser();

        Set<String> v = new LinkedHashSet<String>();
        int position = 0;
        while (parser.seek("class=\\\"js-stream-item stream-item stream-item expanding-stream-item\\\"", page,
                direction.FORWARD, action.NOTHING) != -1) {
            position++;
            String tweetSection = parser.extract("stream-item-tweet", "expanded-content js-tweet-details-dropdown",
                    page, actionFirst.SKIP, actionLast.SKIP);
            // System.out.println(tweetSection);
            tweetSection = parser.formatField(tweetSection);
            // System.out.println(tweetSection);

            Parser parserMessage = new Parser();
            String tweetMessage = parserMessage.extract("tweet-text\">", "</p>", tweetSection, actionFirst.SKIP,
                    actionLast.SKIP);

            // tag set
            Parser parserTagSet = new Parser();
            Set<String> tags = new LinkedHashSet<String>();
            while (parserTagSet.seek("data-query-source=\"hashtag_click\"", tweetMessage, direction.FORWARD,
                    action.SKIP) != -1) {
                parserTagSet.seek("dir=\"ltr", tweetMessage, direction.FORWARD, action.SKIP);
                String tag = parserTagSet.extract(">", "</a>", tweetMessage, actionFirst.SKIP, actionLast.SKIP);
                tag = parserTagSet.clearTags(tag);
                tag = tag.replaceAll(" ", "");
                tags.add(tag.toUpperCase());
            }

            String tweetID = "";

            Parser parserRetweet = new Parser();
            String retweetId = parserRetweet.extract("data-retweet-id=\"", "\"", tweetSection, actionFirst.SKIP,
                    actionLast.SKIP);

            if (tags.contains(mainTag.toUpperCase())) {
                Tweet tweet = new Tweet();
                if (tags.size() > 0) {
                    Set<Tag> tagSet = new LinkedHashSet<Tag>();
                    Iterator<String> iterator = tags.iterator();

                    while (iterator.hasNext()) {
                        String tagName = iterator.next();
                        TagDAO tagDao = new TagDAO();
                        Tag tagObj = tagDao.findByTagName(tagName);

                        if (tagObj == null) {
                            tagObj = new Tag();
                            tagObj.setName(tagName);
                            tagDao.save(tagObj);
                        }
                        tagSet.add(tagObj);
                    }

                    tweet.setTagSet(tagSet);
                }

                // author set
                Parser parserAuthorSet = new Parser();
                Set<Author> authorSet = new LinkedHashSet<Author>();
                while (parserAuthorSet.seek("class=\"username js-action-profile-name", tweetSection, direction.FORWARD,
                        action.SKIP) != -1) {
                    String authorUserName = parserAuthorSet.extract(">", "</a>", tweetSection, actionFirst.SKIP,
                            actionLast.SKIP);
                    authorUserName = parserAuthorSet.clearTags(authorUserName);
                    authorUserName = authorUserName.replaceAll(" ", "");
                    authorUserName = authorUserName.trim();

                    if (authorUserName.equalsIgnoreCase("@granguru") || authorUserName.equalsIgnoreCase("@EMCHellas")) {
                        System.out.println("Aici crapa...");
                    }

                    AuthorDAO authorDao = new AuthorDAO();
                    Author author = authorDao.findByUserName(authorUserName);
                    if (author != null) {
                        authorSet.add(author);
                    } else {
                        author = new Author();
                        author.setUsername(authorUserName);

                        try {
                            String authorPage = downloadPage("https://twitter.com/" + authorUserName.replace("@", ""));
                            // System.out.println("https://twitter.com/" +
                            // authorUserName.replace("@", ""));
                            // System.out.println(authorPage);
                            Parser parserAuthor = new Parser();
                            parser.seek("profile-header-inner flex-module clearfix", authorPage, direction.FORWARD,
                                    action.SKIP);

                            String authorImageLink = parserAuthor.extract("<img src=\"", "\"", authorPage,
                                    actionFirst.SKIP, actionLast.SKIP);
                            String extension = authorImageLink.substring(authorImageLink.lastIndexOf("."),
                                    authorImageLink.length());
                            downloadImage(authorImageLink, authorUserName + extension, "images/users/");
                            author.setPoza("images/users/" + authorUserName + extension);

                            String authorName = parserAuthor.extract("<h1 class=\"fullname editable-group\">", "</",
                                    authorPage, actionFirst.SKIP, actionLast.SKIP);
                            authorName = parserAuthor.clearTags(authorName);
                            authorName = authorName.trim();
                            author.setName(authorName);

                        } catch (Exception e) {
                            System.out.println("Error downloading user " + authorUserName + " page: " + e.getMessage());
                            e.printStackTrace();
                        }

                        authorDao.save(author);

                        authorSet.add(author);
                    }
                }
                if (authorSet.size() > 0) {
                    tweet.setAuthorSet(authorSet);
                }

                // internal ID
                Parser parserTweetId = new Parser();
                tweetID = parserTweetId.extract("-", "\"", tweetSection, actionFirst.SKIP, actionLast.SKIP);
                tweet.setInternalID(tweetID);
                tagTweetId = tweetID;

                // description
                String description = parserMessage.clearTags(tweetMessage);
                description = description.replaceAll("  ", "");
                description = parserMessage.replaceSpecialChars(description);
                description = parserMessage.replaceAsciCharacters(description);
                tweet.setDescription(description);

                // author
                Parser parserAuthorUserName = new Parser();
                String authorUserName = parserAuthorUserName.extract("class=\"username js-action-profile-name\">",
                        "</span", tweetSection, actionFirst.SKIP, actionLast.SKIP);

                authorUserName = parserAuthorUserName.clearTags(authorUserName);
                authorUserName = authorUserName.replaceAll("  ", "");
                authorUserName = authorUserName.trim();

                AuthorDAO authorDao = new AuthorDAO();
                Author author = authorDao.findByUserName(authorUserName);
                if (author != null) {
                    tweet.setAuthor(author.getId());
                } else {
                    author = new Author();

                    author.setUsername(authorUserName);

                    Parser parserAuthor = new Parser();
                    String authorName = parserAuthor.extract(
                            "class=\"fullname js-action-profile-name show-popup-with-id\">", "</", tweetSection,
                            actionFirst.SKIP, actionLast.SKIP);
                    if (authorName.contains("EMC Corporation") || authorName.contains("Forrester Research")) {
                        System.out.println("Test");
                    }
                    authorName = parserAuthor.clearTags(authorName);
                    authorName = authorName.trim();
                    author.setName(authorName);

                    Parser authorImageParser = new Parser();
                    String authorImageLink = "";
                    if (authorImageParser.seek("<img class=\"avatar js-action-profile-avatar\"", tweetSection,
                            direction.FORWARD, action.SKIP) != -1) {
                        authorImageLink = authorImageParser.extract("src=\"", "\"", tweetSection, actionFirst.SKIP,
                                actionLast.SKIP);
                        String extension = authorImageLink.substring(authorImageLink.lastIndexOf("."),
                                authorImageLink.length());
                        downloadImage(authorImageLink, authorUserName + extension, "images/users/");
                        author.setPoza("images/users/" + authorUserName + extension);
                    }
                    authorDao.save(author);

                    author = authorDao.findByUserName(authorUserName);
                    tweet.setAuthor(author.getId());
                }

                // tweet date
                Parser parserDate = new Parser();
                String dateString = "";
                if (parserDate.seek("class=\"tweet-timestamp js-permalink js-nav\"", tweetSection, direction.FORWARD,
                        action.SKIP) != -1) {
                    dateString = parserDate.extract("title=\"", "\"", tweetSection, actionFirst.SKIP, actionLast.SKIP);
                    DateFormat formatter = new SimpleDateFormat("KK:mm aa - dd MMM yy");
                    ;
                    try {
                        formatter.parse(dateString);
                        tweet.setDate(dateString);
                    } catch (ParseException e) {
                        System.out.println("Error parsing tweet date: " + e.getMessage());
                    }
                }

                // expand, view media, view video, view summary, view
                // conversation,
                // view photo, view album
                Parser parserExpandType = new Parser();
                String expandType = "";
                if (parserExpandType.seek("class=\"expand-stream-item js-view-details", tweetSection,
                        direction.FORWARD, action.SKIP) != -1) {
                    expandType = parserExpandType.extract(">", "</span>", tweetSection, actionFirst.SKIP,
                            actionLast.SKIP);
                    expandType = parserExpandType.clearTags(expandType);
                    expandType = expandType.replaceAll("  ", "");
                    tweet.setExpand(expandType);
                }

                TweetDAO tweetDao = new TweetDAO();
                if (!tweetDao.existsInDb(tweet)) {
                    tweetDao.save(tweet);

                    // save links from description
                    Parser linkParser = new Parser();
                    Set<String> urls = ConnectorUtils.parseURLs(linkParser, "http", description);
                    urls.addAll(ConnectorUtils.parseURLs(linkParser, "www.", description));

                    TweetLinkDAO tweetLinkDao = new TweetLinkDAO();
                    for (String url : urls) {
                        TweetLink tweetLink = new TweetLink();
                        tweetLink.setTweetId(tweet.getId());
                        tweetLink.setLink(url);

                        if (!tweetLinkDao.existsInDb(tweetLink)) {
                            tweetLinkDao.save(tweetLink);
                        }
                    }
                } else if (!"".equals(retweetId)) {
                    System.out.println("Tweet-ul " + tweet.getInternalID() + " a fost retweet-uit prin " + retweetId);

                    TweetRetweet retweet = new TweetRetweet();
                    retweet.setTweetId(tweet.getInternalID());
                    retweet.setRetweetId(retweetId);

                    TweetRetweetDAO retweetDAO = new TweetRetweetDAO();
                    if (!retweetDAO.existsInDb(retweet)) {
                        retweetDAO.save(retweet);
                    }
                } else {
                    System.out.println("Tweet-ul exista deja: " + tweet.getInternalID());
                    // return v;
                }
            } else {
                Parser parserTweetId = new Parser();
                tweetID = parserTweetId.extract("-", "\"", tweetSection, actionFirst.SKIP, actionLast.SKIP);
            }

            // if (!"".equals(retweetId)) {
            // System.out.println("i " + i + ":" + tweetID + "   retweet:" +
            // retweetId);
            // } else {
            // System.out.println("i " + i + ":" + tweetID);
            // }
            if ((position == 20 || position == 1) && !"".equals(retweetId)) {
                tweetID = retweetId;
            }

            v.add(tweetID);
            map.put(tweetID, tweetID);
        }

        return v;
    }

    public void writeInFile(String path, String content) {
        try {
            PrintWriter exitFile = new PrintWriter(new FileWriter(path), true);
            exitFile.println(content);
            exitFile.close();
        } catch (IOException e) {
            System.err.println("Eroare de file " + e.toString());
        }

    }

    public void appendInFile(String path, String content) {
        FileWriter fileWritter;
        try {
            fileWritter = new FileWriter(path, true);

            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(content);
            bufferWritter.newLine();
            bufferWritter.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String downloadPage(String url) throws Exception {

        String pageSource = new String();
        // String httpsURL = "http://twitter.com/raresva";
        URL myurl = new URL(url);
        HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        // String header = new String();

        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            pageSource += inputLine;
        }

        return pageSource;
    }

    private static boolean downloadImage(String pictureLink, String name, String folderPath) {
        try {
            URL pictureURL = new URL(pictureLink);
            URLConnection connPicture = pictureURL.openConnection();

            File picturesFolder = new File(folderPath);
            if (!picturesFolder.exists()) {
                picturesFolder.mkdirs();
            }

            BufferedInputStream buf = new BufferedInputStream(connPicture.getInputStream());
            FileOutputStream fstream = new FileOutputStream(folderPath + "/" + name);
            int readBytes = 0;
            byte[] buffer = new byte[128];
            while ((readBytes = buf.read(buffer)) != -1) {
                fstream.write(buffer, 0, readBytes);
            }

            fstream.close();
            return true;
        } catch (MalformedURLException e) {
            return false;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    public String getMainTag() {
        return mainTag;
    }

    public void setMainTag(String mainTag) {
        this.mainTag = mainTag;
    }

}