package ro.star.archiver.connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.validator.routines.UrlValidator;

import parsing.Parser;
import parsing.ParsingEvents;

public class ConnectorUtils implements ParsingEvents {
    
    public static Set<String> parseURLs(Parser linkParser, String urlStart, String messageDescription) {
        Set<String> urls = new HashSet<String>();
        UrlValidator urlValidator = new UrlValidator();
        while (linkParser.seek(urlStart, messageDescription, direction.FORWARD, action.NOTHING) != -1) {
            String link = linkParser.extract(urlStart, " ", messageDescription, actionFirst.NOTHING, actionLast.SKIP);
            if ("".equals(link)) {
                link = messageDescription.substring(messageDescription.lastIndexOf(urlStart));
            }

            linkParser.seek(urlStart, messageDescription, direction.FORWARD, action.SKIP);

            String url = new String();
            while (link != null) {
                if (!link.startsWith("http") && !link.startsWith("https")) {
                    link = "http://" + link;
                }
                if (link.startsWith("https")) {
                    link = link.replace("https", "http");
                }
                if (!link.startsWith("http://")){
                    link = link.replace("http:/", "http://");
                }
                if (!link.startsWith("https://")){
                    link = link.replace("https:/", "https://");
                }
                
                if (!url.equals(link)) {
                    url = link;
                    link = getRedirectionUrl(link);

                    if (urlValidator.isValid(link)) {
                        System.out.println(link);
                    } else {
                        break;
                    }
                } else {
                    link = null;
                }
            }

            if (!"".equals(url) && urlValidator.isValid(url)) {
                urls.add(url);
            }

        }
        return urls;
    }

    public static String getRedirectionUrl(String link) {
        BufferedReader in = null;
        try {
            URL url = new URL(link);
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.connect();
            in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            return (uc.getHeaderField("Location"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != in) {
                    in.close();
                }
            } catch (IOException e) {
                // ignore
            }
        }
        return null;
    }

}
