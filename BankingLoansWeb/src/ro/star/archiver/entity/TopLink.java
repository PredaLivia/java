package ro.star.archiver.entity;

import java.math.BigInteger;

public class TopLink {

    private String link;
    private BigInteger number;

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public BigInteger getNumber() {
        return number;
    }
    public void setNumber(BigInteger number) {
        this.number = number;
    }
}