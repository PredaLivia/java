package ro.star.archiver.entity;

public class Author {

    private Long id;
    private String name = new String();
    private String username = new String();
    private String poza = new String();

    public Author() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPoza() {
        return poza;
    }

    public void setPoza(String poza) {
        this.poza = poza;
    }

    public boolean equals(Author author) {
        return this.id == author.getId();
    }

}
