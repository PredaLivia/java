package ro.star.archiver.entity;

import java.io.Serializable;

public class TweetAuthor implements Serializable {
   
    /**
     * 
     */
    private static final long serialVersionUID = -6218346531381822866L;
    private Long tweetId;
    private Long authorId;

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

}