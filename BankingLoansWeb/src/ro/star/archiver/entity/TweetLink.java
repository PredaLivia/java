package ro.star.archiver.entity;

import java.io.Serializable;

public class TweetLink implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4317974172501421381L;
    private Long id;
    private Long tweetId;
    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
