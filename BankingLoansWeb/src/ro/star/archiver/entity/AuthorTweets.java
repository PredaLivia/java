package ro.star.archiver.entity;

import java.math.BigInteger;

public class AuthorTweets {
    private BigInteger authorId;
    private BigInteger number;

    public BigInteger getAuthorId() {
        return authorId;
    }

    public void setAuthorId(BigInteger authorId) {
        this.authorId = authorId;
    }

    public BigInteger getNumber() {
        return number;
    }

    public void setNumber(BigInteger number) {
        this.number = number;
    }
}
