package ro.star.archiver.entity;

import java.math.BigInteger;

public class RetweetNo {
    private BigInteger tweetId;
    private BigInteger number;

    public BigInteger getTweetId() {
        return tweetId;
    }

    public void setTweetId(BigInteger tweetId) {
        this.tweetId = tweetId;
    }

    public BigInteger getNumber() {
        return number;
    }

    public void setNumber(BigInteger number) {
        this.number = number;
    }

}
