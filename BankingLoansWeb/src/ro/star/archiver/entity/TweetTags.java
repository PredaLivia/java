package ro.star.archiver.entity;

import java.io.Serializable;

public class TweetTags implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 121311947492488627L;
    private Long tweetId;
    private Long tagId;

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

}
