package ro.star.archiver.entity;

import java.io.Serializable;

public class TweetRetweet implements Serializable {

    private static final long serialVersionUID = 1786938033510994193L;
    private String tweetId;
    private String retweetId;

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public String getRetweetId() {
        return retweetId;
    }

    public void setRetweetId(String retweetId) {
        this.retweetId = retweetId;
    }

}
