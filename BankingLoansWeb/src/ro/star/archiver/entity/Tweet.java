package ro.star.archiver.entity;

import java.util.Set;

public class Tweet {

	private Long id;
	private String title = new String();
	private String description = new String();
	private Long author;
	private String date = new String();
	private String expand = new String();
	private String viewMedia = new String();
	private String viewVideo = new String();
	private String viewSummary = new String();
	private String viewConversation = new String();
	private String viewPhoto = new String();
	private String internalID = new String();
	private Set authorSet;
	private Set tagSet;
	
	public Tweet (){
		
	}

	public Set getTagSet() {
		return tagSet;
	}

	public void setTagSet(Set tagSet) {
		this.tagSet = tagSet;
	}

	public Set getAuthorSet() {
		return authorSet;
	}

	public void setAuthorSet(Set authorSet) {
		this.authorSet = authorSet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getAuthor() {
		return author;
	}

	public void setAuthor(Long author) {
		this.author = author;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getExpand() {
		return expand;
	}

	public void setExpand(String expand) {
		this.expand = expand;
	}

	public String getViewMedia() {
		return viewMedia;
	}

	public void setViewMedia(String viewMedia) {
		this.viewMedia = viewMedia;
	}

	public String getViewVideo() {
		return viewVideo;
	}

	public void setViewVideo(String viewVideo) {
		this.viewVideo = viewVideo;
	}

	public String getViewSummary() {
		return viewSummary;
	}

	public void setViewSummary(String viewSummary) {
		this.viewSummary = viewSummary;
	}

	public String getViewConversation() {
		return viewConversation;
	}

	public void setViewConversation(String viewConversation) {
		this.viewConversation = viewConversation;
	}

	public String getViewPhoto() {
		return viewPhoto;
	}

	public void setViewPhoto(String viewPhoto) {
		this.viewPhoto = viewPhoto;
	}

	public String getInternalID() {
		return internalID;
	}

	public void setInternalID(String internalID) {
		this.internalID = internalID;
	}
	
	
}
