package ro.star.archiver.dao;

import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.TweetRetweet;
import ro.star.archiver.util.HibernateUtil;

public class TweetRetweetDAO {

    public void save(TweetRetweet object) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        System.out.println("Obiectul TweetRetweet a fost salvat!");
    }
    
    public boolean existsInDb(String tweetID, String retweetID) {
        TweetRetweet objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from TweetRetweet com where com.tweetId=? and com.retweetId=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, tweetID);
            query.setParameter(0, retweetID);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (TweetRetweet) it.next();
                System.out.println("Obiectul TweetRetweet exista deja in baza de date...");
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        if (objCreated == null) {
            return false;
        }
        return true;
    }
    
    public boolean existsInDb(TweetRetweet retweet) {
        TweetRetweet objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from TweetRetweet com where com.tweetId=? and com.retweetId=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, retweet.getTweetId());
            query.setParameter(1, retweet.getRetweetId());
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (TweetRetweet) it.next();
                System.out.println("Obiectul TweetRetweet exista deja in baza de date...");
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        if (objCreated == null) {
            return false;
        }
        return true;
    }

}
