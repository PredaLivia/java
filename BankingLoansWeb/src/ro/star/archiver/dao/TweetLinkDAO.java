package ro.star.archiver.dao;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

import ro.star.archiver.entity.AuthorTweets;
import ro.star.archiver.entity.TopLink;
import ro.star.archiver.entity.Tweet;
import ro.star.archiver.entity.TweetLink;
import ro.star.archiver.entity.TweetRetweet;
import ro.star.archiver.util.HibernateUtil;

public class TweetLinkDAO {

    public void save(TweetLink object) {

        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        System.out.println("Obiectul TweetLink a fost salvat!");
    }

    public boolean existsInDb(String tweetID, String link) {
        TweetLink objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from TweetLink com where com.tweetId=? and com.link=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, tweetID);
            query.setParameter(0, link);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (TweetLink) it.next();
                System.out.println("Obiectul TweetLink exista deja in baza de date...");
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        if (objCreated == null) {
            return false;
        }
        return true;
    }

    public boolean existsInDb(TweetLink tweetLink) {
        TweetLink objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from TweetLink com where com.tweetId=? and com.link=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, tweetLink.getTweetId());
            query.setParameter(1, tweetLink.getLink());
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (TweetLink) it.next();
                System.out.println("Obiectul TweetLink exista deja in baza de date...");
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        if (objCreated == null) {
            return false;
        }
        return true;
    }

    public ArrayList<TweetLink> getAllLinks() {

        ArrayList<TweetLink> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String SQL_QUERY = "from TweetLink com";
            Query query = session.createQuery(SQL_QUERY);

            objCreated = (ArrayList<TweetLink>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    public ArrayList<TopLink> getTopLinks() {
        ArrayList<TopLink> linksList = new ArrayList<TopLink>();
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "select link, count(tweetId) as number FROM tweet_link group by link order by number desc";
            Query query = session.createSQLQuery(SQL_QUERY).setResultTransformer(
                    Transformers.aliasToBean(TopLink.class));
//            String SQL_QUERY = "from TweetLink tl";
//            Query query = session.createQuery(SQL_QUERY);
            linksList = (ArrayList<TopLink>) query.list();
            
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        } finally {
        }
        return linksList;
    }
}
