package ro.star.archiver.dao;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.Tag;
import ro.star.archiver.entity.Tweet;
import ro.star.archiver.util.HibernateUtil;

public class TagDAO {

    public TagDAO() {

    }

    public void save(Tag object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getName());
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        System.out.println("Obiectul Tag a fost salvat!");
    }

    public Tag findByTagName(String name) {
        Tag objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tag com where com.name=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, name);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (Tag) it.next();
                System.out.println("Gasim tag-ul " + objCreated.getName());
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }
    
    public ArrayList<Tag> getTagsIDByAuthorID(Long authorID){
        ArrayList<Tag> tagslist = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tag t where t.id in (select  t1.tagId FROM TweetTags t1, Tag t2 where t1.tagId = t2.id  and t1.tweetId in (select id from Tweet where author = ?))";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, authorID);
            tagslist = (ArrayList<Tag>) query.list();
            session.close();

        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
        }
        return tagslist;
    }
    
    public ArrayList<Tag> getAllTags() {

        ArrayList<Tag> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tag com";
            Query query = session.createQuery(SQL_QUERY);

            objCreated = (ArrayList<Tag>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

}
