package ro.star.archiver.dao;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.RetweetNo;
import ro.star.archiver.entity.Tag;
import ro.star.archiver.entity.Tweet;
import ro.star.archiver.util.HibernateUtil;

public class TweetDAO {

    public void TweetDAO() {

    }

    public ArrayList<Tweet> getTweetByTagID(Long tagID) {

        ArrayList<Tweet> tweetList = new ArrayList<Tweet>();
        return tweetList;
    }

    public ArrayList<Tweet> getAllTweets() {

        ArrayList<Tweet> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String SQL_QUERY = "from Tweet com";
            Query query = session.createQuery(SQL_QUERY);

            objCreated = (ArrayList<Tweet>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    // public ArrayList<Tweet> findByAuthor(String name) {
    // Tag objCreated = null;
    // Session session = HibernateUtil.getSessionFactory().openSession();
    //
    // try {
    //
    // String SQL_QUERY = "from Tag com where com.name=?";
    // Query query = session.createQuery(SQL_QUERY);
    // query.setParameter(0, name);
    // for (Iterator it = query.iterate(); it.hasNext();) {
    // objCreated = (Tag) it.next();
    // System.out.println("Gasim tag-ul " + objCreated);
    // }
    // session.close();
    //
    // } catch (HibernateException e1) {
    // System.out.println(e1.getMessage());
    // } finally {
    // }
    // return objCreated;
    // }

    public void save(Tweet object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getTitle());
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        System.out.println("Obiectul Tweet a fost salvat!");
    }

    public boolean existsInDb(Tweet tweet) {
        Tweet objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tweet com where com.internalID=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, tweet.getInternalID());
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (Tweet) it.next();
                System.out.println("Gasim tweet-ul " + objCreated);
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        if (objCreated == null) {
            return false;
        }
        return true;

    }

    public Tweet findByInternalID(String internalID) {
        Tweet objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tweet com where com.internalID=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, internalID);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (Tweet) it.next();
                System.out.println("Gasim tweet-ul " + objCreated.getInternalID());
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    public Tweet findByID(Long id) {
        Tweet objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tweet com where com.id=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (Tweet) it.next();
                System.out.println("Gasim tweet-ul " + objCreated.getInternalID());
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    public ArrayList<Tweet> getTweetsByAuthorID(Long authorID) {
        ArrayList<Tweet> tweetlist = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tweet t where t.author=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, authorID);
            tweetlist = (ArrayList<Tweet>) query.list();
            session.close();

        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
        }
        return tweetlist;
    }

    public ArrayList<Tweet> searchTweets(String criteria) {
        ArrayList<Tweet> tweetlist = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tweet com where com.description like :criteria";
            Query query = session.createQuery(SQL_QUERY);
            query.setString("criteria", "%" + criteria + "%");
            tweetlist = (ArrayList<Tweet>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return tweetlist;
    }

    public ArrayList<RetweetNo> getTopNumberOfRetweets() {
        ArrayList<RetweetNo> tweetlist = new ArrayList<RetweetNo>();
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            String SQL_QUERY = "select t.tweetId, r.number from tweet t left join (SELECT tweetId, count(retweetId) as number FROM tweet_retweet group by tweetId) r ON t.internalId = r.tweetId order by r.number desc LIMIT 50";
            Query query = session.createSQLQuery(SQL_QUERY).setResultTransformer(
                    Transformers.aliasToBean(RetweetNo.class));
            tweetlist = (ArrayList<RetweetNo>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return tweetlist;
    }

}
