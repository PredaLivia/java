package ro.star.archiver.dao;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

import ro.fys.nupcrawler.entities.beans.Keyword;
import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.AuthorTweets;
import ro.star.archiver.entity.RetweetNo;
import ro.star.archiver.entity.Tweet;
import ro.star.archiver.util.HibernateUtil;

public class AuthorDAO {

    public AuthorDAO() {

    }

    public Author save(Author object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getName());
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        System.out.println("Obiectul Author a fost salvat!");
        return object;
    }

    public ArrayList<Author> getAllAuthors() {

        ArrayList<Author> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Author com";
            Query query = session.createQuery(SQL_QUERY);

            objCreated = (ArrayList<Author>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    public Author findByUserName(String name) {
        Author objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Author com where com.username=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, name);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (Author) it.next();
                System.out.println("Gasim userul " + objCreated.getName() + " " + objCreated.getUsername());
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }
    
    public Author findByID(Long id) {
        Author objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Author com where com.id= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (Author) it.next();
                System.out.println("Gasim userul " + objCreated);
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }
    
    
    
    public ArrayList<Tweet> getAuthorTweets(Author author){
        ArrayList<Tweet> tweetlist = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Tweet t where t.author=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, author.getId());
            tweetlist = (ArrayList<Tweet>) query.list();
            session.close();

        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } finally {
        }
        return tweetlist;
    }
    
    public ArrayList<AuthorTweets> getTopActiveUsers() {
        ArrayList<AuthorTweets> authorList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "select authorId, count(tweetId) as number FROM tweet_author group by authorId order by number desc LIMIT 50;";
            Query query = session.createSQLQuery(SQL_QUERY).setResultTransformer(
                    Transformers.aliasToBean(AuthorTweets.class));
            authorList = (ArrayList<AuthorTweets>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return authorList;
    }

}
