package ro.star.test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.validator.routines.UrlValidator;

import parsing.Parser;
import parsing.ParsingEvents;
import ro.star.archiver.dao.AuthorDAO;
import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.Tweet;

public class TwitterParser implements ParsingEvents {

    LinkedHashSet<String> tweetList = new LinkedHashSet<String>();
    LinkedHashSet<String> userList = new LinkedHashSet<String>();
    private LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
    private String tagTweetId = null;

    // private int contor = 0;

    public TwitterParser() {

    }

    public void parseTagUsers(String tag) throws Exception {
        String page = "";
        String url = "http://twitter.com/search/realtime?q=" + tag + "&src=typd";
        try {
            page = downloadPage(url);
        } catch (Exception e) {
            System.out.println("Exception reading page: " + e.getMessage());
            return;
        }

        Parser parser = new Parser();
        if (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            tagTweetId = parser.extract("-", "\"", page, actionFirst.SKIP, actionLast.SKIP);
        } else {
            System.out.println("No start tweet ID found for tag '" + tag + "'");
        }

        getTagUsers(tag);
    }

    public void getTagUsers(String tag) throws Exception {

        while (tagTweetId != null) {
            String previousTweetId = tagTweetId;
            System.out.println("Se intra pentru " + tagTweetId + "###########################");
            String pageSource = getTagNextPage(tag, tagTweetId);
            System.out.println(pageSource);
            writeInFile("d://TweetFiles/" + tag + "_" + tagTweetId + ".txt", pageSource);

            Set<String> v = new LinkedHashSet<String>();
            v = parseTagPage(pageSource);
            userList.addAll(v);
            if (previousTweetId.equalsIgnoreCase(tagTweetId)) {
                tagTweetId = null;
            }

        }
        System.out.println("Numar useri: " + userList.size());
        System.out.println(userList);

    }

    public String getTagNextPage(String tag, String tweetID) throws Exception {

        String pageSource = new String();
        String httpsURL = "http://twitter.com/i/search/timeline?src=typd&type=recent&include_available_features=1&include_entities=1&max_id="
                + tweetID + "&q=" + tag;

        URL myurl = new URL(httpsURL);
        HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        // String header = new String();

        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            pageSource += inputLine;
        }

        return pageSource;
    }

    public Set<String> parseTagPage(String page) {
        Parser parser = new Parser();

        Set<String> v = new LinkedHashSet<String>();
        int i = 0;

        while (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            tagTweetId = parser.extract("-", "\\\"", page, actionFirst.SKIP, actionLast.SKIP);
            String userName = parser.extract("data-screen-name=\\\"", "\\\"", page, actionFirst.SKIP, actionLast.SKIP);

            System.out.println("i " + i + ":" + userName);

            i++;
            v.add(userName);
        }
        System.out.println(v.size() + " users: " + v);
        return v;
    }

    public void parseUserTweets(String user) throws Exception {
        String page = "";
        String firstTweet = null;
        String url = "http://twitter.com/" + user;
        try {
            page = downloadPage(url);
        } catch (Exception e) {
            System.out.println("Exception reading page: " + e.getMessage());
            return;
        }

        Parser parser = new Parser();
        if (parser.seek("stream-item-tweet", page, direction.FORWARD, action.SKIP) != -1) {
            firstTweet = parser.extract("-", "\"", page, actionFirst.SKIP, actionLast.SKIP);
        } else {
            System.out.println("No start tweet found");
        }

        getTweets(user, firstTweet);
    }

    public void getTweets(String user, String tweetStart) throws Exception {

        // String tweetStart = "266609304793128960";
        // pentru teste
        // String tweetStart = "225507546641858560";
        // tweetStart = "268030727755542528";

        while (tweetStart != null) {

            System.out.println("Se intra pentru " + tweetStart + "###########################");
            String pageSource = getNextPage(user, tweetStart);
            System.out.println(pageSource);
            writeInFile("d://TweetFilesTest/" + tweetStart + ".txt", pageSource);

            Set<String> v = new LinkedHashSet<String>();
            v = parsePage(pageSource);

            tweetList.addAll(v);
            if (v.size() == 20) {
                tweetStart = (String) v.toArray()[v.size() - 1];
            } else {
                tweetStart = null;
            }

        }
        System.out.println("Numar twetturi: " + tweetList.size());
        System.out.println("Numar twetturi map: " + map.size());
        System.out.println(map.toString());

    }

    public String getNextPage(String user, String tweetID) throws Exception {

        String pageSource = new String();
        // String httpsURL =
        // "http://twitter.com/i/search/timeline?src=typd&type=recent&include_available_features=1&include_entities=1&max_id="+tweetID+"&q=mmtm12";
        String httpsURL = "http://twitter.com/i/profiles/show/" + user
                + "/timeline/with_replies?include_available_features=1&include_entities=1&max_id=" + tweetID;

        URL myurl = new URL(httpsURL);
        HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        // String header = new String();

        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            pageSource += inputLine;
        }

        return pageSource;
    }

    public Set<String> parsePage(String page) {
        Parser parser = new Parser();

        Set<String> v = new LinkedHashSet<String>();
        int position = 0;
        int i = 0;
        while (parser.seek("data-item-type=\\\"tweet\\\"", page, direction.FORWARD, action.NOTHING) != -1) {
            position++;
            String tweetSection = parser.extract("stream-item-tweet", "expanded-content js-tweet-details-dropdown",
                    page, actionFirst.SKIP, actionLast.SKIP);
            System.out.println(tweetSection);
            tweetSection = parser.formatField(tweetSection);
            System.out.println(tweetSection);

            Tweet tweet = new Tweet();

            // internal ID
            Parser parserTweetId = new Parser();
            String tweetID = parserTweetId.extract("-", "\"", tweetSection, actionFirst.SKIP, actionLast.SKIP);
            tweet.setInternalID(tweetID);

            // title
            Parser parserTitle = new Parser();
            String title = parserTitle.extract("class=\"js-tweet-text\">", "</p>", tweetSection, actionFirst.SKIP,
                    actionLast.SKIP);
            title = parserTitle.clearTags(title);
            tweet.setTitle(title);

            // description
            tweet.setDescription(title);

            // author
            Parser parserAuthorUserName = new Parser();
            String authorUserName = parserAuthorUserName.extract("class=\"username js-action-profile-name\">",
                    "</span", tweetSection, actionFirst.SKIP, actionLast.SKIP);
            authorUserName = parserAuthorUserName.clearTags(authorUserName);
            AuthorDAO authorDao = new AuthorDAO();
            Author author = authorDao.findByUserName(authorUserName);
            if (author != null) {
                tweet.setAuthor(author.getId());
            } else {
                author = new Author();
                Parser parserAuthor = new Parser();
                String authorName = parserAuthor.extract("class=\fullname js-action-profile-name show-popup-with-id\"",
                        "</", tweetSection, actionFirst.SKIP, actionLast.SKIP);
                author.setName(authorName);
                author.setUsername(authorUserName);

                Parser authorImageParser = new Parser();
                String authorImage = "";
                if (authorImageParser.seek("<img class=\"avatar js-action-profile-avatar\"", tweetSection,
                        direction.FORWARD, action.SKIP) != -1) {
                    authorImage = authorImageParser.extract("src=\"", "\"", tweetSection, actionFirst.SKIP,
                            actionLast.SKIP);
                }
                author.setPoza(authorImage);
                authorDao.save(author);

                author = authorDao.findByUserName(authorUserName);
                tweet.setAuthor(author.getId());
            }

            // tweet date
            Parser parserDate = new Parser();
            String dateString = "";
            if (parserDate.seek("class=\"tweet-timestamp js-permalink js-nav\"", tweetSection, direction.FORWARD,
                    action.SKIP) != -1) {
                dateString = parserDate.extract("title=\"", "\">", tweetSection,
                        actionFirst.SKIP, actionLast.SKIP);
                DateFormat formatter = new SimpleDateFormat("KK:mm aa - dd MMM yy");;
                try {
                   formatter.parse(dateString);
                   tweet.setDate(dateString);
                } catch (ParseException e) {
                    System.out.println("Error parsing tweet date: " + e.getMessage());
                }
            }
            
            // expand, view media, view video, view summary, view conversation, view photo
            Parser parserExpandType = new Parser();
            String expandType = "";
            if (parserExpandType.seek("class=\"expand-stream-item js-view-details\"", tweetSection, direction.FORWARD,
                    action.SKIP) != -1) {
                expandType = parserExpandType.extract("title=\"", "\">", tweetSection, actionFirst.SKIP, actionLast.SKIP);
                
            }

            title = parserTitle.clearTags(title);
            tweet.setTitle(title);

            Parser parserRetweet = new Parser();
            String retweetId = parserRetweet.extract("data-retweet-id=\"", "\"", tweetSection, actionFirst.SKIP,
                    actionLast.SKIP);

            if (!"".equals(retweetId)) {
                System.out.println("i " + i + ":" + tweetID + "   retweet:" + retweetId + " TITLE: " + title);
            } else {
                System.out.println("i " + i + ":" + tweetID + " TITLE: " + title);
            }
            if ((position == 20 || position == 1) && !"".equals(retweetId)) {
                tweetID = retweetId;
            }

            i++;
            v.add(tweetID);
            map.put(tweetID, tweetID);
        }

        return v;
    }

    public void writeInFile(String path, String content) {
        try {
            PrintWriter exitFile = new PrintWriter(new FileWriter(path), true);
            exitFile.println(content);
            exitFile.close();
        } catch (IOException e) {
            System.err.println("Eroare de file " + e.toString());
        }

    }

    public String downloadPage(String url) throws Exception {

        String pageSource = new String();
        // String httpsURL = "http://twitter.com/raresva";
        URL myurl = new URL(url);
        HttpURLConnection con = (HttpURLConnection) myurl.openConnection();
        InputStream ins = con.getInputStream();
        // String header = new String();

        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            pageSource += inputLine;
        }

        return pageSource;
    }

    private static boolean getImage(String pictureLink, String name, String folderPath) {
        try {
            URL pictureURL = new URL(pictureLink);
            URLConnection connPicture = pictureURL.openConnection();

            File picturesFolder = new File(folderPath);
            if (!picturesFolder.exists()) {
                picturesFolder.mkdirs();
            }

            BufferedInputStream buf = new BufferedInputStream(connPicture.getInputStream());
            FileOutputStream fstream = new FileOutputStream(folderPath + "/" + name);
            int readBytes = 0;
            byte[] buffer = new byte[128];
            while ((readBytes = buf.read(buffer)) != -1) {
                fstream.write(buffer, 0, readBytes);
            }

            fstream.close();
            return true;
        } catch (MalformedURLException e) {
            return false;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }
    
    
    public static void main(String[] args){
        TwitterParser parser = new TwitterParser();
        String page = "";
        String url = "http://188.215.38.40";
            try {
                page = parser.downloadPage(url);
            } catch (Exception e) {
                System.out.println("Exception reading page: " + e.getMessage());
                e.printStackTrace();
                return;
            }
        
        System.out.println(page);
    }

}
