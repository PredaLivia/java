package ro.star.test;

import java.util.ArrayList;
import java.util.HashSet;

import ro.star.archiver.dao.AuthorDAO;
import ro.star.archiver.dao.TagDAO;
import ro.star.archiver.dao.TweetDAO;
import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.Tag;
import ro.star.archiver.entity.Tweet;

public class TestDAO {

    public TestDAO() {

    }

    public void getAllTweets() {

        ArrayList<Tweet> tweetsList = new ArrayList<Tweet>();
        TweetDAO tweetDao = new TweetDAO();

        tweetsList = tweetDao.getAllTweets();

        for (int i = 0; i < tweetsList.size(); i++) {
            System.out.println("Tweet Title " + tweetsList.get(i).getTitle());
        }

    }

    public void saveTag() {

        Tag tag = new Tag();
        tag.setName("mmtm12");

        TagDAO tagDao = new TagDAO();
        tagDao.save(tag);

    }

    public void saveTweet() {
        Tweet tweet = new Tweet();
        // tweet.setId(Long.parseLong("1"));
        tweet.setAuthor(Long.parseLong("9"));
        tweet.setDate("10.02.2004");
        tweet.setDescription("description");
        tweet.setExpand("expand");
        tweet.setTitle("title3");
        tweet.setViewConversation("viewConversation");
        tweet.setViewMedia("viewMedia");
        tweet.setViewPhoto("viewPhoto");
        tweet.setViewSummary("viewSummary");
        tweet.setViewVideo("viewVideo");
        tweet.setInternalID("232323232323");

        /* authors contained in tweet body */
        HashSet<Author> authorSet = new HashSet<Author>();

        Author author = new Author();
        author.setName("Ionescu3");
        author.setPoza("poza");
        author.setUsername("@ionescu3");

        AuthorDAO authDao = new AuthorDAO();
        Author autFind = new Author();
        autFind = authDao.findByUserName("@ionescu3");

        if (autFind != null) {
            authorSet.add(autFind);
        } else {
            authorSet.add(author);
        }

        Author author1 = new Author();
        author1.setName("Ionescu4");
        author1.setPoza("poza");
        author1.setUsername("@ionescu4");
        authorSet.add(author1);

        tweet.setAuthorSet(authorSet);

        /* tags contained in tweet body */

        HashSet<Tag> tagSet = new HashSet<Tag>();
        Tag tag = new Tag();
        tag.setName("#mmtm12");
        tagSet.add(tag);

        Tag tag1 = new Tag();
        tag1.setName("#mmtm13");
        tagSet.add(tag1);

        tweet.setTagSet(tagSet);

        TweetDAO tweetDao = new TweetDAO();
        tweetDao.save(tweet);

    }

    public void saveAuthor() {
        Author author = new Author();
        author.setName("Nume");
        author.setPoza("poza");
        author.setUsername("username");

        AuthorDAO authorDao = new AuthorDAO();
        authorDao.save(author);
    }

}
