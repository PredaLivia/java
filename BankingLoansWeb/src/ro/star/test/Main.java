package ro.star.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.validator.routines.UrlValidator;

import com.itextpdf.text.pdf.codec.Base64.InputStream;

import parsing.Parser;
import parsing.ParsingEvents;

import ro.fys.nupcrawler.dao.impl.UserDAO;
import ro.star.archiver.connector.ConnectorUtils;
import ro.star.archiver.connector.TwitterDailyParser;
import ro.star.archiver.connector.TwitterParser;
import ro.star.archiver.dao.AuthorDAO;
import ro.star.archiver.dao.TweetDAO;
import ro.star.archiver.dao.TweetLinkDAO;
import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.Tweet;
import ro.star.archiver.entity.TweetLink;

public class Main implements ParsingEvents {

    public static void main(String args[]) {

        try {
            // parser.parseUserTweets("raresva");
            // parser.getTweets("raresva", "270602960298442752");
            // parser.getTagUsersFromFile("d://TweetFiles mmtm12 Marius");
            // parser.parseTag("emcworld");
            // parser.parseTag("mmtm13");
            getLinksFromDB();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getLinksFromDB() {
        TweetDAO tweetDao = new TweetDAO();
        TweetLinkDAO tweetLinkDao = new TweetLinkDAO();
        List<Tweet> tweets = tweetDao.getAllTweets();
        for (Tweet tweet : tweets) {
            String messageDescription = tweet.getDescription();
            System.out.println(messageDescription);
            Parser linkParser = new Parser();
            Set<String> results = ConnectorUtils.parseURLs(linkParser, "www.", messageDescription);
            results.addAll(ConnectorUtils.parseURLs(linkParser, "http", messageDescription));
            // System.out.println(results);

            for (String url : results) {
                TweetLink tweetLink = new TweetLink();
                tweetLink.setTweetId(tweet.getId());
                tweetLink.setLink(url);

                if (!tweetLinkDao.existsInDb(tweetLink)) {
                    tweetLinkDao.save(tweetLink);
                }
            }
        }
    }

    public static void parseAuthorsFromDB() throws Exception {
        TwitterParser parser = new TwitterParser();
        AuthorDAO authorDAO = new AuthorDAO();
        List<Author> authorList = authorDAO.getAllAuthors();

        parser.setMainTag("#EMCWORLD");

        int index = 0;

        for (int i = 0; i < authorList.size(); i++) {
            index = i;
            if ("@office365fan".equals(authorList.get(i).getUsername())) {
                break;
            }
        }

        for (int i = index; i < authorList.size(); i++) {
            parser.parseUser(authorList.get(i).getUsername().replace("@", ""));
            parser.appendInFile("D:\\twitter_users.txt", authorList.get(i).getName() + " "
                    + authorList.get(i).getUsername());
        }
    }

    public static void testTweetMessageLinks(String message) {
        Parser linkParser = new Parser();
        Set<String> results = ConnectorUtils.parseURLs(linkParser, "www.", message);
        results.addAll(ConnectorUtils.parseURLs(linkParser, "http", message));
        System.out.println(results);

        TweetLinkDAO tweetLinkDao = new TweetLinkDAO();
        for (String url : results) {
            TweetLink tweetLink = new TweetLink();
            tweetLink.setTweetId(new Long(122));
            tweetLink.setLink(url);

            if (!tweetLinkDao.existsInDb(tweetLink)) {
                tweetLinkDao.save(tweetLink);
            }
        }
    }

}
