package ro.banking.entity;

public class Banca {
    
    private String tipBanca = new String();
    private String tipCredit = new String();
    private String rata = new String();
    private String dobanda = new String();
    private String tipDobanda = new String();
    private String sumaRambursat = new String ();
    private String dae = new String();
    private String sumaCredit = new String();
    private String venitMinim = new String();
    private String costTotal = new String();
    private String sursa = new String();
    
    public Banca(){
        
    }

    public String getTipBanca() {
        return tipBanca;
    }

    public void setTipBanca(String tipBanca) {
        this.tipBanca = tipBanca;
    }

    public String getTipCredit() {
        return tipCredit;
    }

    public void setTipCredit(String tipCredit) {
        this.tipCredit = tipCredit;
    }

    public String getRata() {
        return rata;
    }

    public void setRata(String rata) {
        this.rata = rata;
    }

    public String getSumaRambursat() {
        return sumaRambursat;
    }

    public void setSumaRambursat(String sumaRambursat) {
        this.sumaRambursat = sumaRambursat;
    }

    public String getDae() {
        return dae;
    }

    public void setDae(String dae) {
        this.dae = dae;
    }

    public String getDobanda() {
        return dobanda;
    }

    public void setDobanda(String dobanda) {
        this.dobanda = dobanda;
    }

    public String getTipDobanda() {
        return tipDobanda;
    }

    public void setTipDobanda(String tipDobanda) {
        this.tipDobanda = tipDobanda;
    }

    public String getSumaCredit() {
        return sumaCredit;
    }

    public void setSumaCredit(String sumaCredit) {
        this.sumaCredit = sumaCredit;
    }

    public String getVenitMinim() {
        return venitMinim;
    }

    public void setVenitMinim(String venitMinim) {
        this.venitMinim = venitMinim;
    }

    public String getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(String costTotal) {
        this.costTotal = costTotal;
    }

    public String getSursa() {
        return sursa;
    }

    public void setSursa(String sursa) {
        this.sursa = sursa;
    }
    
}
