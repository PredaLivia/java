package ro.banking.entity;

public class BancaBean {

    private String tipBancaBean = new String();
    private String tipCreditBean = new String();
    private String rataBean = new String();
    private String dobandaBean = new String();
    private String tipDobandaBean = new String();
    private String sumaRambursatBean = new String ();
    private String daeBean = new String();
    private String sumaCreditBean = new String();
    private String venitMinimBean = new String();
    private String costTotalBean = new String();
    private String sursaBean = new String();
    
    public BancaBean (){
        
        
    }

    public String getTipBancaBean() {
        return tipBancaBean;
    }

    public void setTipBancaBean(String tipBancaBean) {
        this.tipBancaBean = tipBancaBean;
    }

    public String getTipCreditBean() {
        return tipCreditBean;
    }

    public void setTipCreditBean(String tipCreditBean) {
        this.tipCreditBean = tipCreditBean;
    }

    public String getRataBean() {
        return rataBean;
    }

    public void setRataBean(String rataBean) {
        this.rataBean = rataBean;
    }

    public String getSumaRambursatBean() {
        return sumaRambursatBean;
    }

    public void setSumaRambursatBean(String sumaRambursatBean) {
        this.sumaRambursatBean = sumaRambursatBean;
    }

    public String getDaeBean() {
        return daeBean;
    }

    public void setDaeBean(String daeBean) {
        this.daeBean = daeBean;
    }

    public String getDobandaBean() {
        return dobandaBean;
    }

    public void setDobandaBean(String dobandaBean) {
        this.dobandaBean = dobandaBean;
    }

    public String getTipDobandaBean() {
        return tipDobandaBean;
    }

    public void setTipDobandaBean(String tipDobandaBean) {
        this.tipDobandaBean = tipDobandaBean;
    }

    public String getSumaCreditBean() {
        return sumaCreditBean;
    }

    public void setSumaCreditBean(String sumaCreditBean) {
        this.sumaCreditBean = sumaCreditBean;
    }

    public String getVenitMinimBean() {
        return venitMinimBean;
    }

    public void setVenitMinimBean(String venitMinimBean) {
        this.venitMinimBean = venitMinimBean;
    }

    public String getCostTotalBean() {
        return costTotalBean;
    }

    public void setCostTotalBean(String costTotalBean) {
        this.costTotalBean = costTotalBean;
    }

    public String getSursaBean() {
        return sursaBean;
    }

    public void setSursaBean(String sursaBean) {
        this.sursaBean = sursaBean;
    }
    
    
}
