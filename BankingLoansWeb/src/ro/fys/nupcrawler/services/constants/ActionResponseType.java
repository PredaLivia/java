/*
 * ServiceConstants.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services.constants;

/**
 * Replace with your class description.
 */
public abstract class ActionResponseType {
    
    public static final int ACTION_SUCCESS = 1;
    public static final int ACTION_FAILED = 0;
    public static final String ACTION_RESPONSE_KEY = "ActionResponse";
    public static final String ACTION_MESSAGE_KEY = "ActionMessage";
    public static final String ACTION_ID_KEY = "ActionID";
 }
