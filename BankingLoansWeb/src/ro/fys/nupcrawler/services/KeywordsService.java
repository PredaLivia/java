/*
 * KeywordsService.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services;

import java.util.*;

import ro.fys.nupcrawler.dao.def.IKeywordDAO;
import ro.fys.nupcrawler.dao.impl.*;
import ro.fys.nupcrawler.entities.beans.*;
import ro.fys.nupcrawler.services.constants.ActionResponseType;

/**
 * 
 * This service is dealing with Keywords actions.
 */
public class KeywordsService {
    
    /**
     * The database object for the user table.
     */
    private IKeywordDAO dao =  new KeywordDAO();

    public HashMap<String, Object> addKeyword(Keyword keyword){
        HashMap<String, Object> response = new HashMap<String, Object>();

//        ArrayList<Keyword> identicalUserList = dao.findByName(keyword.getName(), keyword.get().getId());
//        if(identicalUserList != null && identicalUserList.size() > 0){
//            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
//            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword [" + keyword.getName() + "] is already registered in the system for this topic.");
//            return response;
//        }

        Long responseID = dao.save(keyword);
        if(responseID == 0){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The add operation for the [" + keyword.getName() + "] keyword failed.");
            return response;
        }

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword [" + keyword.getName() + "] has been successfully added.");
        response.put(ActionResponseType.ACTION_ID_KEY, responseID);
        return response;
    }

    public  HashMap<String, Object> deleteKeyword(Long id){
        HashMap<String, Object> response = new HashMap<String, Object>();

        int responseID = dao.delete(id);
        if(responseID == ActionResponseType.ACTION_FAILED){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword delete operation has failed because of database issues.");
            return response;
        }

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword has been successfully deleted.");
        return response;
    }

    public HashMap<String, Object> updateKeyword(Keyword keyword){
        HashMap<String, Object> response = new HashMap<String, Object>();

        Keyword keyword2update = dao.findByID(keyword.getId());
        if(keyword2update == null){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword [" + keyword.getName() + "] has an invalid ID.");
            return response;
        }
        
        dao.saveorupdate(keyword);
                
        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword has been successfully updated.");
        return response;
    }

    public Keyword getKeywordInfo(Long id){
        
        return dao.findByID(id);
    }
}
