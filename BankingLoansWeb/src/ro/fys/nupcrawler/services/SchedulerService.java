/*
 * TopicsService.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services;

import java.util.HashMap;

import ro.fys.nupcrawler.dao.def.ISchedulerDAO;
import ro.fys.nupcrawler.dao.impl.SchedulerDAO;
import ro.fys.nupcrawler.entities.beans.Scheduler;
import ro.fys.nupcrawler.services.constants.ActionResponseType;


public class SchedulerService {
    ISchedulerDAO dao = new SchedulerDAO();
    
    public HashMap<String, Object> updateScheduler(Scheduler scheduler) {
        HashMap<String, Object> response = new HashMap<String, Object>();

        dao.saveorupdate(scheduler);

        response.put(ActionResponseType.ACTION_RESPONSE_KEY,
                        ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY,
                        "The topic has been successfully updated.");
        return response;
    }
}
