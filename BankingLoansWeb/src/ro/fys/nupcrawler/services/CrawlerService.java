/*
 * CrawlerService.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services;

import java.io.IOException;
import java.util.*;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.swing.SwingUtilities;

import org.apache.commons.collections.map.HashedMap;

import com.icesoft.faces.webapp.http.servlet.ServletExternalContext;

import ro.fys.nupcrawler.algorithms.*;
import ro.fys.nupcrawler.algorithms.util.ContentUtil;
import ro.fys.nupcrawler.beans.ReportsBean;
import ro.fys.nupcrawler.dao.def.ITopicDAO;
import ro.fys.nupcrawler.dao.impl.NotificationMessageDAO;
import ro.fys.nupcrawler.dao.impl.ReportDAO;
import ro.fys.nupcrawler.dao.impl.TopicDAO;
import ro.fys.nupcrawler.entities.beans.*;
import ro.fys.nupcrawler.entities.connectors.*;
import ro.fys.nupcrawler.entities.message.NotificationMessage;
import ro.fys.nupcrawler.parsing.connectors.*;
import ro.fys.nupcrawler.parsing.connectors.base.IBaseConnector;
import ro.fys.nupcrawler.parsing.harvesting.DepthHarvester;
import ro.fys.nupcrawler.services.constants.ActionResponseType;
import ro.fys.nupcrawler.text.utils.WriteExcel;

/**
 * 
 * This service is dealing with web crawler actions.
 */
public class CrawlerService {

    private ArrayList<IBaseConnector> connectorsList = new ArrayList<IBaseConnector>();
    private static final int NUMBER_OF_RECORDS_PER_CONNECTOR = 10;
    private static final int NUMBER_OF_SEEDS_PER_KEYWORD = 10;

    ArrayList<SeedsURL> relevantSeeds = new ArrayList<SeedsURL>();
    private ITopicDAO topicDAO = new TopicDAO();

    public HashMap<String, Object> runAdHocCrawler(Topic topic, Runnable doLater){
        HashMap<String, Object> response = new HashMap<String, Object>();
        initConnectorsList();


        ArrayList<Keyword> keywords = new ArrayList<Keyword>(topic.getKeywords());
        if(keywords == null || keywords.size() == 0){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "No keyword for the current Topic already added. Please add some keywords and try again.");
            return response;
        }

        ArrayList<ArrayList<UrlResult>> resultsURL = null;


        for (int keywordsIndex = 0; keywordsIndex < keywords.size(); keywordsIndex++) {

            resultsURL = new ArrayList<ArrayList<UrlResult>>();
            for (int connectorsIndex = 0; connectorsIndex < connectorsList.size(); connectorsIndex++) {

                resultsURL.add(connectorsList.get(
                        connectorsIndex).parseResults(
                                NUMBER_OF_RECORDS_PER_CONNECTOR, keywords.get(keywordsIndex).getName()));
            }

            relevantSeeds.addAll(getBestSeeds(resultsURL, NUMBER_OF_SEEDS_PER_KEYWORD));

        }


        for (Iterator iterator = topic.getSeeds().iterator(); iterator.hasNext();) {
            Seed seed = (Seed)iterator.next();
            UrlResult urlres = new UrlResult();
            urlres.setUrl(seed.getUrl());
            relevantSeeds.add(new SeedsURL(urlres));
        }

        ArrayList<UrlEntity> urlEntityList =  new ArrayList<UrlEntity>();
        for (int i = 0; i < relevantSeeds.size(); i++) {
            System.out.println("ALT SEED");
            urlEntityList.addAll(new DepthHarvester().parseInDepth(relevantSeeds.get(i).urlResult.getUrl(), topic.getDeepnessLevel()));
        }


        long timeInMillis = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        Date date = cal.getTime();

        Page page = null;

        for (int i = 0; i < urlEntityList.size(); i++) {
            page = new Page();
            page.setContent("");
            page.setUpdateDate(date);
            page.setUrl(urlEntityList.get(i).getUrl());
            page.setWeight(new RelevanceAlgorithm().computeRelevanceScore(new ArrayList<Keyword>(topic.getKeywords()), urlEntityList.get(i).getContent()));

            int counter = 0;
            ArrayList<Page> pagesSet = new ArrayList<Page>(topic.getPages());
            for (int j = 0; j < pagesSet.size(); j++) {
                Page currentPage = pagesSet.get(j);
                ArrayList<BkwLinkMap> bkwList = new ArrayList<BkwLinkMap>(currentPage.getBkwLinks());
                for (int k = 0; k < bkwList.size(); k++) {
                    if(bkwList.get(k).getUrl().equals(urlEntityList.get(i).getUrl())){
                        counter++;
                    }
                }
            }

            page.setPvalue(new PValueAlgorithm().compute(urlEntityList.get(i).getChildrenUrls(), counter)); 

            page.setContentNumber(new ContentChangesAlgorithm().getPageRelevanceScore(urlEntityList.get(i).getContent()));

            //TODO Set Correct values for these.
            // page.setImageNumber(0);



            try {
                page.setStructureStart(ContentUtil.getFirstString(urlEntityList.get(i).getContent()));
            } catch (IOException e) {
                page.setStructureStart("");
            }
            try {
                page.setStructureEnd(ContentUtil.getSecondString(urlEntityList.get(i).getContent()));
            } catch (IOException e) {
                page.setStructureEnd("");
            }


            ArrayList<Page> existanceList = new ArrayList<Page>();
            for (int j = 0; j < pagesSet.size(); j++) {
                Page currentPage = pagesSet.get(j);
                if(currentPage.getUrl().equals(urlEntityList.get(i).getUrl())){
                    existanceList.add(currentPage);
                    break;
                }
            }

            if(existanceList != null && existanceList.size() > 0){
                 Page existingPage = existanceList.get(0);
                if(existingPage.getContentNumber() != page.getContentNumber() ||
                        //TODO: existingPage.getImageNumber() != page.getImageNumber() ||
                        !existingPage.getStructureStart().equals(page.getStructureStart()) ||
                        !existingPage.getStructureEnd().equals(page.getStructureEnd())
                ){

                    existingPage.setContent("");
                    existingPage.setUpdateDate(page.getUpdateDate());
                    existingPage.setUrl(page.getUrl());
                    existingPage.setWeight(page.getWeight());
                    existingPage.setBkwLinks(page.getBkwLinks());
                    existingPage.setContentNumber(page.getContentNumber());
                    existingPage.setPvalue(page.getPvalue());
                    existingPage.setStructureEnd(page.getStructureEnd());
                    existingPage.setStructureStart(page.getStructureStart());
                }
            }else{
                topic.getPages().add(page);
                
            }
        }
        
        new TopicDAO().saveorupdate(topic);
        
        String path = ((HttpSession) ((ServletExternalContext) FacesContext.getCurrentInstance()
                .getExternalContext()).getSession(true)).getServletContext().getRealPath("/");
        
        String reportName = new String();
        String reportId = new String();
        String reportDate = new String();
        String reportPath = new String();
        String reportUser = new String();
        String reportTopic = new String();
        
        Vector values = new Vector();
        
        Set pages = new HashSet(topic.getPages());
        
        for (Iterator iterator = pages.iterator(); iterator.hasNext();) {
            Page object = (Page) iterator.next();
            LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
            map.put("URL", object.getUrl());
            map.put("DATE", object.getUpdateDate().toString());
            map.put("RELEVANCE", object.getWeight().toString());
            values.add(map);
            
        }
        
        reportName = topic.getName() + "_" + new Date().getTime();
        reportPath = path + "WEB-INF\\resources\\" + reportName + ".xls";
        new WriteExcel(values, reportPath);
        
        Report report = new Report();
        report.setFilePath(reportPath);
        report.setReportName(reportName + ".xls");
        report.setTopic(topic);
        report.setUser(topic.getUser());
        report.setDate(new Date());
        
        new ReportDAO().save(report);
        
        NotificationMessage message =  new NotificationMessage();
        message.setNotificationMessageTitle("The system finished job for the topic: " + topic.getName());
        message.setUserId(topic.getUser().getId());
        new NotificationMessageDAO().save(message);
        
        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The crawling pricess has finished with success.");
        
        SwingUtilities.invokeLater(doLater);
        return response;
    }

    public HashMap<String, Object> runAdHocCrawler(Long topicID, Runnable doLater){
        HashMap<String, Object> response = new HashMap<String, Object>();
        Topic topic = topicDAO.findByID(topicID);
        if(topic == null){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The topic doesn't exist in the database.");
            return response;
        }
        
        return runAdHocCrawler(topic, doLater);
    }


    public void automaticCrawler(){
        Date currentDate = GregorianCalendar.getInstance().getTime();
        

    }

    public static void main(String[] args){

        Keyword keyword = new Keyword();
        keyword.setName("art");
        keyword.setWeight(0.5);
        HashSet<Keyword> keywords = new HashSet<Keyword>();
        keywords.add(keyword);


        Seed seed = new Seed();
        seed.setUrl("http://www.prosport.ro/");
        HashSet<Seed> seeds = new HashSet<Seed>();
        seeds.add(seed);


        Topic topic = new Topic();
        topic.setDeepnessLevel(1);
        topic.setName("FIRST TOPIC");
        topic.setKeywords(keywords);


      // new TopicDAO().save(topic);

        new CrawlerService().runAdHocCrawler(1L, null);

    }

    /**
     * Create the list of connectors to be run in the crawler engine.  
     */
    private void initConnectorsList() {
        connectorsList.add(new GoogleConnector());
        connectorsList.add(new YahooConnector());
        connectorsList.add(new BingConnector());
    }

    /**
     * 
     * @param resultsURL2
     * @return
     */
    private ArrayList<SeedsURL> getBestSeeds(ArrayList<ArrayList<UrlResult>> inputURLLists, int maxRecordsNumber) {

        ArrayList<SeedsURL> resultSeedsList = new ArrayList<SeedsURL>();

        ArrayList<SeedsURL> seedsList = new ArrayList<SeedsURL>();
        for (int i = 0; i < inputURLLists.size(); i++) {
            ArrayList<UrlResult> temp = inputURLLists.get(i);
            for (int j = 0; j < temp.size(); j++) {
                boolean found = false;
                for (int k = 0; k < seedsList.size(); k++) {
                    if(seedsList.get(k).urlResult.getUrl().equals(temp.get(j).getUrl())){
                        seedsList.get(k).categoryLevel++;
                        found = true;
                        continue;
                    }
                }
                if(!found){
                    seedsList.add(new SeedsURL(temp.get(j)));
                }
            }
        }

        ArrayList<SeedsURL> tmpList = new ArrayList<SeedsURL>();
        for (int i = connectorsList.size() ; i > 0; i--) {
            for (int j = 0; j < seedsList.size(); j++) {
                SeedsURL currentSeed = seedsList.get(j);
                if(currentSeed.categoryLevel == i){
                    tmpList.add(currentSeed);
                }
            }
        }

        int n = resultSeedsList.size();
        for(int j=0; j < maxRecordsNumber; j++){
            resultSeedsList.add(tmpList.get(j));
        }
        
        return resultSeedsList;
    }

    private class SeedsURL{

        public int categoryLevel = 1;
        public UrlResult urlResult = null;

        /**
         * 
         * @param urlResult
         */
        public SeedsURL(UrlResult urlResult) {
            this.urlResult = urlResult;
        }

    }

}
