package ro.fys.nupcrawler.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import ro.fys.nupcrawler.entities.gui.LeftMenuItem;

public class GUILeftMenuService {

    
    public GUILeftMenuService(){
        
    }
    
    public static ArrayList<LeftMenuItem> createLeftMenu(LinkedHashMap<String, String> menuItem){
        ArrayList<LeftMenuItem> leftMenu = new ArrayList<LeftMenuItem>();
        
        Set set = menuItem.keySet();
        for (Iterator iterator = set.iterator(); iterator.hasNext();) {
            
            String key = iterator.next().toString();
            String value = menuItem.get(key);
            
            LeftMenuItem menu = new LeftMenuItem();
            menu.setMenuItemId(key);
            menu.setMenuItemName(value);
            
            leftMenu.add(menu);
        }
        
        return leftMenu;
    }
    
    
}
