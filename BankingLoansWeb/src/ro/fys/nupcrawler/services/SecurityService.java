/*
 * SecurityService.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services;

import java.util.HashMap;

import ro.fys.nupcrawler.dao.def.IUserDAO;
import ro.fys.nupcrawler.dao.impl.UserDAO;
import ro.fys.nupcrawler.entities.UserType;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.services.constants.ActionResponseType;

/**
 * 
 * This service is dealing with security actions.
 */
public class SecurityService {

    IUserDAO dao  = new UserDAO();


    /**
     * Has as input an object that contains only user and pass. The scope is to do verify if existent user or not.
     * 
     * @param user The current user details
     * 
     * @return the level of authentication for the current user.
     */
    public HashMap<String, Object> checkAuthentication(User user){
        HashMap<String, Object> response = new HashMap<String, Object>();
        String inputUserName = user.getName();
        String inputUserPassword = user.getPassword();
        
        User userDB = dao.findByType("power");
        
        if(userDB == null){
            
//          user.setUserType("power");
//          dao.save(user);
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "No power user on this database.");
            
            return response;    
        }
        
        
        if(inputUserName == null || !inputUserName.equals(userDB.getName())){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user is invalid.");
            return response;
        }
        
        if(inputUserPassword == null || !inputUserPassword.equals(userDB.getPassword())){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The password is invalid.");
            return response;
        }
        
        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user has been successfully loggin.");
        return response;
    }

}
