/*
 * AccountService.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services;

import java.util.*;

import ro.fys.nupcrawler.dao.def.IUserDAO;
import ro.fys.nupcrawler.dao.impl.UserDAO;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.services.constants.ActionResponseType;

/**
 * 
 * This service is dealing with users accounts actions.
 */
public class AccountService {

    /**
     * Authenticate an user using the input credentials from the login form
     */
    
    public User authenticateUser(User user){
        
        return new UserDAO().authenticate(user);
    }
    
    /**
     * The database object for the user table.
     */
    private IUserDAO dao =  new UserDAO();

    /**
     * The add user service method.
     * 
     * @param user The user to be added in the system.
     * 
     * @return List of status, message and id.
     */
    public HashMap<String, Object> addUser(User user){
        HashMap<String, Object> response = new HashMap<String, Object>();

        ArrayList<User> identicalUserList = dao.findByName(user.getName());
        if(identicalUserList != null && identicalUserList.size() > 0){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user [" + user.getName() + "] is already registered in the system.");
            return response;
        }

        ArrayList<User> identicalEmailList = dao.findByEmail(user.getEmail());
        if(identicalEmailList != null && identicalEmailList.size() > 0){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The email [" + user.getEmail() + "] is already registered in the system.");
            return response;
        }

        Long responseID = dao.save(user);
        if(responseID == 0){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The add operation for the ]" + user.getName() + "] user failed.");
            return response;
        }

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user [" + user.getName() + "] has been successfully added.");
        response.put(ActionResponseType.ACTION_ID_KEY, responseID);
        return response;

    }

    /**
     * The change User Password service method.
     * 
     * @param user The user to be updated in the system.
     * 
     * @return List of status, message and id.
     */
    public HashMap<String, Object> changeUserPassword(User user){
        HashMap<String, Object> response = new HashMap<String, Object>();
        if(user.getPassword() == null || user.getPassword().trim().length() == 0){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user password is null or empty.");
            return response;
        }

        User user2update = dao.findByID(user.getId());
        if(user2update == null){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user [" + user.getName() + "] has an invalid ID.");
            return response;
        }

        dao.saveorupdate(user);
        
        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user password has been successfully updated.");
        return response;
    }

    /**
     * The delete user service method.
     * 
     * @param user The user to be deleted from the system.
     * 
     * @return List of status, message and id.
     */
    public HashMap<String, Object> deleteUser(Long id){
        HashMap<String, Object> response = new HashMap<String, Object>();

        int responseID = dao.delete(id);
        if(responseID == ActionResponseType.ACTION_FAILED){
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user delete operation has failed because of database issues.");
            return response;
        }

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The user has been successfully deleted.");
        return response;
    }

    /**
     * Retrieve all the current users.
     * 
     * @return the list of users.
     */
    public ArrayList<User> getAllUsers(){

        return dao.getAll(); 
    }
}
