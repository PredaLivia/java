/*
 * TopicsService.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ro.fys.nupcrawler.dao.def.ITopicDAO;
import ro.fys.nupcrawler.dao.impl.DummySchedulerDAO;
import ro.fys.nupcrawler.dao.impl.TopicDAO;
import ro.fys.nupcrawler.entities.beans.Page;
import ro.fys.nupcrawler.entities.beans.Scheduler;
import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.services.constants.ActionResponseType;

/**
 * 
 * This service is dealing with Topics actions.
 */
public class TopicsService {

    ITopicDAO dao = new TopicDAO();

    public HashMap<String, Object> addTopic(Topic topic) {
        HashMap<String, Object> response = new HashMap<String, Object>();

        ArrayList<Topic> identicalUserList = dao.findByName(topic.getName(), topic.getUser().getId());
        if (identicalUserList != null && identicalUserList.size() > 0) {
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The topic [" + topic.getName()
                    + "] is already registered in the system for this user.");
            return response;
        }

        Long responseID = dao.save(topic);
        if (responseID == 0) {
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The add operation for the [" + topic.getName()
                    + "] topic failed.");
            return response;
        }

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The topic [" + topic.getName()
                + "] has been successfully added.");
        response.put(ActionResponseType.ACTION_ID_KEY, responseID);
        return response;
    }

    public HashMap<String, Object> deleteTopic(Long id) {
        HashMap<String, Object> response = new HashMap<String, Object>();

        int responseID = dao.delete(id);
        if (responseID == ActionResponseType.ACTION_FAILED) {
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY,
                    "The topic delete operation has failed because of database issues.");
            return response;
        }

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The keyword has been successfully deleted.");
        return response;
    }

    public HashMap<String, Object> updateTopic(Topic topic) {
        HashMap<String, Object> response = new HashMap<String, Object>();

        Topic topic2update = dao.findByID(topic.getId());
        if (topic2update == null) {
            response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_FAILED);
            response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The topic [" + topic.getName()
                    + "] has an invalid ID.");
            return response;
        }

        dao.saveorupdate(topic);

        response.put(ActionResponseType.ACTION_RESPONSE_KEY, ActionResponseType.ACTION_SUCCESS);
        response.put(ActionResponseType.ACTION_MESSAGE_KEY, "The topic has been successfully updated.");
        return response;
    }

    public ArrayList<Topic> getAllTopicsForUser(Long userID) {

        return dao.getAll(userID);
    }
    
    public ArrayList<Topic> getAllTopicsForUserAndQuery(Long userID, String title){

        return dao.getAllAndQuery(userID, title);
    }

    public List<Topic> getTopicsToRun(Long userID) {
        List<Topic> topicsToRun = null;

        Scheduler scheduler = null;
        
        if (userID != null) {
            topicsToRun = new ArrayList<Topic>();
            
            // Save of dummy scheduler object in order to force the loading of Scheduler 
            // objects from the database
            new DummySchedulerDAO().refresh();
            
            // Get the list of topics for current user
            ArrayList<Topic> userTopics = new TopicsService().getAllTopicsForUser(userID);
            if (userTopics != null && userTopics.size() > 0) {
                // Iterate over each topic to see if it must be run now
                Topic currentTopic = null;
                for (Iterator<Topic> it = userTopics.iterator(); it.hasNext();) {
                    currentTopic = it.next();
                    Set<Scheduler> schedulers = currentTopic.getSchedulers();
                    // if we have schedulers defined the check if the current
                    // date is after scheduler date
                    if (schedulers != null && schedulers.size() > 0) {
                        Date schedulerDate = null;
                        
                        for (Iterator<Scheduler> itSch = schedulers.iterator(); itSch.hasNext();) {
                            scheduler = itSch.next();
                            if (scheduler.getActive() == 1) {
                                schedulerDate = scheduler.getDate();
                                // Get current time
                                Calendar schedulerCalendar = Calendar.getInstance();
                                schedulerCalendar.setTime(schedulerDate);
                                // If current time more than scheduler time
                                if (Calendar.getInstance().after(schedulerCalendar)) {
                                    topicsToRun.add(currentTopic);
                                    // TODO Update scheduler entry for this
                                    // topic - set inactive
                                    scheduler.setActive(0);
                                    new SchedulerService().updateScheduler(scheduler);
                                }
                            }
                        }
                    }
                }
            }
        }

        return topicsToRun;
    }

    public ArrayList<Page> showMostRelevantPages(Long topicID) {

        // return new PageDAO().sortByRelevance(topicID);
        return null;
    }

    public Topic getTopicInfo(Long id) {

        return dao.findByID(id);
    }
}
