/*
 * HtmlParser.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

/**
 * Replace with your class description.
 */
public class HtmlParser extends HTMLEditorKit.ParserCallback {

    /**
     * The desired string is stored in this accumulator.
     * @uml.property  name="accumulator"
     */
    private StringBuffer accumulator = null;

    /**
     * Specifies whether the firstString or secondString is to be extracted. It is also used to tell the parser to used the text extraction method.
     * @uml.property  name="methodType"
     * @uml.associationEnd  
     */
    private HtmlParserMethodTypeEnum methodType = null;

    /**
     * Default constructor.
     */
    public HtmlParser() {
    }

    /**
     * Parse the content.
     * 
     * @param in
     *            Input string as Reader.
     * @throws IOException
     *             In case the parse method exits with an exception.
     */
    public void parse(Reader in) throws IOException {
        accumulator = new StringBuffer();
        // Make a delegator
        ParserDelegator delegator = new ParserDelegator();
        // The third parameter is TRUE to ignore charset directive
        delegator.parse(in, this, Boolean.TRUE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.swing.text.html.HTMLEditorKit.ParserCallback#handleEndTag(javax
     * .swing.text.html.HTML.Tag, int)
     */
    @Override
    public void handleEndTag(Tag arg0, int arg1) {
        switch (this.methodType) {
        case TAG_FIRST_CHAR:
            accumulator.append(arg0.toString().charAt(0));
            break;

        case TAG_LAST_CHAR:
            accumulator.append(arg0.toString().charAt(arg0.toString().length() - 1));
            break;

        default:

        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.swing.text.html.HTMLEditorKit.ParserCallback#handleSimpleTag(javax
     * .swing.text.html.HTML.Tag, javax.swing.text.MutableAttributeSet, int)
     */
    @Override
    public void handleSimpleTag(Tag arg0, MutableAttributeSet arg1, int arg2) {
        switch (this.methodType) {
        case TAG_FIRST_CHAR:
            accumulator.append(arg0.toString().charAt(0));
            break;

        case TAG_LAST_CHAR:
            accumulator.append(arg0.toString().charAt(arg0.toString().length() - 1));
            break;

        default:

        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.swing.text.html.HTMLEditorKit.ParserCallback#handleStartTag(javax
     * .swing.text.html.HTML.Tag, javax.swing.text.MutableAttributeSet, int)
     */
    @Override
    public void handleStartTag(Tag arg0, MutableAttributeSet arg1, int arg2) {
        switch (this.methodType) {
        case TAG_FIRST_CHAR:
            accumulator.append(arg0.toString().charAt(0));
            break;

        case TAG_LAST_CHAR:
            accumulator.append(arg0.toString().charAt(arg0.toString().length() - 1));
            break;

        default:

        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.swing.text.html.HTMLEditorKit.ParserCallback#handleText(char[],
     * int)
     */
    @Override
    public void handleText(char[] arg0, int arg1) {
        switch (this.methodType) {
        case TEXT:
            accumulator.append(arg0);
            break;

        default:

        }
    }

    /**
     * Retrieves the first string for the page structure comparing algorithm.
     * 
     * @param contentToParse
     * @return
     * @throws IOException
     */
    public String getFirstString(String contentToParse) throws IOException {
        String firstString = null;

        methodType = HtmlParserMethodTypeEnum.TAG_FIRST_CHAR;

        parse(new StringReader(contentToParse));

        firstString = accumulator.toString();

        return firstString;
    }

    /**
     * Retrieves the second string for the page structure comparing algorithm.
     * 
     * @param contentToParse
     * @return
     * @throws IOException
     */
    public String getSecondString(String contentToParse) throws IOException {
        String secondString = null;

        methodType = HtmlParserMethodTypeEnum.TAG_LAST_CHAR;

        parse(new StringReader(contentToParse));

        secondString = accumulator.toString();

        return secondString;
    }

    /**
     * Retrieves the text for the text content changes detection algorithm.
     * 
     * @param contentToParse
     * @return
     * @throws IOException
     */
    public String getText(String contentToParse) throws IOException {
        String textContent = null;

        methodType = HtmlParserMethodTypeEnum.TEXT;

        parse(new StringReader(contentToParse));

        textContent = accumulator.toString();

        return textContent;
    }

}
