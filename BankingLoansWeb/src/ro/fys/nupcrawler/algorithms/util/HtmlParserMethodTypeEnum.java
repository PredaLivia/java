/*
 * HtmlParserMethodTypeEnum.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.util;

/**
 * Replace with your class description.
 */
public enum HtmlParserMethodTypeEnum {
    TAG_FIRST_CHAR, TAG_LAST_CHAR, TEXT
}
