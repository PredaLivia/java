/*
 * MathUtils.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.util;

import java.text.DecimalFormat;

/**
 * Replace with your class description.
 */
public class MathUtils {

    public static double roundSixDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.######");
        return Double.valueOf(twoDForm.format(d));
    }

    /**
     * 
     * 
     * @param array
     * @return
     */
    public static int[][] toIntegerArray(byte[][] array) {
        int dimension = (int) (array.length / Math.pow(2, 2));
        int[][] returnValue = new int[dimension][dimension];

        String tempValue = "";
        int k = 0;
        int l = 0;

        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array.length; i++) {
                if (array[j][i] == 0) {
                    tempValue += "0";
                } else if (array[j][i] == 1) {
                    tempValue += "1";
                }
                if ((i + 1) % 16 == 0) {
                    returnValue[k][l] = Integer.parseInt(tempValue, 2);
                    if (l + 1 == dimension) {
                        l = 0;
                        k++;
                    } else {
                        l++;
                    }
                        
                    tempValue = "";
                }
            }
        }

        return returnValue;
    }

    /**
     * Computes the determinant of an array.
     * 
     * @param arr
     * @return
     */
    public static long determinant(int[][] arr) {
        long result = 0;
        if (arr.length == 1) {
            result = arr[0][0];
            return result;
        }
        if (arr.length == 2) {
            result = arr[0][0] * arr[1][1] - arr[0][1] * arr[1][0];
            return result;
        }
        for (int i = 0; i < arr[0].length; i++) {
            int temp[][] = new int[arr.length - 1][arr[0].length - 1];

            for (int j = 1; j < arr.length; j++) {
                for (int k = 0; k < arr[0].length; k++) {

                    if (k < i) {
                        temp[j - 1][k] = arr[j][k];
                    } else if (k > i) {
                        temp[j - 1][k - 1] = arr[j][k];
                    }
                }
            }
            result += arr[0][i] * Math.pow(-1, (int) i) * determinant(temp);
        }
        return result;
    }

}
