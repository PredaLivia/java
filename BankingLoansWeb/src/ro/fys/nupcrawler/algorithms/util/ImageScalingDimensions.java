/*
 * ImageScalingDimensions.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.util;

/**
 * Replace with your class description.
 */
public enum ImageScalingDimensions {
    
    
    SCALE_16(16), 
    SCALE_32(32),
    SCALE_64(64);
    
    int dim = 0;
    
    private ImageScalingDimensions(int dim) {
        this.dim = dim;
    }
    
    public int getValue() {
        return dim;
    }
}
