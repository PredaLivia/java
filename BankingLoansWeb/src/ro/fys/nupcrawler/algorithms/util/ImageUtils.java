/*
 * ImageUtils.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * Replace with your class description.
 */
public class ImageUtils {
    
    /**
     * Scales an image to the provided dimension.
     * @param location The URL of the image.
     * @param width The width of the scaled image.
     * @param height The height of the scaled image.
     * @return The scaled image as a buffer.
     * @throws Exception
     */
    public static BufferedImage scale(URL location, int dim) throws Exception {

        BufferedImage bsrc = ImageIO.read(location);

        BufferedImage bdest = new BufferedImage(dim, dim, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bdest.createGraphics();
        AffineTransform at = AffineTransform.getScaleInstance((double) dim / bsrc.getWidth(),
                (double) dim / bsrc.getHeight());
        g.drawRenderedImage(bsrc, at);

        return bdest;

    }

    /**
     * Apply black and white filter to the image. The sepia filter is applied first and then 
     * a threshold is used to transform the image into a black and white image.
     * @param img
     * @param sepiaIntensity
     */
    public static void applyBlackAndWhiteFilter(BufferedImage img, int threshold) {
        // Play around with this. 20 works well and was recommended
        // by another developer. 0 produces black/white image
        int sepiaDepth = 0;

        int w = img.getWidth();
        int h = img.getHeight();

        WritableRaster raster = img.getRaster();

        // We need 3 integers (for R,G,B color values) per pixel.
        int[] pixels = new int[w * h * 3];
        raster.getPixels(0, 0, w, h, pixels);

        // Process 3 ints at a time for each pixel.
        // Each pixel has 3 RGB colors in array
        for (int i = 0; i < pixels.length; i += 3) {
            int r = pixels[i];
            int g = pixels[i + 1];
            int b = pixels[i + 2];

            int gry = (r + g + b) / 3;
            r = g = b = gry;
            r = r + (sepiaDepth * 2);
            g = g + sepiaDepth;

            if (r > 255)
                r = 255;
            if (g > 255)
                g = 255;
            if (b > 255)
                b = 255;

            // normalize if out of bounds
            if (b < 0)
                b = 0;
            if (b > 255)
                b = 255;

            // if values more or equal to defined threshold then set color to
            // WHITE
            if (r >= threshold)
                r = 255;
            if (g >= threshold)
                g = 255;
            if (b >= threshold)
                b = 255;

            // if values less than defined threshold then set color to BLACK
            if (r < threshold)
                r = 0;
            if (g < threshold)
                g = 0;
            if (b < threshold)
                b = 0;

            pixels[i] = r;
            pixels[i + 1] = g;
            pixels[i + 2] = b;

        }

        raster.setPixels(0, 0, w, h, pixels);

    }

    /**
     * 
     * @param bufferedImage
     * @param x
     * @param y
     */
    public static Color getPixelColor(BufferedImage bufferedImage, int x, int y) {
        Color pixelColor = null;

        int pixel = bufferedImage.getRGB(x, y);

        int red = (pixel & 0x00ff0000) >> 16;
        int green = (pixel & 0x0000ff00) >> 8;
        int blue = (pixel & 0x000000ff);

        pixelColor = new Color(red, green, blue);

        return pixelColor;
    }

    /**
     * 
     * 
     * @param imageLocation
     * @param dim
     * @return
     * @throws Exception
     */
    public static byte[][] imageToBinaryArray(URL imageLocation, ImageScalingDimensions imageScaleDim) throws Exception {
        // Get the value for the scaling factor.
        int dim = imageScaleDim.getValue();
        
        // 1. Scale image
        BufferedImage bufferedImage = ImageUtils.scale(imageLocation, dim);
        //BufferedImage bufferedImage = ImageIO.read(imageLocation);
        
        // 2. Transform to black and white (sepia filter and defined threshold 120)
        ImageUtils.applyBlackAndWhiteFilter(bufferedImage, 120);
        
        ImageIO.write(bufferedImage, "JPG", new File("e:/testAAA.jpg"));
        
        // 3. Read image as an width*height array with each value being either 0 or 1.
        byte[][] returnValue = new byte[dim][dim];
        for(int i=0;i<bufferedImage.getWidth();i++) {
            for(int j=0;j<bufferedImage.getHeight();j++) {
                Color pixelColor = ImageUtils.getPixelColor(bufferedImage, i, j);
                if(pixelColor.equals(Color.black)) {
                    returnValue[i][j] = 0;
                } else if(pixelColor.equals(Color.white)) {
                    returnValue[i][j] = 1;
                }
                
            }
        }
        
        return returnValue;
    }

    /**
     * 
     * 
     * @param imageLocation
     * @param scaleDimension
     * @return
     * @throws Exception 
     */
    public static long computeIval(URL imageLocation, ImageScalingDimensions imageScaleDim) throws Exception {
        long iVal = 0;
       
        byte[][] twoToneImage = ImageUtils.imageToBinaryArray(imageLocation, imageScaleDim);
        
        int[][] reducedArray =  MathUtils.toIntegerArray(twoToneImage);
        
        iVal = MathUtils.determinant(reducedArray);
        
        return iVal;
        
    }
    
    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        try {
            URL imageURL = new URL("http://pozedesktop.com/data/media/13/Flower.jpg");

            ImageUtils.imageToBinaryArray(imageURL, ImageScalingDimensions.SCALE_64);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
