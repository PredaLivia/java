/*
 * ContentUtil.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.util;

import java.io.IOException;

/**
 * Replace with your class description.
 */
public class ContentUtil {
    
    /**
     * Returns the text content of the page (without tags).
     * 
     * @param pageContent
     *            The content of the page.
     * @return The text content without tags.
     * @throws IOException
     *             In case the page content cannot be parsed by the parser.
     */
    public static String getText(String pageContent) throws IOException {
        String text = null;

        HtmlParser parser = new HtmlParser();

        text = parser.getText(pageContent);

        return text;

    }
    
    /**
     * Returns the first string containing characters appearing at first
     * position in the tag for all tags in the order they appear in the page.
     * 
     * @param pageContent
     *            The content of the page.
     * @return The first string.
     * @throws IOException
     *             In case the page content cannot be parsed by the parser.
     */
    public static String getFirstString(String pageContent) throws IOException {
        String firstString = null;

        HtmlParser parser = new HtmlParser();

        firstString = parser.getFirstString(pageContent);

        return firstString;
    }
    
    /**
     * Returns the second string containing characters appearing at last
     * position in the tag for all tags in the order they appear in the page.
     * 
     * @param pageContent
     *            The content of the page.
     * @return The second string.
     * @throws IOException
     *             In case the page content cannot be parsed by the parser.
     */
    public static String getSecondString(String pageContent) throws IOException {
        String secondString = null;

        HtmlParser parser = new HtmlParser();

        secondString = parser.getSecondString(pageContent);

        return secondString;

    }
    
}
