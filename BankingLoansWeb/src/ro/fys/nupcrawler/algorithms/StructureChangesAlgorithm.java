/*
 * StructureChangesAlgorithm.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms;

import java.io.IOException;

import ro.fys.nupcrawler.algorithms.util.*;

/**
 * Replace with your class description.
 */
public class StructureChangesAlgorithm {
    
    /**
     * Determines if the page structure has changed.
     * 
     * @param newContent
     *            The new page content.
     * @param storedFirstString
     *            The previous computed fist string.
     * @param storedSecondString
     *            The previous computed second string.
     * @return true if the page structure has been modified, false otherwise.
     * @throws IOException
     *             In case the page content cannot be parsed by the parser.
     */
    public boolean pageStructureHasChanged(String newContent, String storedFirstString, String storedSecondString)
            throws IOException {
        boolean isStructureChanged = true;

        // extract first string = characters appearing at first position in the
        // tag for all tags in the order they appear
        String firstString = ContentUtil.getFirstString(newContent);
        if (firstString != null) {
            if (storedFirstString != null && storedFirstString.equals(firstString)) {
                // extract second string = characters at the last position in a
                // tag for all tags in the order they appear
                String secondString = ContentUtil.getSecondString(newContent);
                if (secondString != null) {
                    if (storedSecondString != null && storedSecondString.equals(secondString)) {
                        isStructureChanged = false;
                    }
                }
            }
        }

        return isStructureChanged;

    }
    
}
