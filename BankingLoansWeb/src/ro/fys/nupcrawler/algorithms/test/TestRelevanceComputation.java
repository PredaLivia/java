/*
 * RelevanceComputation.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.test;

import java.util.ArrayList;
import java.util.List;

import ro.fys.nupcrawler.algorithms.RelevanceAlgorithm;
import ro.fys.nupcrawler.entities.beans.Keyword;

/**
 * Replace with your class description.
 */
public class TestRelevanceComputation {

    /**
     * @param args
     */
    public static void main(String[] args) {

        Keyword k1 = new Keyword();
        k1.setName("Test case");
        k1.setWeight(0.25);

        Keyword k2 = new Keyword();
        k2.setName("Test coverage");
        k2.setWeight(0.4);

        Keyword k3 = new Keyword();
        k3.setName("Test suite");
        k3.setWeight(0.25);

        Keyword k4 = new Keyword();
        k4.setName("Test oracle");
        k4.setWeight(0.10);
        
        List<Keyword> keywordList = new ArrayList<Keyword>();
        keywordList.add(k1);
        keywordList.add(k2);
        keywordList.add(k3);
        keywordList.add(k4);

        String content = "Test case Test coverage Test suite Test oracle Test coverage Test case Test case Test case Test case Test coverage Test coverage Test coverage Test coverage Test coverage Test suite Test suite Test suite Test suite Test suite Test suite Test suite Test suite Test oracle Test suite Test oracle Test oracle";

        double contribution = new RelevanceAlgorithm().computeContributionForKeyword(
                "Test case", 0.25, 26, content);

        System.out.format("Contribution for keyword (%s) = %2.3f%n", "Test case", contribution);

        contribution = new RelevanceAlgorithm().computeContributionForKeyword("Test coverage",
                0.4, 26, content);

        System.out.format("Contribution for keyword (%s) = %2.3f%n", "Test coverage", contribution);

        contribution = new RelevanceAlgorithm().computeContributionForKeyword("Test suite",
                0.25, 26, content);

        System.out.format("Contribution for keyword (%s) = %2.3f%n", "Test suite", contribution);

        contribution = new RelevanceAlgorithm().computeContributionForKeyword("Test oracle",
                0.10, 26, content);

        System.out.format("Contribution for keyword (%s) = %2.3f%n", "Test oracle", contribution);

         int frequency = new RelevanceAlgorithm().getTopicEffectiveFrequency(keywordList, content);
        
         System.out.println("Total effective words = " + frequency);
        
         double relevanceScore = new RelevanceAlgorithm().computeRelevanceScore(keywordList, content);
        
         System.out.format("Relevance score = %2.3f%n", relevanceScore);

    }

}
