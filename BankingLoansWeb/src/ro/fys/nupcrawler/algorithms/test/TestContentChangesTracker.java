/*
 * ContentChangesTracker.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.test;

import java.io.*;

import ro.fys.nupcrawler.algorithms.*;
import ro.fys.nupcrawler.algorithms.util.*;
import ro.fys.nupcrawler.text.utils.TextUtils;

/**
 * Replace with your class description.
 */
public class TestContentChangesTracker {
    /**
     * Main method used for testing.
     * 
     * @param args
     */
    public static void main(String[] args) {

        StringBuffer pageContent = new StringBuffer();

        String storedFirstString = "hhttlllsssslllsssshbbnddndddddddddaadddsssaassaassaassssaassaassaassaasddfdidfddbdaadddulaallaallaallaallaaluddulaaludddddhaahdddddtttdiaassaaaadbbddttddpppccppppccpppddaaaaaadtttdaassaadttddssddaiaddaabssssssssssssssssssssddbddtttdttttttdttttttttdtttddaaddhhdaaaaaadddaadtttdiaassaassdttdpaappccpdtttdaassaadttddssddaiaddaabssssssssssssssssssssddtttttttttdttttttsstttttttdssaassssdttttttdsaccasaassssdttttttdssaassssssdtttdtttdddaadtttdiaassaadttdpppccppaapdtttdaassaadttddssddaiaddaabssssssssssssssssssssddtttttttttdtttttdssaassssdtttdtttdaadtttdiaassaadttdpaapdtttdaassaadttddssddaiaddaabssssssssssssssssssssddtttttttttdtttttdssaassssdttttttdssaassssdtttdtttdaadtttdiaassaadttdpccccccppccppccppaappeepdtttdaassaadttddassaddddbddttddssddaiaddaabssssssssssssssddtttttttttdttttttttdtttdaadtttdiaassaadttdpccccppaappccppccpdtttdaassaadttddssddaiaddaabssssssssssssssddtttttttttdttttttttdtttdaadtttdiaassaadttdpppccpdtttdaassaadttddssddssddbddtttttttttdttttttttdtttdaadtttdiaassaadttdpppccpdtttdaassaadttddassaddddbddttddssddaiaddaabssssssssddtttttttttdttttttttdtttdaadtttdiaassaadttdpppccpdtttdaassaadttddssddaiaddaabssssssssddtttttttttdttttttttdtttdaadtttdiaassaadttdpppccpppdtttdaassaadttddssddaiaddaabssssssssddtttttttttdttttttttdtttdaadtttdiaassaadttdpppppccpdtttdaassaadttddssddaiaddaabssssssssssssssddtttttttttdttttttttdtttdaadtttdiaassaadttdpppccppaapppdtttdaassaadttddassaddaiaddaabssssssssssssssddttddssddaiaddaabssssssssddtttttttttdtttttdssaassssdtttdtttdaadtttdiaassaadttdpppccppppccpdtttdaassaadttddassaddaiaddaabssssssssssssssddttddssddaiaddaabssddtttttttttdttttttttdtttdaadtttdiaassaadttdpppccpdtttdaassaadttddssddaiaddaabssssssssddtttttttttdttttttttdtttdaafhhssddddttddddddddddiidddtttaassttddddttdlliddllissddllidtttddidfhaaaaaaaahddddhhdpbbppaaaapdddppdaassbaassbaassbdtttppttpbbpttttppttpbbpttttppttpbaabptttdssdddssddhhddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddddhhddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddaaddddddassaddssndidndddddddaaaaaaaaaaaaaaaabaabbaabdssaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaassaadddaaaaddddaaaadddndidnssssbh";
        String storedSecondString = "ldeekkkttttkkkttttdyrtvvtvvvvvvvvvaavvvnnnaannaannaannnnaannaannaannaanvvmvtvmvvrvaavvvliaaiiaaiiaaiiaaiiaailvvliaailvvvvv1aa1vvvvverdvtaannaaaavbbvvddvvppeeeeppeeeeppvvaaaaaaverdvaannaavddvvnnvvagavvaarnnnnnnnnnnnnnnnnnnnnvvrvvdrevdrrdddverddddrevdrevvaavv22vaaaaaavvvaaverdvtaannaannvddvpaapeeeeverdvaannaavddvvnnvvagavvaarnnnnnnnnnnnnnnnnnnnnvvdredrrdddverderdnndddreddvnnaannnnvdrrdddvnaeeanaannnnvdrrdddvnnaannnnnnvdrevdrevvvaaverdvtaannaavddvppeeeepaapverdvaannaavddvvnnvvagavvaarnnnnnnnnnnnnnnnnnnnnvvdredrrdddverdddvnnaannnnvdrevdrevaaverdvtaannaavddvpaapverdvaannaavddvvnnvvagavvaarnnnnnnnnnnnnnnnnnnnnvvdredrrdddverdddvnnaannnnvdrrdddvnnaannnnvdrevdrevaaverdvtaannaavddvpeeeeeepeeeepeeppaappmmpverdvaannaavddvvannavvvvrvvddvvnnvvagavvaarnnnnnnnnnnnnnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvpeeeeppaapeeeepeepverdvaannaavddvvnnvvagavvaarnnnnnnnnnnnnnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvppeeeeverdvaannaavddvvnnvvnnvvrvvdredrrdddverddddrevdrevaaverdvtaannaavddvppeeeeverdvaannaavddvvannavvvvrvvddvvnnvvagavvaarnnnnnnnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvppeeeeverdvaannaavddvvnnvvagavvaarnnnnnnnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvppeeeeppverdvaannaavddvvnnvvagavvaarnnnnnnnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvppppeeeeverdvaannaavddvvnnvvagavvaarnnnnnnnnnnnnnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvppeeeepaapppverdvaannaavddvvannavvagavvaarnnnnnnnnnnnnnnvvddvvnnvvagavvaarnnnnnnnnvvdredrrdddverdddvnnaannnnvdrevdrevaaverdvtaannaavddvppeeeeppeeeeverdvaannaavddvvannavvagavvaarnnnnnnnnnnnnnnvvddvvnnvvagavvaarnnvvdredrrdddverddddrevdrevaaverdvtaannaavddvppeeeeverdvaannaavddvvnnvvagavvaarnnnnnnnnvvdredrrdddverddddrevdrevaam22ttvvvvaavvvvvvvvvvttvvverdaattddvvvvddvlltvvlltnnvvlltvdrevvtvm2aaaaaaaa2vvvv44vpbbppaaaapvvvppvaannraannraannrverdppddpbbpdrrdppddpbbpdrrdppddpbaabpdrevttvvvttvv44vvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvvv44vvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvaavvvvvvannavvtttvgvtvvvvvvvaaaaaaaaaaaaaaaabaabbaabvnnaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaannaavvvaaaavvvvaaaavvvtvgvtttttyl";

        try {

            FileReader in = new FileReader("java-new.html");

            int len = -1;
            char[] buf = new char[8 * 2048];

            while ((len = in.read(buf, 0, 256)) != -1) {
                pageContent.append(new String(buf, 0, len));
            }

            in.close();

            System.out.println("First string: " + ContentUtil.getFirstString(pageContent.toString()));
            System.out.println("Second string: " + ContentUtil.getSecondString(pageContent.toString()));

            boolean pageHasChanged = new StructureChangesAlgorithm().pageStructureHasChanged(
                    pageContent.toString(), storedFirstString, storedSecondString);
            System.out.println("Page has changed: " + pageHasChanged);

        } catch (IOException ioe) {
            System.err.println("Error reading from Reader :" + ioe.getMessage());
        }

        String textContent = " March 26, 2007 issue - The stereotype of the \"dumb jock\" has never sounded right to Charles Hillman. A jock himself, he plays hockey four times a week, but when he isn't body-checking his opponents on the ice, he's giving his mind a comparable workout in his neuroscience and kinesiology lab at the University of Illinois. Nearly every semester in his classroom, he says, students on the women's cross-country team set the curve on his exams. So recently he started wondering if there was a vital and overlooked link between brawn and brains�if long hours at the gym could somehow build up not just muscles, but minds.";
        int asciiSum = TextUtils.computeAsciiSum(textContent);
        System.out.println("Sum of ASCII codes: " + asciiSum);
        System.out.println("Total character count: " + textContent.length());
        System.out.println("Distinct character count: " + TextUtils.getDistinctCharacterCount(textContent));

        String modifiedContent = " March 26, 2007 issue - The stereotype of the \"dumb jock\" has never sounded right to Charles Hillman. A jock himself, he used to play hockey five times a week, but when he isn't body-checking his opponents on the ice, he's giving his mind a comparable workout in his neuroscience and kinesiology lab at the University of Florida. Nearly every semester in his classroom, he says, students on the women's cross-country team set the curve on his exams. So recently he started wondering if there was a vital and overlooked link between brawn and brains�if long hours at the gym could somehow build up not just muscles, but minds.";
        asciiSum = TextUtils.computeAsciiSum(modifiedContent);
        System.out.println("Sum of ASCII codes: " + asciiSum);
        System.out.println("Total character count: " + modifiedContent.length());

        try {
            textContent = ContentUtil.getText(textContent);
            asciiSum = TextUtils.computeAsciiSum(textContent);
            double oldCode = (double) asciiSum / (double) TextUtils.getDistinctCharacterCount(textContent);
            oldCode = MathUtils.roundSixDecimals(oldCode);

            System.out.println("Text content has changed: "
                    + new ContentChangesAlgorithm().pageTextContentHasChanged(modifiedContent,
                            oldCode));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
