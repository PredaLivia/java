/*
 * TestImageChangesAlgorithm.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ro.fys.nupcrawler.algorithms.ImageChangesAlgorithm;
import ro.fys.nupcrawler.algorithms.util.ImageScalingDimensions;
import ro.fys.nupcrawler.entities.beans.Image;

/**
 * Replace with your class description.
 */
public class TestImageChangesAlgorithm {
    
    /**
     * Main method.
     * 
     * @param args
     */
    public static void main(String[] args) {
        
        List<String> imageUrls = new ArrayList<String>();
        imageUrls.add("http://www.wallpapere.eu/data/media/12/www.wallpapere.eu-Andrea_Andrades_Wallpapers_Collection_03.jpg");
        imageUrls.add("http://www.wallpapers-free.tk/wp-content/uploads/Free-wallpapers-2.jpg");
        imageUrls.add("http://www.webwallpapers.net/wp-content/uploads/2009/08/romantic-vista_wallpapers_5718_1600x1200.jpg");
        imageUrls.add("http://www.golesdelmadrid.com/wp-content/uploads/2010/08/real-madrid-wallpaper2010.jpg");
        
        try {
            List<Image> imageList = new ImageChangesAlgorithm().generateImagesMagicNumber(imageUrls, ImageScalingDimensions.SCALE_32);
            Image image = null;
            for(Iterator<Image> it = imageList.iterator(); it.hasNext(); ) {
                image = it.next();
                System.out.println("Image url = " + image.getUrl() + "; ival = " + image.getNumber());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
