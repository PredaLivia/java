/*
 * PValue.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms;

import java.util.ArrayList;

/**
 * Replace with your class description.
 */
public class PValueAlgorithm {

    /**
     * 
     * @param childrenUrls
     * @param findByName
     * @return
     */
    public int compute(ArrayList<String> childrenUrls, int bkwLinkNumber) {

        if(childrenUrls == null || bkwLinkNumber == 0){
            if(childrenUrls == null && bkwLinkNumber != 0){
                return -bkwLinkNumber;
            }
            if(childrenUrls != null && bkwLinkNumber == 0 ){
                return childrenUrls.size();
            }
            
            return 0;
        }
        
        int fwdLinkNumber = childrenUrls.size();
       
        return fwdLinkNumber - bkwLinkNumber;
    }

}
