/*
 * ContentChangesTracker.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms;

import java.io.IOException;

import ro.fys.nupcrawler.algorithms.util.ContentUtil;
import ro.fys.nupcrawler.algorithms.util.MathUtils;
import ro.fys.nupcrawler.text.utils.TextUtils;

/**
 * Replace with your class description.
 */
public class ContentChangesAlgorithm {

	/**
	 * Determines if the text content has changed.
	 * 
	 * @param newContent
	 *            The new page content.
	 * @param oldCode
	 *            The previously computed text code.
	 * @return true if the text content has changed, false otherwise.
	 * @throws IOException
	 *             In case the page content cannot be parsed by the parser.
	 */
	public boolean pageTextContentHasChanged(String newContent,
			double oldCode) throws IOException {
		boolean isTextContentChanged = true;

		if (newContent != null) {

			String textContent = ContentUtil.getText(newContent);

			double texContentCode = getPageRelevanceScore(textContent);

			if ((oldCode - texContentCode) == 0.0D) {
				isTextContentChanged = false;
			}

		}

		return isTextContentChanged;
	}

	/**
	 * Computes page relevance score of a page.
	 * 
	 * @param pageContent
	 *            The content of the page.
	 * @return The relevance score of the page.
	 */
	public double getPageRelevanceScore(String pageContent) {
		double textContentCode = 0.00;

		if (pageContent != null) {
			// compute the new content code
			int textContentAsciiSum = TextUtils.computeAsciiSum(pageContent);
			int textContentDistinctCharacterCount = TextUtils
					.getDistinctCharacterCount(pageContent);

			if (textContentDistinctCharacterCount > 0) {
				textContentCode = (double) textContentAsciiSum
						/ (double) textContentDistinctCharacterCount;
				textContentCode = MathUtils.roundSixDecimals(textContentCode);
			}
		}

		return textContentCode;
	}

}
