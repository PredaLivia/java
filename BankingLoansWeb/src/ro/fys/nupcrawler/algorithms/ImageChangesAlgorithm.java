/*
 * ImageChangesAlgorithm.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import ro.fys.nupcrawler.algorithms.util.ImageScalingDimensions;
import ro.fys.nupcrawler.algorithms.util.ImageUtils;
import ro.fys.nupcrawler.entities.beans.Image;

/**
 * Replace with your class description.
 */
public class ImageChangesAlgorithm {
    
    /**
     * Logger for this class.
     */
    Logger logger =  Logger.getLogger(ImageChangesAlgorithm.class);

    /**
     * 
     * 
     * @param content
     * @return
     */
    public List<Image> generateImagesMagicNumber(List<String> imageUrls, ImageScalingDimensions imageScalingDim) throws Exception{
        List<Image> pageImages = new ArrayList<Image>();
        
        if(imageUrls != null && imageUrls.size() > 0) {
            // compute the ival for every image
            String imageUrl = null;
            for(Iterator<String >it = imageUrls.iterator(); it.hasNext(); ) {
                imageUrl = it.next();
                logger.debug("Processing image url: " + imageUrl);
                if(imageUrl != null) {
                    Image imageBean = new Image();
                    imageBean.setUrl(imageUrl);
                    long ival = ImageUtils.computeIval(new URL(imageUrl), imageScalingDim);
                    imageBean.setNumber(ival);
                    
                    pageImages.add(imageBean);
                }
            }
            
        }
        
        return pageImages;
    }

    /**
     * 
     * 
     * @param newContent
     * @param images
     * @return
     */
    public List<Image> compareImagesChanges(String newContent, List<Image> images) {

        return null;
    }
    
}
