/*
 * RelevanceComputation.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.algorithms;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ro.fys.nupcrawler.algorithms.util.Constants;
import ro.fys.nupcrawler.entities.beans.Keyword;

/**
 * Replace with your class description.
 */
public class RelevanceAlgorithm {

    /**
     * Computes the contribution of web page in result domain.
     * 
     * @param keyword
     *            The keyword or phrase of a topic T.
     * @param keywordWeight
     *            The weight of the keyword/phrase in topic T.
     * @param topicEffectiveFrequency
     *            The sum of occurrences for every keyword (from topic T) in the
     *            content.
     * @param content
     *            The effective content (e.g. web page content).
     * @return The computed contribution.
     */
    public double computeContributionForKeyword(String keyword, double keywordWeight,
            int topicEffectiveFrequency, String content) {
        double contribution = 0.000;

        if (keywordWeight > Constants.MAX_WEIGHTS_SUM) {
            throw new IllegalArgumentException("Illegal weigth value : " + keywordWeight + ". It must be lower than "
                    + Constants.MAX_WEIGHTS_SUM + ".");
        }

        if (content != null && content.length() > 0) {
            // compute the occurrences of keyword in the content
            int keywordOccurenceNumber = StringUtils.countMatches(content.toLowerCase(), keyword.toLowerCase());
            if (keywordOccurenceNumber > 0) {
                contribution = (double) ((double) keywordOccurenceNumber / (double) topicEffectiveFrequency)
                        * keywordWeight;
            }
        }

        return contribution;
    }

    /**
     * Computes the relevance score of a page depending on a topic T.
     * 
     * @param topic
     *            The topic T.
     * @param content
     *            The content of the page (e.g. a web page).
     * @return The computed relevance score.
     */
    public double computeRelevanceScore(List<Keyword> topicKeywords, String content) {
        double relevanceScore = 0.000;

        double sum = 0.000;

        if (content != null && content.length() > 0) {

            Iterator<Keyword> keywordListIterator = topicKeywords.iterator();
            Keyword keyword = null;
            while(keywordListIterator.hasNext()) {
                keyword = keywordListIterator.next();
                if(keyword != null) {
                    sum += (double) computeContributionForKeyword(keyword.getName(), keyword.getWeight(),
                            getTopicEffectiveFrequency(topicKeywords, content), content);
                }
            }

            relevanceScore = sum;
        }

        return relevanceScore;
    }

    /**
     * Computes the sum of all topic T keyword number of occurrences in a
     * content.
     * 
     * @param topic
     *            the topic T.
     * @param content
     *            The content of the page (e.g. a web page).
     * @return The computed sum of occurrences.
     */
    public int getTopicEffectiveFrequency(List<Keyword> topicKeywords, String content) {
        int topicEffectiveFrequency = -1;

        int sum = 0;

        if (content != null && content.length() > 0) {
            // sum the number of every keyword occurrence in the content
            Iterator<Keyword> keywordListIterator = topicKeywords.iterator();
            Keyword keyword = null;
            while(keywordListIterator.hasNext()) {
                keyword = keywordListIterator.next();
                if(keyword != null) {
                    sum += StringUtils.countMatches(content.toLowerCase(), keyword.getName().toLowerCase());
                }
            }

            topicEffectiveFrequency = sum;
        }

        return topicEffectiveFrequency;
    }

}
