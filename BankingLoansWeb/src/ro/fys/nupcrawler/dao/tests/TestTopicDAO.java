/*
 * TestTopicDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.tests;

import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.entities.beans.User;

public class TestTopicDAO {

    public TestTopicDAO() {

    }

    public static void save() {

        Topic topic = new Topic();

        topic.setName("Topic2");
        User user = new User();
        user.setName("mstddddd");
        topic.setUser(user);

        new GeneralDAO().saveTopic(topic);

    }

    public static void main(String args[]) {
        TestTopicDAO.save();
    }

}
