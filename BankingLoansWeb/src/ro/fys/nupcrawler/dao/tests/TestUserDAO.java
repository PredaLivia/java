/*
 * TestUserDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.tests;

import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.tests.UserDAO;

public class TestUserDAO {

    public TestUserDAO() {

    }

    public static void testUser() {
        User user = new User();
        user.setName("marius");
        new GeneralDAO().saveUser(user);

    }

    public static void main(String args[]) {
        TestUserDAO.testUser();
    }

}
