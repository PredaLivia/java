/*
 * GeneralDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.tests;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import ro.fys.nupcrawler.entities.beans.BkwLinkMap;
import ro.fys.nupcrawler.entities.beans.Keyword;
import ro.fys.nupcrawler.entities.beans.Page;
import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

public class GeneralDAO {

    /**
     * @uml.property  name="log"
     * @uml.associationEnd  multiplicity="(1 1)"
     */
    private Logger log = Logger.getLogger(GeneralDAO.class);

    public GeneralDAO() {

    }

    /**
     * Save an User object in DB
     * 
     * @param object
     */
    public void saveUser(User object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getName());

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Session1 " + session);
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        log.debug("Obiectul User a fost salvat!");
    }

    /**
     * Save a Topic object in DB
     * 
     * @param object
     */
    public void saveTopic(Topic object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getName());

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Session1 " + session);
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        log.debug("Obiectul Topic a fost salvat!");
    }

    /**
     * Save a Keyword object in DB
     * 
     * @param object
     */
    public void saveKeyword(Keyword object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getName());

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Session1 " + session);
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        log.debug("Obiectul Keyword a fost salvat!");
    }

    /**
     * Save a Page object in DB
     * 
     * @param object
     */
    public void savePage(Page object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getUrl());

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Session1 " + session);
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        log.debug("Obiectul Page a fost salvat!");
    }

    /**
     * Save an BkwLink object in DB
     * 
     * @param object
     */
    public void saveBkwLink(BkwLinkMap object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getUrl());

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Session1 " + session);
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        log.debug("Obiectul BkwLink a fost salvat!");
    }
}
