/*
 * IScheduledDateDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.def;

import java.util.ArrayList;
import java.util.Date;

import ro.fys.nupcrawler.entities.beans.Scheduler;

/**
 * Replace with your class description.
 */
public interface ISchedulerDAO {
    public int delete(Long id);
    public Scheduler findLowerThanDate(Date date);
    public ArrayList<Scheduler> findById(Long id);
    public Long save(Scheduler scheduledDate);
    public void saveorupdate(Scheduler scheduledDate);
}
