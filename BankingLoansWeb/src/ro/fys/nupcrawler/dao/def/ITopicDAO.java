/*
 * ITopicDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.def;

import java.util.ArrayList;

import ro.fys.nupcrawler.entities.beans.*;

/**
 * Replace with your class description.
 */
public interface ITopicDAO {

    public Long save(Topic topic);
    public int delete(Long id);
    public ArrayList<Topic> getAll(Long userId);
    public ArrayList<Topic> getAllAndQuery(Long userId, String title);
    public ArrayList<Topic> findByName (String name, Long userID);
    /**
     * 
     * @param id
     * @return
     */
    public Topic findByID(Long id);
    /**
     * 
     * @param topic
     * @return
     */
    public void saveorupdate(Topic topic);
 
}
