/*
 * IUserDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.def;

import java.util.ArrayList;

import ro.fys.nupcrawler.entities.beans.User;

/**
 * Replace with your class description.
 */
public interface IUserDAO {

    public int delete(Long userId) ;

    public ArrayList<User> findByEmail(String userEmail);
    
    public User findByID(Long id);

    public ArrayList<User> findByName(String userName);

    public ArrayList<User> getAll();

    public Long save(User user);
    
    public User authenticate(User user);

    /**
     * 
     * @param user
     */
    public void saveorupdate(User user);

    /**
     * 
     * @param string
     */
    public User findByType(String type);

}
