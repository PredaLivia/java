package ro.fys.nupcrawler.dao.def;

import ro.fys.nupcrawler.entities.beans.Page;

public interface IPageDAO {

    public int delete(Long id);

    public Page findByID(Long id);

    public Long save(Page page);

    public void saveorupdate(Page page);

}
