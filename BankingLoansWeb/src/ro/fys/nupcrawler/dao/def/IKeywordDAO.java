package ro.fys.nupcrawler.dao.def;

import ro.fys.nupcrawler.entities.beans.Keyword;

public interface IKeywordDAO{

    public int delete(Long id) ;

    public Keyword findByID(Long id);

    public Long save(Keyword keyword);

    public void saveorupdate(Keyword keyword);

}
