/*
 * ISeedDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.def;

import ro.fys.nupcrawler.entities.beans.*;

/**
 * Replace with your class description.
 */
public interface ISeedDAO {
    public int delete(Long seedID) ;
    public Long save(Seed seed);
    public Seed findByID(Long id);
    public void saveorupdate(Seed seed);
}
