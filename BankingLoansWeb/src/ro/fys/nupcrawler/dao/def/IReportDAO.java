/*
 * IReportsDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.def;

import java.util.ArrayList;

import ro.fys.nupcrawler.entities.beans.*;

/**
 * Replace with your class description.
 */
public interface IReportDAO {
    public Long save(Report report);
    public ArrayList<Report> getAllByUserAndTopic(Long userID, Long topicID);
}
