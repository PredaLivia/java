/*
 * PageDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.Iterator;

import org.hibernate.*;

import ro.fys.nupcrawler.dao.def.IPageDAO;
import ro.fys.nupcrawler.entities.beans.Page;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class PageDAO implements IPageDAO{

    public int delete(Long id) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            String hql = "delete from Page com where com.id =?";
            Query query = session.createQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }

    public Page findByID(Long id) {
        Page object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from Page com where com.id= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (Page) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }

    public Long save(Page page) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(page);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.IPageDAO#saveorupdate(ro.fys.nupcrawler.entities.beans.Page)
     */
    public void saveorupdate(Page page) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(page);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
    }
}
