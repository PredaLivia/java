/*
 * SeedDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.Iterator;

import org.hibernate.*;

import ro.fys.nupcrawler.dao.def.ISeedDAO;
import ro.fys.nupcrawler.entities.beans.*;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class SeedDAO implements ISeedDAO{

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.ISeedDAO#delete(java.lang.Long)
     */
    public int delete(Long seedID) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            String hql = "delete from Seed com where com.seedID=?";
            Query query = session.createQuery(hql);
            query.setParameter(0, seedID);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.ISeedDAO#findByID(java.lang.Long)
     */
    public Seed findByID(Long id) {
        Seed object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from Seed com where com.seedID=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (Seed) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.ISeedDAO#save(ro.fys.nupcrawler.entities.beans.Seed)
     */
    public Long save(Seed seed) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(seed);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.ISeedDAO#saveorupdate(ro.fys.nupcrawler.entities.beans.Seed)
     */
    public void saveorupdate(Seed seed) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(seed);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
    }

}
