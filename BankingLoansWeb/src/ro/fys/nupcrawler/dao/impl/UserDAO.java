/*
 * UserDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.*;

import org.hibernate.*;

import ro.fys.nupcrawler.dao.def.IUserDAO;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

public class UserDAO implements IUserDAO {

    public int delete(Long userId) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();
            
            tx = session.beginTransaction();
            String hql = "delete from User com where com.id =?";
            Query query = session.createQuery(hql);
            query.setParameter(0, userId);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }

    public ArrayList<User> findByEmail(String userEmail) {
        ArrayList<User> objectList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from User com where com.email=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, userEmail);
            objectList = (ArrayList<User>) query.list();

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return objectList;
    }

    public User findByID(Long id) {
        User object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from User com where com.id= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (User) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }

    public ArrayList<User> findByName(String userName) {
        ArrayList<User> objectList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from User com where com.name=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, userName);
            objectList = (ArrayList<User>) query.list();

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return objectList;
    }

    public ArrayList<User> getAll() {
        ArrayList<User> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from User com";
            Query query = session.createQuery(SQL_QUERY);

            objCreated = (ArrayList<User>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    public Long save(User user) {
        Long newID = 0L;
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            newID = (Long) session.save(user);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }

        return newID;
    }

    public void saveorupdate(User user) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(user);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }

    }
    
    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.IUserDAO#findByType(java.lang.String)
     */
    public User findByType(String type) {
        User object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from User com where com.userType= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, type);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (User) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }
    
    public static void main(String [] args){
        User user = new User();
        user.setEmail("11");
        user.setName("22");
        user.setPassword("33");
        
        
        System.out.println(new UserDAO().save(user));
        System.out.println("merge: " + new UserDAO().getAll().get(0).getEmail());
      //  System.out.println("delete: " + new UserDAO().delete(1L));
        System.out.println("find: " + new UserDAO().findByEmail("11").get(0).getName());
        System.out.println("find: " + new UserDAO().findByName("22").get(0).getPassword());
        System.out.println("find: " + new UserDAO().findByID(2L).getName());
        
        user.setId(2L);
        new UserDAO().saveorupdate(user);
        System.out.println("find: " + new UserDAO().findByID(2L).getName());

    }

    public User authenticate(User user) {
        User userAuth = new User();
        Session session = HibernateUtil.getSessionFactory().openSession();
    try {
            
            String SQL_QUERY = "from User com where com.name=? and com.password=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, user.getName());
            query.setParameter(1, user.getPassword());
            ArrayList<User> objectList = (ArrayList<User>) query.list();

            if (objectList.size() > 0){
                userAuth = objectList.get(0);
            } 

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }


        
        return userAuth;
    }



}
