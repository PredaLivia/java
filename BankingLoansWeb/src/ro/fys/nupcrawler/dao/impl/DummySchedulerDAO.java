package ro.fys.nupcrawler.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import ro.fys.nupcrawler.dao.def.IDummySchedulerDAO;
import ro.fys.nupcrawler.entities.beans.Scheduler;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

public class DummySchedulerDAO implements IDummySchedulerDAO{

	public void refresh() {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

            String SQL_QUERY = "from Scheduler sched where sched.id=1";
            Query query = session.createQuery(SQL_QUERY);
            List<Scheduler> schedulerList = query.list();
            session.close();
            Scheduler scheduler = schedulerList.get(0);
            new SchedulerDAO().saveorupdate(scheduler);
            
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
	}

}
