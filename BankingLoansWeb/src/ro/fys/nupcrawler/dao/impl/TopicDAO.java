/*
 * TopicDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ro.fys.nupcrawler.dao.def.ITopicDAO;
import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class TopicDAO implements ITopicDAO {

    /**
     * Delete a topic.
     */
    public int delete(Long id) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            
            // Delete from  page_keyword
            String hql = "delete from page_keyword where TOPIC_ID = ?";
            Query query = session.createSQLQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            
            // Delete from report
            hql = "delete from report where topic = ?";
            query = session.createSQLQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            
            // Delete from topic_keyword
            hql = "delete from topic_keyword  where TOPIC_ID = ?";
            query = session.createSQLQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            
            // Delete from topic_scheduler
            hql = "delete from topic_scheduler  where TOPIC_ID = ?";
            query = session.createSQLQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            
            hql = "delete from Topic com where com.id =?";
            query = session.createQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            tx.commit();
            

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }

    /**
     * Return all the topics which contains the name string in the topic name.
     */
    @SuppressWarnings("unchecked")
    public ArrayList<Topic> findByName(String name) {
        ArrayList<Topic> topics = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Topic com where com.name like %?%";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, name);

            topics = (ArrayList<Topic>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return topics;
    }

    /**
     * Returns all the topics for an user.
     */
    @SuppressWarnings("unchecked")
    public ArrayList<Topic> getAll(Long userId) {

        ArrayList<Topic> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Topic com where com.user.id=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, userId);

            objCreated = (ArrayList<Topic>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }
    
    public ArrayList<Topic> getAllAndQuery(Long userId, String title) {

        ArrayList<Topic> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Topic com where com.user.id=? AND com.name LIKE '%" + title + "%'";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, userId);


            objCreated = (ArrayList<Topic>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    /**
     * Save topic.
     */
    public Long save(Topic object) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

         try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    /**
     * Find topic by name for a certain user.
     */
    public ArrayList<Topic> findByName(String name, Long userID) {
        ArrayList<Topic> topics = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from Topic com where com.name like %?% and com.user.id=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, name);
            query.setParameter(1, userID);

            topics = (ArrayList<Topic>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return topics;
    }

    /**
     * Find topic by id.
     */
    public Topic findByID(Long id) {
        Topic object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from Topic com where com.id= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator<?> it = query.iterate(); it.hasNext();) {
                object = (Topic) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }

   /**
    * Save or Update topic.
    */
    public void saveorupdate(Topic topic) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(topic);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
    }
}
