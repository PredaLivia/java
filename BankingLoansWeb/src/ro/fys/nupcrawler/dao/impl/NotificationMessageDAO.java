package ro.fys.nupcrawler.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ro.fys.nupcrawler.dao.def.INotificationMessageDAO;
import ro.fys.nupcrawler.entities.message.NotificationMessage;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

public class NotificationMessageDAO implements INotificationMessageDAO{

    public int delete(String message, Long userId) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();
            
            tx = session.beginTransaction();
            String hql = "delete from notificationmessage com where com.notificationMessageTitle =? and com.userid=?";
            Query query = session.createQuery(hql);
            query.setParameter(0, message);
            query.setParameter(1, userId);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }

    @SuppressWarnings("rawtypes")
    public NotificationMessage findMessage(String message, Long userId) {
        NotificationMessage objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from notificationmessage com where com.notificationMessageTitle =? and com.userid=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, message);
            query.setParameter(1, userId);
            
            for (Iterator it = query.iterate(); it.hasNext();) {
                objCreated = (NotificationMessage) query.list();
            }
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<NotificationMessage> getAll() {
        ArrayList<NotificationMessage> objCreated = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from notificationmessage com";
            Query query = session.createQuery(SQL_QUERY);

            objCreated = (ArrayList<NotificationMessage>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objCreated;
    }

    public Long save(NotificationMessage message) {
        Long newID = 0L;
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            newID = (Long) session.save(message);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }

        return newID;
    }

    public void saveorupdate(NotificationMessage message) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(message);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        
    }

}
