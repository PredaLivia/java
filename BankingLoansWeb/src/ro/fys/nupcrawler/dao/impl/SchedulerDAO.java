/*
 * ScheduledDateDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ro.fys.nupcrawler.dao.def.ISchedulerDAO;
import ro.fys.nupcrawler.entities.beans.Scheduler;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class SchedulerDAO implements ISchedulerDAO{

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.IScheduledDateDAO#delete(java.lang.Long)
     */
    public int delete(Long id) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            String hql = "delete from ScheduledDate com where com.id =?";
            
            Query query = session.createQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.IScheduledDateDAO#findLowerThanDate(java.util.Date)
     */
    public Scheduler findLowerThanDate(Date date) {
        return null;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.IScheduledDateDAO#save(ro.fys.nupcrawler.entities.beans.ScheduledDate)
     */
    public Long save(Scheduler scheduledDate) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(scheduledDate);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }
    
    /**
     * Save or Update scheduler.
     */
     public void saveorupdate(Scheduler scheduler) {
         Session session = null;
         Transaction tx = null;

         try {
             session = HibernateUtil.getSessionFactory().openSession();
             tx = session.beginTransaction();
             session.saveOrUpdate(scheduler);
             tx.commit();
         } catch (Exception e) {
             if (tx != null) {
                 try {
                     tx.rollback();
                 } catch (Exception e1) {
                     System.out.println(e1);
                 }
             }
         } finally {
             session.flush();
             session.close();
         }
     }
     
     @SuppressWarnings("unchecked")
	public ArrayList<Scheduler> findById(Long id) {
    	 ArrayList<Scheduler> schedulers = null;
         Session session = HibernateUtil.getSessionFactory().openSession();

         try {

             String SQL_QUERY = "from Scheduler scheduler where scheduler.id = ?";
             Query query = session.createQuery(SQL_QUERY);
             query.setParameter(0, id);

             schedulers = (ArrayList<Scheduler>) query.list();
             session.close();

         } catch (HibernateException e1) {
             System.out.println(e1.getMessage());
         } finally {
         }
         return schedulers;
     }

}
