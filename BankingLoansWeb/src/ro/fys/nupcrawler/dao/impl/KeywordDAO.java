/*
 * KeywordDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.*;

import org.hibernate.*;

import ro.fys.nupcrawler.dao.def.IKeywordDAO;
import ro.fys.nupcrawler.entities.beans.*;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class KeywordDAO implements IKeywordDAO{
    
    public int delete(Long id) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            String hql = "delete from Keyword com where com.id =?";
            Query query = session.createQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }
    
    public Keyword findByID(Long id) {
        Keyword object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from Keyword com where com.id= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (Keyword) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }

    public Long save(Keyword keyword) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

         try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(keyword);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

   /**
    * 
    */
    public void saveorupdate(Keyword keyword) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(keyword);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
    }
}
