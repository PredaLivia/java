/*
 * BkwLinkDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.*;

import org.hibernate.*;

import ro.fys.nupcrawler.dao.def.IBkwLinkDAO;
import ro.fys.nupcrawler.entities.beans.*;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class BkwLinkDAO implements IBkwLinkDAO{

    public int delete(Long id) {
        int row = 0;
        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            String hql = "delete from BkwLinkMap com where com.id =?";
            
            Query query = session.createQuery(hql);
            query.setParameter(0, id);
            row = query.executeUpdate();
            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

        return row;
    }
    
    public BkwLinkMap findByID(Long id) {
        BkwLinkMap object = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from BkwLinkMap com where com.id= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (BkwLinkMap) it.next();
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }

        return object;
    }

    public Long save(BkwLinkMap bkwLink) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(bkwLink);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }

    /* (non-Javadoc)
     * @see ro.fys.nupcrawler.dao.def.IBkwLinkDAO#saveorupdate(ro.fys.nupcrawler.entities.beans.BkwLinkMap)
     */
    public void saveorupdate(BkwLinkMap bkwLink) {
        Session session = null;
        Transaction tx = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(bkwLink);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
    }
}
