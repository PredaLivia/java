/*
 * ReportsDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.dao.impl;

import java.util.ArrayList;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.*;

import ro.fys.nupcrawler.dao.def.IReportDAO;
import ro.fys.nupcrawler.entities.beans.*;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

/**
 * Replace with your class description.
 */
public class ReportDAO implements IReportDAO{
    
    public Long save(Report report) {
        Session session = null;
        Transaction tx = null;
        Long result = 0L;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            result = (Long) session.save(report);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        return result;
    }
    
    public ArrayList<Report> getAllByUserAndTopic(Long userID, Long topicID) {
        ArrayList<Report> reports = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            
            String SQL_QUERY = "from Report com where com.user.id=? AND com.topic.id=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, userID);
            query.setParameter(1, topicID);

            reports = (ArrayList<Report>) query.list();
            session.close();

        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        
        return reports;
    }
    
    
}
