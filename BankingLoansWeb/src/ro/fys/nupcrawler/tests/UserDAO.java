package ro.fys.nupcrawler.tests;

import java.util.ArrayList;
import java.util.Iterator;
/*
 * UserDAO.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.hibernate.HibernateUtil;

public class UserDAO {

    public UserDAO() {

    }

    public void delete(String username) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            tx = session.beginTransaction();
            String hql = "delete from User com where com.username =?";
            Query query = session.createQuery(hql);
            query.setParameter(0, username);
            int row = query.executeUpdate();
            if (row == 0) {
                System.out.println("Deleted operation failed due an unknown cause!");
            } else {
                System.out.println("Deleted Row: " + row);
            }

            tx.commit();

        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e.getMessage());
                }
            }
        } finally {
            session.flush();
            session.close();

        }

    }

    public User findById(Long id) {

        User object = new User();
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Cautam userul cu id-ul " + id);
            String SQL_QUERY = "from User com where com.idUser= ?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, id);

            for (Iterator it = query.iterate(); it.hasNext();) {
                object = (User) it.next();
                System.out.println(object);
            }
            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return object;
    }

    public ArrayList<User> findByName(String name) {

        ArrayList<User> objectList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from User com where com.nume LIKE '%" + name + "%'";
            Query query = session.createQuery(SQL_QUERY);
            objectList = (ArrayList<User>) query.list();

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objectList;
    }

    public ArrayList<User> getAll() {
        ArrayList<User> objectList = null;
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String SQL_QUERY = "from User com";
            Query query = session.createQuery(SQL_QUERY);

            objectList = (ArrayList<User>) query.list();

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objectList;
    }

    public void save(User object) {

        boolean success = false;

        Session session = null;
        Transaction tx = null;

        System.out.println("Inainte de add " + object.getName());
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            System.out.println("Session1 " + session);
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                    System.out.println(e1);
                }
            }
        } finally {
            session.flush();
            session.close();
        }
        System.out.println("Obiectul User a fost salvat!");
    }

    public ArrayList findByUserPassword(String user, String password) {

        ArrayList<User> objectList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from User com where com.username=? and com.password=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, user);
            query.setParameter(1, password);
            objectList = (ArrayList<User>) query.list();

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return objectList;
    }

    public boolean findByUserName(String username) {

        ArrayList<User> objectList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        boolean isAuthenticated = false;

        try {

            String SQL_QUERY = "from User com where com.username=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, username);
            objectList = (ArrayList<User>) query.list();
            if (objectList.size() > 0) {
                isAuthenticated = true;
            }

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        return isAuthenticated;
    }

    public User findUserByUserName(String username) {

        ArrayList<User> objectList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {

            String SQL_QUERY = "from User com where com.username=?";
            Query query = session.createQuery(SQL_QUERY);
            query.setParameter(0, username);
            objectList = (ArrayList<User>) query.list();

            session.close();
        } catch (HibernateException e1) {
            System.out.println(e1.getMessage());
        } finally {
        }
        if (objectList.size() > 0) {
            return objectList.get(0);
        } else
            return null;

    }

}
