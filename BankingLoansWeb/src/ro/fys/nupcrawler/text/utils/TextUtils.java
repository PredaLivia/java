/*
 * TextUtils.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.text.utils;

import java.util.Vector;

/**
 * Replace with your class description.
 */
public class TextUtils {

    /**
     * Transform the text into a sum ascii codes of all characters in the text.
     * 
     * @param textContent
     * @return
     */
    public static int computeAsciiSum(String textContent) {
        int asciiSum = 0;
        byte current = 0;
        
        if (textContent != null) {
            for (byte c : textContent.getBytes()) {
            	current = (byte)c;
            	asciiSum +=  current;
            }
        }

        return asciiSum;
    }

    /**
     * Returns the number of occurrences of a character in a text.
     * 
     * @param character
     * @param textContent
     * @return
     */
    public static int getCharacterOccurrences(char character, String textContent) {
        int characterFrequency = 0;

        if (textContent != null) {
            for (int i = 0; i < textContent.length(); i++) {
                if (character == textContent.charAt(i)) {
                    characterFrequency++;
                }
            }
        }

        return characterFrequency;

    }

    /**
     * Returns the number of distinct characters from the content.
     * 
     * @param textContent
     *            The content.
     * @return the number of distinct characters.
     */
    public static int getDistinctCharacterCount(String textContent) {
        int distinctCharCount = 0;

        Vector<Character> distinctCharacters = new Vector<Character>();

        if (textContent != null) {
            for (char c : textContent.toCharArray()) {
                if (!distinctCharacters.contains(c)) {
                    distinctCharacters.add(c);
                    distinctCharCount++;
                }
            }
        }

        return distinctCharCount;
    }
}
