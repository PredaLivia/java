/*
 * TextUtils.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.text.utils.tests;

/**
 * Replace with your class description.
 */
public class TestTextUtils {
    
    /**
     * Main method used for testing.
     * @param args
     */
    public static void main(String[] args) {
            String textContent = "March 26, 2007 issue - The stereotype of the \"dumb jock\" has never sounded right to Charles Hillman. A jock himself, he plays hockey four times a week, but when he isn't body-checking his opponents on the ice, he's giving his mind a comparable workout in his neuroscience and kinesiology lab at the University of Illinois. Nearly every semester in his classroom, he says, students on the women's cross-country team set the curve on his exams. So recently he started wondering if there was a vital and overlooked link between brawn and brains�if long hours at the gym could somehow build up not just muscles, but minds.";
            int asciiSum = ro.fys.nupcrawler.text.utils.TextUtils.computeAsciiSum(textContent);
            System.out.println(textContent);
            System.out.println("Sum of ASCII character codes: " + asciiSum);
            System.out.println("Total character count: " + textContent.length());
            System.out.println("Distinct character count: " + ro.fys.nupcrawler.text.utils.TextUtils.getDistinctCharacterCount(textContent));
            
            System.out.println();
            
            String modifiedContent = "March 26, 2007 issue - The stereotype of the \"dumb jock\" has never sounded right to Charles Hillman. A jock himself, he used to play hockey five times a week, but when he isn't body-checking his opponents on the ice, he's giving his mind a comparable workout in his neuroscience and kinesiology lab at the University of Florida. Nearly every semester in his classroom, he says, students on the women's cross-country team set the curve on his exams. So recently he started wondering if there was a vital and overlooked link between brawn and brains�if long hours at the gym could somehow build up not just muscles, but minds.";
            asciiSum = ro.fys.nupcrawler.text.utils.TextUtils.computeAsciiSum(modifiedContent);
            
            System.out.println(modifiedContent);
            System.out.println("Sum of ASCII character codes: " + asciiSum);
            System.out.println("Total character count: " + modifiedContent.length());
            System.out.println("Distinct character count: " + ro.fys.nupcrawler.text.utils.TextUtils.getDistinctCharacterCount(modifiedContent));
            
    }

}
