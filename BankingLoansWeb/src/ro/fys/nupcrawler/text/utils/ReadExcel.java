package ro.fys.nupcrawler.text.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;


public class ReadExcel {

	private String fileName = new String();
	private Vector headers = new Vector(); 
	private Vector values = new Vector(); 
	private Vector hashes = new Vector();
	private int rowsNumber;
	private int colNumber;
	
	public ReadExcel(){}
	
	public ReadExcel(String fileName) {
		
		this.fileName = fileName;
		
		try{
			readDocument();
		}
		catch (IOException ex){
			System.out.println(ex.getMessage());
		}
	}


	
	public void readDocument() throws IOException {
		//create a POIFSFileSystem object to read the data
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));

		//create a workbook out of the input stream
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		// get a reference to the worksheet
		HSSFSheet sheet = wb.getSheetAt(0);
		
		//read headers line
		HSSFRow headerRow = sheet.getRow(0);					
		colNumber = 0;
		while ( headerRow.getCell((short) colNumber) != null ){			
			headers.add(headerRow.getCell((short) colNumber));
			colNumber++;
		}
		
		// read values lines
		rowsNumber = 0; int col;
		while (sheet.getRow(rowsNumber + 1) != null){
			HSSFRow currentRow = sheet.getRow(rowsNumber + 1);
			col = 0;
			while ( currentRow.getCell((short) col) != null ){
				values.add(currentRow.getCell((short) col));
				col++;
			}
			rowsNumber++;			
		}
		
	
		// create hashes vector
		int i=0;
		LinkedHashMap h = new LinkedHashMap();
		
		do{
			
			//System.out.println(headers.elementAt(i%colNumber).toString() + " " + values.elementAt(i).toString());
			h.put(headers.elementAt(i%colNumber).toString(), values.elementAt(i).toString());
			
			i++;
			if ( i % colNumber == 0){
				hashes.add(h);
				
				h = new LinkedHashMap();
			}			
			
		}while(i<values.size());
		
	}
	
	
	public Vector getHeaders(){
		
		return headers;
	}
	
	
	public Vector getValues(){
		return values;
	}
	
	public Vector getHashes(){
		return hashes;
	}
	
	
	public int getColumnsNumber(){
		return colNumber;
	}
	
	public int getRowsNumber(){
		return rowsNumber;
	}
	
	/*
	 * return the value of a certain row for the header specified as key;
	 * getValueFor(1, "source_id") will return "id2" for the above example:
	 * 	source_id	source_name
		id1			name1
		id2			name2
		id3			name3

	 */
	public String getValueFor ( int rowIndex, String key) throws Exception{
		String val = new String();
	
		if ( rowIndex > rowsNumber || rowIndex <0 )
			throw new Exception("invalid row index");
		// search key in headers 
		int pos = -1;
		for ( int i = 0 ; i < headers.size(); i++)
			if ( key.equals(headers.elementAt(i).toString())){
				pos = i;
			}
		if ( pos == -1)
			throw new Exception("key not found in headers list");
		
		val = values.elementAt(rowIndex * headers.size() + pos).toString();
		
		return val;
	}
	
	public LinkedHashMap getRowAtIndex(int index){
		//System.out.println("iNTRA,M");
		LinkedHashMap h = new LinkedHashMap();
		for (int i =0; i<headers.size(); i++){
			//System.out.println("iNTRAM2");
			h.put(headers.elementAt(i), values.elementAt(colNumber*index+ i));
			//System.out.println(headers.elementAt(i) + ": " + values.elementAt(colNumber*index+ i));
		}
		

		
		return h;
	}
	
}
