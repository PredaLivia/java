package ro.fys.nupcrawler.text.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public class WriteExcel {

	private String fileName = new String();	
	private Vector<String> headers = new Vector<String>(); 
	private Vector<Object> values = new Vector<Object>(); 
	private Vector<HashMap<String,Object>> hashes = new Vector<HashMap<String,Object>>();
	private int rowsNumber;

	private HSSFWorkbook workBook = new HSSFWorkbook();
	private HSSFCell headerCell[] = null;
	
	private final int CHAR_LIMIT = 32000;	
	
	public WriteExcel(Vector<HashMap<String,Object>> hashes, String fileName) {
		int index = 1;
		while (new File(fileName).exists()) {
			String oldName = index==1 ? ".xls" : index + ".xls";
			fileName = fileName.replace(oldName, "") + (++index) + ".xls";
		}
		this.fileName = fileName;
		
		if (hashes == null || hashes.size() == 0){
			System.out.println(fileName + " not written - empty content!!!\n");
			return;
		} else {
			System.out.println("Writting " + fileName + "...");
		}
		// test
		this.hashes = hashes;
		
		
				
		try{
			createDocument();
			//System.out.println(fileName + " written !!!\n");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	
	public void createDocument () throws IOException{
		
		// create the folder for the results file - if it does not exist
		// FIXME
		File temp = new File(fileName);
		if (!temp.exists()){
			File folder = temp.getParentFile();
			if (folder!=null){
				folder.mkdirs();
			}
		}
		
		readHashes();
		writeFile();
		formatExcelFile();
	}
	
	
	private void readHashes(){
		HashMap<String,Object> hash = hashes.elementAt(0);				
		LinkedHashSet<String> set = new LinkedHashSet<String>(hash.keySet());

		for (Object key: set ) {
			Object obj = hash.get(key);			
			headers.add(key.toString());
			values.add(obj.toString());
		}		
		
		for (int i1 = 1; i1 < hashes.size(); i1++) {
			hash = (HashMap<String,Object>) hashes.elementAt(i1);
			
			for (Object key:set) {
				Object obj = hash.get(key);					
				if (obj!= null){
					values.add(obj.toString());
				}
			}
		}
		
		rowsNumber = hashes.size();
	}	
	
	
	@SuppressWarnings("deprecation")
	private void writeFile() throws IOException{
		
		//create a new worksheet
		HSSFSheet sheet = workBook.createSheet();	
		
		//		create a header row
		HSSFRow headerRow = sheet.createRow((short) 0);
		headerCell = new HSSFCell[headers.size()];
		
		for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++){
			headerCell[headerIndex] = headerRow.createCell((short) headerIndex);
			headerCell[headerIndex].setCellValue(headers.get(headerIndex).toString());
		}
		
		HSSFRow row[] = new HSSFRow[rowsNumber];
				
		for (int rowIndex = 0; rowIndex < rowsNumber; rowIndex++){
			row[rowIndex] = sheet.createRow((short) rowIndex + 1);
			
			//row[rowIndex].setHeight( (short) 0x349 );
			
			for ( int colIndex = 0 ; colIndex < headers.size(); colIndex++ )
			{
				HSSFCell cell = row[rowIndex].createCell((short) colIndex);
				cell.setEncoding(HSSFCell.ENCODING_UTF_16);
				
				int pos = rowIndex * headers.size() + colIndex;
				String value = "";
				if (pos < values.size()){
					Object cellValue = values.get(pos);
					value = cellValue.toString();		
				}
				
				if (value.length() <= CHAR_LIMIT){
				cell.setCellValue(value);	
				} else {
					System.err.println("Size problem at :" + (rowIndex+1) + ", " + (colIndex+1));
					System.err.println(value);
					cell.setCellValue("");
				}
			}
		}
				
		FileOutputStream stream = new FileOutputStream(fileName);
		workBook.write(stream);
		stream.close();
	}

	
	private void formatExcelFile() throws IOException{
		// set style		
		HSSFCellStyle style = workBook.createCellStyle();	    
		style.setWrapText(true);		
	    
	    // border
	    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
	    style.setBorderTop(HSSFCellStyle.BORDER_THIN);    
	    
	    // set font  
	    HSSFFont font = workBook.createFont();
	    font.setItalic(true);
	    font.setFontHeightInPoints((short)14);
	    font.setColor(HSSFColor.DARK_BLUE.index);
	    style.setFont(font);
	    
	    // Orange "foreground", foreground being the fill foreground not the font color.	    
	    style.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
	    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	    style.setFillBackgroundColor(HSSFColor.DARK_BLUE.index);
	    
		// apply style to headers
	    HSSFSheet sheet = workBook.getSheetAt(0);		
	    HSSFRow headerRow = sheet.getRow(0);
	    headerCell = new HSSFCell[headers.size()];
	    
		for (int headerIndex = 0; headerIndex < headers.size(); headerIndex++){
			headerCell[headerIndex] = headerRow.getCell((short)headerIndex);
			headerCell[headerIndex].setCellStyle(style);
		}
		
		HSSFCellStyle style1 = workBook.createCellStyle();
		style1.setWrapText(true);		
	    
	    // border
	    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
	    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
	    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
	    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);    
		
		// apply another style to cells
		for (int rowIndex = 0; rowIndex < rowsNumber; rowIndex++){
			HSSFRow row = sheet.getRow((short) rowIndex + 1);			
			for ( int colIndex = 0 ; colIndex < headers.size(); colIndex++ )
			{
				HSSFCell cell = row.getCell((short) colIndex);
				cell.setCellType( HSSFCell.CELL_TYPE_STRING );
				cell.setCellStyle(style1);
			}
		}
		
		//	adjust column width to fit the contents
		for (int i = 0; i < headers.size(); i++)
			sheet.autoSizeColumn((short)i);
				
		FileOutputStream stream = new FileOutputStream(fileName);
		workBook.write(stream);
		stream.close();	   
	}
	
	
	public Vector<String> getHeaders(){		
		return headers;
	}
	
	
	public Vector<Object> getValues(){
		return values;
	}
	
	public Vector<HashMap<String,Object>> getHashes(){
		return hashes;
	}
}
