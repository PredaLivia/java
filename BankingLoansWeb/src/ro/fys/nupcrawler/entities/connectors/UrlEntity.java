package ro.fys.nupcrawler.entities.connectors;

import java.util.ArrayList;

public class UrlEntity {
    
    /**
     * @uml.property  name="url"
     */
    private String url;
    /**
     * @uml.property  name="content"
     */
    private String content;
    /**
     * @uml.property  name="childrenUrls"
     * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.String"
     */
    private ArrayList<String> childrenUrls = new ArrayList<String>();
    
    /**
     * @uml.property  name="host"
     */
    private String host;
    
    /**
     * @return
     * @uml.property  name="host"
     */
    public String getHost(){
        return host;
    }
    /**
     * @param host
     * @uml.property  name="host"
     */
    public void setHost(String host){
        this.host = host;
    }
    
    public UrlEntity(){
    }

    /**
     * @return
     * @uml.property  name="url"
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     * @uml.property  name="url"
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return
     * @uml.property  name="content"
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     * @uml.property  name="content"
     */
    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getChildrenUrls() {
        return childrenUrls;
    }

    public void setChildrenUrls(ArrayList<String> childrenUrls) {
        this.childrenUrls = childrenUrls;
    }
}
