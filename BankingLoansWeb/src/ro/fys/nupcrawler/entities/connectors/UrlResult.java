package ro.fys.nupcrawler.entities.connectors;

public class UrlResult {

    /**
     * @uml.property  name="url"
     */
    private String url = new String();

    /**
     * @uml.property  name="title"
     */
    private String title = new String();
  
    /**
     * @uml.property  name="searchEngine"
     */
    private String searchEngine = new String();
    
    public UrlResult() {
    }

    /**
     * @return
     * @uml.property  name="url"
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     * @uml.property  name="url"
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return
     * @uml.property  name="searchEngine"
     */
    public String getSearchEngine() {
        return searchEngine;
    }

    /**
     * @param searchEngine
     * @uml.property  name="searchEngine"
     */
    public void setSearchEngine(String searchEngine) {
        this.searchEngine = searchEngine;
    }

    /**
     * @return
     * @uml.property  name="title"
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     * @uml.property  name="title"
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
}
