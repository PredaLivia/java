/*
 * Images.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

/**
 * Replace with your class description.
 */
public class Image {
  
    /**
     * The image ID.
     * @uml.property  name="imageID"
     */
    private Long imageID;
    
    /**
     * The image url.
     * @uml.property  name="url"
     */
    private String url = new String();

    /**
     * The image magic number.
     * @uml.property  name="weight"
     */
    private Long number;

    public Image() {

    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getNumber() {
        return number;
    }

    public void setImageID(Long imageID) {
        this.imageID = imageID;
    }

    public Long getImageID() {
        return imageID;
    }
}
