/*
 * Seed.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

/**
 * Replace with your class description.
 */
public class Seed {
    /**
     * The seed ID.
     * @uml.property  name="seedID"
     */
    private Long seedID;
    
    /**
     * The seed url.
     * @uml.property  name="url"
     */
    private String url = new String();

    public Seed() {

    }

    public void setSeedID(Long seedID) {
        this.seedID = seedID;
    }

    public Long getSeedID() {
        return seedID;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
