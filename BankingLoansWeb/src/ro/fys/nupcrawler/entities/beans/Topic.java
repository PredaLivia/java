/*
 * Topic.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

import java.util.*;

public class Topic {

    /**
     * The topic ID.
     * @uml.property  name="id"
     */
    private Long id;

    /**
     * The topic name.
     * @uml.property  name="name"
     */
    private String name = new String();

    /**
     * The update scheduler time step.
     * @uml.property  name="schedulerDate"
     */
    private Long schedulerDate;

    /**
     * The date of the last update of the topics records.
     * @uml.property  name="updateDate"
     */
    private Date updateDate = new Date();

    /**
     * The user that created the topic.
     * @uml.property  name="user"
     * @uml.associationEnd  multiplicity="(1 1)"
     */
    private User user = new User();

    /**
     * The level of deepness until were we should extract information.
     * @uml.property  name="deepnessLevel"
     */
    private int deepnessLevel;

    private Set<Keyword> keywords = new HashSet<Keyword>(0);
    private Set<Page> pages = new HashSet<Page>(0);
    private Set<Image> seeds = new HashSet<Image>(0);
    private Set<Scheduler> schedulers = new HashSet<Scheduler>(0);
    private String keywordsList = new String();
    
    public Topic() {

    }

    /**
     * @return
     * @uml.property  name="id"
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     * @uml.property  name="id"
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     * @uml.property  name="name"
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     * @uml.property  name="name"
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return
     * @uml.property  name="schedulerDate"
     */
    public Long getSchedulerDate() {
        return schedulerDate;
    }

    /**
     * @param schedulerDate
     * @uml.property  name="schedulerDate"
     */
    public void setSchedulerDate(Long schedulerDate) {
        this.schedulerDate = schedulerDate;
    }

    /**
     * @return
     * @uml.property  name="updateDate"
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     * @uml.property  name="updateDate"
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return
     * @uml.property  name="userId"
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     * @uml.property  name="user"
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return
     * @uml.property  name="deepnessLevel"
     */
    public int getDeepnessLevel() {
        return deepnessLevel;
    }

    /**
     * @param deepnessLevel
     * @uml.property  name="deepnessLevel"
     */
    public void setDeepnessLevel(int deepnessLevel) {
        this.deepnessLevel = deepnessLevel;
    }

    public void setSeeds(Set<Image> seeds) {
        this.seeds = seeds;
    }

    public Set<Image> getSeeds() {
        return seeds;
    }

    public void setKeywords(Set<Keyword> keywords) {
        this.keywords = keywords;
    }

    public Set<Keyword> getKeywords() {
        return keywords;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    public Set<Page> getPages() {
        return pages;
    }

    public String getKeywordsList() {
        return keywordsList;
    }

    public void setKeywordsList(String keywordsList) {
        this.keywordsList = keywordsList;
    }

    public void setSchedulers(Set<Scheduler> schedulers) {
        this.schedulers = schedulers;
    }

    public Set<Scheduler> getSchedulers() {
        return schedulers;
    }   
}
