/*
 * ScheduledDate.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

import java.util.Date;

/**
 * Replace with your class description.
 */
public class Scheduler {
    /**
     * The backward URL ID.
     * 
     * @uml.property name="id"
     */
    private Long id;

    /**
     * The backward url.
     * 
     * @uml.property name="url"
     */
    private Date date = new Date();

    /**
     * Tells whether the scheduler is active or not.
     */
    private int active = 0;

    public Scheduler() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return the active
     */
    public int getActive() {
        return active;
    }

}
