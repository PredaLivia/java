package ro.fys.nupcrawler.entities.beans;

import java.util.Date;

import com.icesoft.faces.context.Resource;

public class Report {
    
    /**
     * The report ID.
     * @uml.property  name="id"
     */
    private Long id;

    private String reportName = new String();
    private Date date = new Date();
    private String filePath = new String();
    private User user = new User();
    private Topic topic = new Topic();
    private Resource csvFile;
    
    public Report(){
        
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Resource getCsvFile() {
        return csvFile;
    }

    public void setCsvFile(Resource csvFile) {
        this.csvFile = csvFile;
    }
    
    
}
