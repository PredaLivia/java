/*
 * BkwLinkMap.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

public class BkwLinkMap {
    /**
     * The backward URL ID.
     * @uml.property  name="id"
     */
    private Long id;

    /**
     * The backward url.
     * @uml.property  name="url"
     */
    private String url = new String();

    public BkwLinkMap() {

    }

    /**
     * @return
     * @uml.property  name="id"
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     * @uml.property  name="id"
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     * @uml.property  name="url"
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     * @uml.property  name="url"
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
