/*
 * Page.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

import java.util.*;

public class Page {

    /**
     * The page ID.
     * @uml.property  name="id"
     */
    private Long id;

    /**
     * The page url.
     * @uml.property  name="url"
     */
    private String url = new String();

    /**
     * The page content.
     * @uml.property  name="content"
     */
    private String content = new String();

    /**
     * The page weight.
     * @uml.property  name="weight"
     */
    private Double weight;

    /**
     * The page pvalue.
     * @uml.property  name="pvalue"
     */
    private int pvalue;

    /**
     * The page contentNumber.
     * @uml.property  name="contentNumber"
     */
    private Double contentNumber;

    private String structureStart = new String();
    
    private String structureEnd = new String();

    /**
     * The page last update date.
     * @uml.property  name="updateDate"
     */
    private Date updateDate = new Date();
    
    private Set<Image> images = new HashSet<Image>(0);
    private Set<BkwLinkMap> bkwLinks = new HashSet<BkwLinkMap>(0);

    public Page() {

    }

    /**
     * @return
     * @uml.property  name="id"
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     * @uml.property  name="id"
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     * @uml.property  name="url"
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     * @uml.property  name="url"
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return
     * @uml.property  name="content"
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     * @uml.property  name="content"
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return
     * @uml.property  name="weight"
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight
     * @uml.property  name="weight"
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    /**
     * @return
     * @uml.property  name="pvalue"
     */
    public int getPvalue() {
        return pvalue;
    }

    /**
     * @param pvalue
     * @uml.property  name="pvalue"
     */
    public void setPvalue(int pvalue) {
        this.pvalue = pvalue;
    }

    /**
     * @return
     * @uml.property  name="contentNumber"
     */
    public Double getContentNumber() {
        return contentNumber;
    }

    /**
     * @param contentNumber
     * @uml.property  name="contentNumber"
     */
    public void setContentNumber(Double contentNumber) {
        this.contentNumber = contentNumber;
    }

    /**
     * @return
     * @uml.property  name="updateDate"
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     * @uml.property  name="updateDate"
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setStructureStart(String structureStart) {
        this.structureStart = structureStart;
    }

    public String getStructureStart() {
        return structureStart;
    }

    public void setStructureEnd(String structureEnd) {
        this.structureEnd = structureEnd;
    }

    public String getStructureEnd() {
        return structureEnd;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setBkwLinks(Set<BkwLinkMap> bkwLinks) {
        this.bkwLinks = bkwLinks;
    }

    public Set<BkwLinkMap> getBkwLinks() {
        return bkwLinks;
    }
}
