/*
 * User.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

import java.util.Date;

public class User {

    /**
     * The user ID.
     * @uml.property  name="id"
     */
    private Long id;

    /**
     * The user name.
     * @uml.property  name="name"
     */
    private String name = new String();

    /**
     * The user password.
     * @uml.property  name="password"
     */
    private String password = new String();
    
    /**
     * @uml.property  name="email"
     */
    private String email = new String();
    
    private String userType;
    
    private Long schedulerTimer;
    
    private Long oldnessFactor;
    
    private int topicCount;
    
    private Date creationDate = new Date();

    /**
     * The default constructor.
     */
    public User() {

    }

    /**
     * @return
     * @uml.property  name="id"
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     * @uml.property  name="id"
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     * @uml.property  name="name"
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     * @uml.property  name="name"
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return
     * @uml.property  name="password"
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     * @uml.property  name="password"
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return
     * @uml.property  name="email"
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     * @uml.property  name="email"
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public void setSchedulerTimer(Long schedulerTimer) {
        this.schedulerTimer = schedulerTimer;
    }

    public Long getSchedulerTimer() {
        return schedulerTimer;
    }

    public void setOldnessFactor(Long oldnessFactor) {
        this.oldnessFactor = oldnessFactor;
    }

    public Long getOldnessFactor() {
        return oldnessFactor;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public int getTopicCount() {
        return topicCount;
    }

    public void setTopicCount(int topicCount) {
        this.topicCount = topicCount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    
}
