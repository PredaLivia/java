/*
 * Keyword.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities.beans;

public class Keyword {

    /**
     * The keyword ID.
     * @uml.property  name="id"
     */
    private Long id;

    /**
     * The keyword name.
     * @uml.property  name="name"
     */
    private String name = new String();

    /**
     * The weight of the keyword.
     * @uml.property  name="weight"
     */
    private double weight;

    public Keyword() {

    }

    /**
     * @return
     * @uml.property  name="id"
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     * @uml.property  name="id"
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return
     * @uml.property  name="name"
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     * @uml.property  name="name"
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return
     * @uml.property  name="weight"
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight
     * @uml.property  name="weight"
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }
}
