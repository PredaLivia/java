package ro.fys.nupcrawler.entities.gui;

public class LeftMenuItem {

    private String menuItemName = new String();
    private String menuItemId = new String();
    
    public LeftMenuItem (){
        
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public String getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(String menuItemId) {
        this.menuItemId = menuItemId;
    }
    
    
    
}
