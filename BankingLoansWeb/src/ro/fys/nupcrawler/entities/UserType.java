/*
 * UserType.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.entities;

/**
 * 
 * This is an user type enumeration.
 */
public enum UserType {
	NOT_AUTHENTICATED, 
	STANDARD_USER, 
	ADMINISTRATOR
}
