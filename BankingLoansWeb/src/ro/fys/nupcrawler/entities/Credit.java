package ro.fys.nupcrawler.entities;

public class Credit implements Comparable<Credit> {

    private Long id;
    private String categorieCredit = new String();
    private String moneda = new String();
    private String suma = new String();
    private String banca = new String();
    private String numeCredit = new String();
    private String primaRata = new String();
    private String dobanda = new String();
    private String tipDobanda = new String();
    private String dae = new String();
    private String comisioane = new String();
    private String venitMinim = new String();
    private String rataDobanda = new String();
    private String costTotal = new String();
    private String urlRecord = new String();

    public Credit() {
    }

    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    public String getNumeCredit() {
        return numeCredit;
    }

    public void setNumeCredit(String numeCredit) {
        this.numeCredit = numeCredit;
    }

    public String getPrimaRata() {
        return primaRata;
    }

    public void setPrimaRata(String primaRata) {
        this.primaRata = primaRata;
    }

    public String getDobanda() {
		return dobanda;
	}

	public void setDobanda(String dobanda) {
		this.dobanda = dobanda;
	}

	public String getTipDobanda() {
        return tipDobanda;
    }

    public void setTipDobanda(String tipDobanda) {
        this.tipDobanda = tipDobanda;
    }

    public String getDae() {
        return dae;
    }

    public void setDae(String dae) {
        this.dae = dae;
    }

    public String getComisioane() {
        return comisioane;
    }

    public void setComisioane(String comisioane) {
        this.comisioane = comisioane;
    }

    public String getVenitMinim() {
        return venitMinim;
    }

    public void setVenitMinim(String venitMinim) {
        this.venitMinim = venitMinim;
    }

    public String getCategorieCredit() {
        return categorieCredit;
    }

    public void setCategorieCredit(String categorieCredit) {
        this.categorieCredit = categorieCredit;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getSuma() {
        return suma;
    }

    public void setSuma(String suma) {
        this.suma = suma;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRataDobanda() {
        return rataDobanda;
    }

    public void setRataDobanda(String rataDobanda) {
        this.rataDobanda = rataDobanda;
    }

    public String getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(String costTotal) {
        this.costTotal = costTotal;
    }

	public String getUrlRecord() {
		return urlRecord;
	}

	public void setUrlRecord(String urlRecord) {
		this.urlRecord = urlRecord;
	}

	public int compareTo(Credit credit) {
		float dae = new Float(this.getDae().replace(",", "."));
		float daeCompare = new Float(credit.getDae().replace(",", "."));
		
		if (dae < daeCompare) {
			return -1;
		} else if (dae == daeCompare) {
			return 0;
		} else {
			return 1;
		}
	}
    
}
