package ro.fys.nupcrawler.entities.message;

public class NotificationMessage {
	
	private Long id;
	private String notificationMessageTitle = new String();
	private Long userId;
	
	public NotificationMessage(){
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long notificationMessageId) {
		this.id = notificationMessageId;
	}

	public String getNotificationMessageTitle() {
		return notificationMessageTitle;
	}

	public void setNotificationMessageTitle(String notificationMessageTitle) {
		this.notificationMessageTitle = notificationMessageTitle;
	}

        public Long getUserId() {
            return userId;
        }
    
        public void setUserId(Long userId) {
            this.userId = userId;
        }
	
}
