/*
 * UserBean.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *      
 * Version Info:
 *
 */

package ro.fys.nupcrawler.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.services.AccountService;
import ro.fys.nupcrawler.services.constants.ActionResponseType;

public class UserBean {
    
    private Logger log = Logger.getLogger(UserBean.class);
    /**
     * @uml.property  name="username"
     */
    private String usernameTxt = new String();
    /**
     * @uml.property  name="userPassword"
     */
    private String userPasswordTxt = new String();
    
    private String roleTxt = new String();
    private boolean viewPopupUpdate = false;
    
    /**
     * @uml.property  name="email"
     */
    private String emailTxt = new String();

    private List<SelectItem> roleSelectItemList = new ArrayList<SelectItem>();
    private boolean viewPopup = false;
    private String popupMsg = new String();
    private String userForUpdate = new String();
    
    
    public UserBean(){
        
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        userForUpdate = (String) request.getSession().getAttribute("userUpdate");
        log.debug("Update/Save action " + userForUpdate);
        
        roleSelectItemList.clear();
        
        SelectItem item = new SelectItem();
        item.setLabel("Admin");
        item.setValue("Admin");
        roleSelectItemList.add(item);
        
        SelectItem item2 = new SelectItem();
        item2.setLabel("Normal");
        item2.setValue("Normal");
        roleSelectItemList.add(item2);     
    }
    
    public void roleValueChanged(ValueChangeEvent event) {
        if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
            event.setPhaseId(PhaseId.INVOKE_APPLICATION);
            event.queue();
        }
        log.debug("Role Item selected " + roleTxt);
      
        
    }
    
    public void mainListener(ActionEvent event){
        
        /* actions from GUI initiate users management*/
        if (event.getComponent().getId().equals("saveUser")) {
            
            User user = new User();
            user.setName(usernameTxt);
            user.setPassword(userPasswordTxt);
            user.setUserType(roleTxt);
            user.setEmail(emailTxt);
            user.setCreationDate(new Date());
            
            HashMap msg = new AccountService().addUser(user);
            Set mapSet = msg.keySet();
            for (Iterator iterator = mapSet.iterator(); iterator.hasNext();) {
                Object object = (Object) iterator.next();
                log.debug("KEY " + object);
                log.debug("Value " + msg.get(object));
            }
            popupMsg = (String) msg.get(ActionResponseType.ACTION_MESSAGE_KEY);

            if (((Number)msg.get(ActionResponseType.ACTION_RESPONSE_KEY)).intValue() == 1){
                popupMsg += "The user can be viewed in View all user area.";
            }
            
           
            viewPopup = true;
        }
        if (event.getComponent().getId().equals("closePopups")) {
            viewPopup = false;
        }
        
    }

    public String getUsernameTxt() {
        return usernameTxt;
    }

    public void setUsernameTxt(String usernameTxt) {
        this.usernameTxt = usernameTxt;
    }

    public String getUserPasswordTxt() {
        return userPasswordTxt;
    }

    public void setUserPasswordTxt(String userPasswordTxt) {
        this.userPasswordTxt = userPasswordTxt;
    }

    public String getEmailTxt() {
        return emailTxt;
    }

    public void setEmailTxt(String emailTxt) {
        this.emailTxt = emailTxt;
    }

    public String getRoleTxt() {
        return roleTxt;
    }

    public void setRoleTxt(String roleTxt) {
        this.roleTxt = roleTxt;
    }

    public List<SelectItem> getRoleSelectItemList() {
        return roleSelectItemList;
    }

    public void setRoleSelectItemList(List<SelectItem> roleSelectItemList) {
        this.roleSelectItemList = roleSelectItemList;
    }

    public boolean isViewPopup() {
        return viewPopup;
    }

    public void setViewPopup(boolean viewPopup) {
        this.viewPopup = viewPopup;
    }

    public String getPopupMsg() {
        return popupMsg;
    }

    public void setPopupMsg(String popupMsg) {
        this.popupMsg = popupMsg;
    }

    public boolean isViewPopupUpdate() {
        return viewPopupUpdate;
    }

    public void setViewPopupUpdate(boolean viewPopupUpdate) {
        this.viewPopupUpdate = viewPopupUpdate;
    }

    public String getUserForUpdate() {
        return userForUpdate;
    }

    public void setUserForUpdate(String userForUpdate) {
        this.userForUpdate = userForUpdate;
    }
    
    

}
