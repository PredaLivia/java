/*
 * MainPageBean.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.beans;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseStream;
import javax.faces.context.ResponseWriter;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.render.RenderKit;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ro.fys.nupcrawler.dao.impl.NotificationMessageDAO;
import ro.fys.nupcrawler.dao.impl.ReportDAO;
import ro.fys.nupcrawler.dao.impl.SchedulerDAO;
import ro.fys.nupcrawler.entities.beans.Keyword;
import ro.fys.nupcrawler.entities.beans.Page;
import ro.fys.nupcrawler.entities.beans.Report;
import ro.fys.nupcrawler.entities.beans.Scheduler;
import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.entities.gui.LeftMenuItem;
import ro.fys.nupcrawler.entities.message.NotificationMessage;
import ro.fys.nupcrawler.services.AccountService;
import ro.fys.nupcrawler.services.CrawlerService;
import ro.fys.nupcrawler.services.GUILeftMenuService;
import ro.fys.nupcrawler.services.TopicsService;
import ro.fys.nupcrawler.services.test.JobScheduler;
import ro.star.archiver.beans.AuthorBean;
import ro.star.archiver.beans.TagBean;
import ro.star.archiver.beans.TweetBean;
import ro.star.archiver.beans.TweetsTopBean;
import ro.star.archiver.dao.AuthorDAO;
import ro.star.archiver.dao.TagDAO;
import ro.star.archiver.dao.TweetDAO;
import ro.star.archiver.entity.Author;
import ro.star.archiver.entity.RetweetNo;
import ro.star.archiver.entity.Tag;
import ro.star.archiver.entity.Tweet;

import com.icesoft.faces.async.render.OnDemandRenderer;
import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.context.ByteArrayResource;
import com.icesoft.faces.context.Resource;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;

/**
 * Bean for the main jspx.
 */
public class TopTweetsBean extends FacesContext implements Serializable, Renderable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Logger log = Logger.getLogger(TopTweetsBean.class);

    /*
     * label displayed on GUI in as the Title of the left menu, after a main
     * menu option is clicked
     */
    private String menuSelected = new String();

    /*
     * label displayed on GUI in as the Title in the body center, after a main
     * menu option or a left maneu option is clicked
     */
    private String bodySelected = new String();

    /* properties for show/hide the panels body when an menu item is selected */
    private boolean viewTopicsBody = false;
    private boolean viewFavUrlsBody = false;
    private boolean viewMyAccountBody = false;
    private boolean viewAdministrationBody = false;
    private boolean viewTopicAll = false;
    private boolean viewTopicDetails = false;
    private boolean viewUserAll = false;
    private boolean viewReports = false;
    private boolean viewScheduler = false;
    private boolean changeScheduler = false;

    /* menu items */
    private boolean viewLeftMenu = false;
    private boolean viewAddTopic = false;
    private boolean viewAddUser = false;
    private int menu1Rows = 4;
    private ArrayList<Topic> topicList = new ArrayList<Topic>();
    private ArrayList<TweetBean> tweetList = new ArrayList<TweetBean>();
    private ArrayList<LeftMenuItem> leftMenuList = new ArrayList<LeftMenuItem>();
    private ArrayList<NotificationMessage> notificationMessagesList = new ArrayList<NotificationMessage>();
    private ArrayList<User> userList = new ArrayList<User>();
    private ArrayList<Keyword> topicKeywordsList = new ArrayList<Keyword>();
    private ArrayList<Page> topicPagesList = new ArrayList<Page>();
    private String topicDetailNameTxt = new String();
    private Topic topicSelected = new Topic();
    private ArrayList<Scheduler> schedulerDates = new ArrayList<Scheduler>();
    private String userName = new String();
    private String userPassword = new String();
    private String authFailedMsg = new String();
    private User userAuthenticated = new User();
    private String topicSelectedStr = new String();
    private String deepnessLevelTxt = new String();

    private Date date = new Date();
    private Date dateScheduler = new Date();
    private List<SelectItem> topicSelectItemList = new ArrayList<SelectItem>();
    LinkedHashMap<String, String> menuHashes = new LinkedHashMap<String, String>();
    private boolean viewPersonalInfo = false;
    private boolean viewChangeEmail = false;
    private boolean viewChangePassword = false;
    private boolean vizViewAll1 = true;
    private boolean vizViewLess1 = false;

    private String acccountUserName = new String();
    private String accountUserPassword = new String();
    private String accountEmail = new String();
    private Date accountRegDate = new Date();
    private String accountUserType = new String();

    private String changeEmailCurrent = new String();
    private String changeEmailNew = new String();
    private String changePasswordCurrent = new String();
    private String changePasswordNew = new String();
    private String changePasswordConfirm = new String();
    private String welcomeMsg = new String();
    private boolean viewPopupUpdate = false;
    private boolean viewUpdateUser = false;
    private String loadingImg = new String();
    private Runnable doLater;
    private RenderManager renderManager;
    private OnDemandRenderer onDemandRenderer;
    private boolean vizConnectionStatus = true;
    private FacesContext facesContextG;
    private String topicId = new String();
    protected PersistentFacesState state;
    private String topicSel = new String();
    private String searchText = new String();
    private boolean viewChangeAccount = false;
    private String popupMsg = new String();
    private boolean adminOnly = false;
    private String crawlingTopic = new String();
    private boolean closeNotification = false;
    private boolean closeNotificationPanel = false;

    /* Reports atrs */
    private ArrayList<Keyword> topicKeywordsListReport = new ArrayList<Keyword>();
    private ArrayList<Report> topicReportsListReport = new ArrayList<Report>();
    private Resource pdfResourceForOpen;
    private static final String RESOURCE_PATH_FOR_DOWNLOAD1 = "/WEB-INF/resources/importFile.xml";
    private Resource xmlResourceForDownload;
    private static final String RESOURCE_PATH_FOR_DOWNLOAD = "/WEB-INF/resources/test2.csv";
    private String topicSelectedStrReport = new String();
    private List<SelectItem> topicSelectItemListReport = new ArrayList<SelectItem>();
    private boolean viewUpdateTopic = false;
    private String topicNameTxtUpdate = new String();
    private String keywordTxtUpdate = new String();
    private double keywordWeightTxtUpdate;
    private ArrayList<Keyword> keywordsListUpdate = new ArrayList<Keyword>();
    private int deepnessLevelUpdate = 0;
    private int currentKeywordUpdate = 0;
    private String topicSelectedUpdate = new String();

    private List<AuthorBean> authorsFromList = new ArrayList<AuthorBean>();
    private List<TagBean> tagsFromList = new ArrayList<TagBean>();
    private ArrayList<TweetsTopBean> topTweetsList = new ArrayList<TweetsTopBean>();

    @PostConstruct
    private void init() {
        initTopTweets();
    }

    private void initTopTweets() {
        authorsFromList.clear();
        tagsFromList.clear();
        topTweetsList.clear();

        TweetDAO tweetDao = new TweetDAO();
        List<RetweetNo> retweetsList = tweetDao.getTopNumberOfRetweets();

        AuthorDAO authorDao = new AuthorDAO();

        for (int i = 0; i < retweetsList.size(); i++) {
            Tweet tweet = tweetDao.findByID(retweetsList.get(i).getTweetId().longValue());
            Author author = authorDao.findByID(tweet.getAuthor());

            TweetsTopBean bean = new TweetsTopBean();
            bean.setTitle(author.getName() + " " + author.getUsername());
            bean.setDescription(tweet.getDescription());
            bean.setImage("../" + author.getPoza());
            bean.setInternalID(tweet.getInternalID());
            bean.setRetweetsNo(retweetsList.get(i).getNumber().intValue());

            topTweetsList.add(bean);
        }

    }

    public void changeSelection(ValueChangeEvent event) {
        // Maintain a count of selected checkboxes
        System.out.println();
    }

    public void setRenderManager(RenderManager renderManager) {
        if (onDemandRenderer == null) {
            onDemandRenderer = renderManager.getOnDemandRenderer("onDemand");
            onDemandRenderer.add(this);
        }
    }

    public void renderingException(RenderingException arg0) {
        if (onDemandRenderer != null) {
            onDemandRenderer.remove(this);
            onDemandRenderer = null;
        }
    }

    /* Reports area */

    public void topicValueChangedReport(ValueChangeEvent event) {
        if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
            event.setPhaseId(PhaseId.INVOKE_APPLICATION);
            event.queue();
        }

        Long id = 0L;

        System.out.println("S-a selectat " + topicSelectedStrReport);
        if (!topicSelectedStrReport.equals("")) {

            id = Long.parseLong(topicSelectedStrReport);
            Topic topic = new TopicsService().getTopicInfo(id);

            Set<Keyword> setKeywords = new HashSet<Keyword>(topic.getKeywords());
            topicKeywordsListReport.clear();
            for (Iterator<Keyword> iterator = setKeywords.iterator(); iterator.hasNext();) {
                Keyword object = (Keyword) iterator.next();
                topicKeywordsListReport.add(object);

            }

            ReportDAO report = new ReportDAO();
            topicReportsListReport.clear();
            topicReportsListReport = new ReportDAO().getAllByUserAndTopic(userAuthenticated.getId(), topic.getId());

            for (int i = 0; i < topicReportsListReport.size(); i++) {
                String filePath = topicReportsListReport.get(i).getFilePath();
                topicReportsListReport.get(i)
                        .setCsvFile(
                                buildResourceForDownload("/WEB-INF/resources/"
                                        + topicReportsListReport.get(i).getReportName()));
            }

        }
    }

    public Resource buildResourceForDownload(String resourcePath) {
        Resource xmlResourceForDownload = null;
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            xmlResourceForDownload = new ByteArrayResource(toByteArray(context.getResourceAsStream(resourcePath)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlResourceForDownload;
    }

    /**
     * Convert an InputStream into byteArray representation.
     * 
     * @param input
     *            - stream which is converted into byteArray
     * @return object in byteArray representation
     * @throws IOException
     */
    public static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buf = new byte[4096];
        int len = 0;
        while ((len = input.read(buf)) > -1)
            output.write(buf, 0, len);
        return output.toByteArray();
    }

    /* Reports area end here */

    public void topicValueChanged(ValueChangeEvent event) {
        if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
            event.setPhaseId(PhaseId.INVOKE_APPLICATION);
            event.queue();
        }
        log.debug("Topic Item selected " + topicSelectedStr);
        topicSel = topicSelectedStr;
        Long id = 0L;

        log.debug("Reinit the scheduler for this topic ");
        schedulerDates.clear();

        if (!topicSel.equals("")) {
            id = Long.parseLong(topicSelectedStr);
            Topic topic = new TopicsService().getTopicInfo(id);
            log.debug("TOPICC " + topic.getSchedulers().size());
            Set<Scheduler> set = new HashSet<Scheduler>(topic.getSchedulers());

            for (Iterator<Scheduler> iterator = set.iterator(); iterator.hasNext();) {
                Scheduler object = (Scheduler) iterator.next();
                schedulerDates.add(object);
            }

        }

    }

    public void removeFromListUdpate(String keywordName) {
        for (int i = 0; i < keywordsListUpdate.size(); i++) {
            if (keywordsListUpdate.get(i).getName().equals(keywordName)) {
                keywordsListUpdate.remove(i);
            }
        }

    }

    public void mainListener(ActionEvent event) {
        
        if (event.getComponent().getId().equals("tweets")) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("mainPage.iface");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        
        if (event.getComponent().getId().equals("topTweets")) {
            initTopTweets();
        }
        
        if (event.getComponent().getId().equals("images")) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("mainPage.iface");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        
        if (event.getComponent().getId().equals("links")) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("linksPage.iface");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        
        if (event.getComponent().getId().equals("mostActive")) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("topActive.iface");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        
        if (event.getComponent().getId().equals("userTweets")) {
            try {
                ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext();
                String userName = (String) ex.getRequestParameterMap().get("userName");
                FacesContext.getCurrentInstance().getExternalContext().redirect("mainPage.iface?userName="+ userName);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        
        if (event.getComponent().getId().equals("twitterPage")) {
            try {
                ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext();
                String userName = (String) ex.getRequestParameterMap().get("userName");
                FacesContext.getCurrentInstance().getExternalContext().redirect("https://twitter.com/"+ userName);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        
        // event initiated when users click on left menu items
        // topics_reports updateUser updateTopic

        if (event.getComponent().getId().equals("refreshNotifyMessage")) {
            notificationMessagesList.clear();
            ArrayList<NotificationMessage> list = new NotificationMessageDAO().getAll();
            if (list != null) {
                for (NotificationMessage message : list) {
                    if (message.getUserId() == userAuthenticated.getId()) {
                        notificationMessagesList.add(message);
                    }
                }
                log.debug("Notification messages list refreshed.");
            } else {
                log.debug("Notification messages list refreshed. No items found.");
            }

        }
        if (event.getComponent().getId().equals("viewLess1")) {
            log.debug("View less for meniu 1");
            menu1Rows = 4;
            vizViewLess1 = false;
            vizViewAll1 = true;

            authorsFromList.clear();

            AuthorDAO authorDao = new AuthorDAO();

            ArrayList<Author> authList = authorDao.getAllAuthors();
            AuthorBean autBean = new AuthorBean();
            for (int i = 0; i < 4; i++) {
                autBean = new AuthorBean();
                List<Tweet> tweetsList = authorDao.getAuthorTweets(authList.get(i));
                String tweetsNo = new Integer(tweetsList.size()).toString();
                autBean.setCountTweets(tweetsNo);
                autBean.setUsername(authList.get(i).getUsername() + " " + tweetsNo);
                autBean.setUserID(authList.get(i).getId());

                authorsFromList.add(autBean);
            }
        }

        if (event.getComponent().getId().equals("viewAll1")) {
            log.debug("View all for meniu 1");
            vizViewLess1 = true;
            vizViewAll1 = false;

            authorsFromList.clear();

            AuthorDAO authorDao = new AuthorDAO();

            ArrayList<Author> authList = authorDao.getAllAuthors();
            AuthorBean autBean = new AuthorBean();
            for (int i = 0; i < authList.size(); i++) {
                autBean = new AuthorBean();
                List<Tweet> tweetsList = authorDao.getAuthorTweets(authList.get(i));
                String tweetsNo = new Integer(tweetsList.size()).toString();
                autBean.setCountTweets(tweetsNo);
                autBean.setUsername(authList.get(i).getUsername() + " " + tweetsNo);
                autBean.setUserID(authList.get(i).getId());

                authorsFromList.add(autBean);
            }

            menu1Rows = authorsFromList.size();

        }

        if (event.getComponent().getId().equals("closeNotifyMessage")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());

            String itemSelected = (String) hash.get("messageItemSelected");
            new NotificationMessageDAO().delete(itemSelected, userAuthenticated.getId());

            log.debug("Notification topic '" + itemSelected + "' was deleted.");
        }

        if (event.getComponent().getId().equals("addKeywordUpdate")) {

            Keyword keyword = new Keyword();
            keyword.setName(keywordTxtUpdate);
            keyword.setWeight(keywordWeightTxtUpdate);

            keywordsListUpdate.add(keyword);
            currentKeywordUpdate++;

        }
        if (event.getComponent().getId().equals("deleteKeywordUpdate")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String itemSelected = (String) hash.get("keywordSelected");

            log.debug(keywordsListUpdate.size());
            log.debug("Removed keyword " + itemSelected + " from list before save!");
            removeFromListUdpate(itemSelected);
            log.debug(keywordsListUpdate.size());

        }
        // saveUpdateTopic
        if (event.getComponent().getId().equals("saveUpdateTopic")) {
            Topic topic = new TopicsService().getTopicInfo(Long.parseLong(topicSelectedUpdate));

            topic.setName(topicNameTxtUpdate);
            topic.setDeepnessLevel(deepnessLevelUpdate);

            Set keywordSet = new HashSet(keywordsListUpdate);
            topic.setKeywords(keywordSet);

            new TopicsService().updateTopic(topic);

        }
        if (event.getComponent().getId().equals("updateTopic")) {
            viewUpdateTopic = true;
            viewTopicsBody = false;
            viewFavUrlsBody = false;
            viewMyAccountBody = false;
            viewAdministrationBody = false;
            viewAddTopic = false;
            viewTopicDetails = false;
            viewReports = false;
            viewTopicAll = true;
            viewAddTopic = false;
            viewReports = false;
            viewScheduler = false;

            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            topicSelectedUpdate = (String) hash.get("topicId");

            log.debug("Topic selected " + topicSelectedUpdate);

            Topic topic = new TopicsService().getTopicInfo(Long.parseLong(topicSelectedUpdate));

            topicNameTxtUpdate = topic.getName();
            deepnessLevelUpdate = topic.getDeepnessLevel();
            Set keys = new HashSet(topic.getKeywords());

            keywordsListUpdate.clear();
            for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
                Keyword object = (Keyword) iterator.next();
                keywordsListUpdate.add(object);

            }

        }
        if (event.getComponent().getId().equals("updateUser")) {

            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String userId = (String) hash.get("userSelected");

            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            request.getSession().setAttribute("userUpdate", userId);

            viewAddUser = true;
            viewUserAll = false;
            menuSelected = "Update user";

        }

        if (event.getComponent().getId().equals("deleteUser")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String userSelected = (String) hash.get("userSelected");
            log.debug("Delete user with id " + userSelected);

            new AccountService().deleteUser(Long.parseLong(userSelected));
            userList = new AccountService().getAllUsers();
            for (int i = 0; i < userList.size(); i++) {
                ArrayList<Topic> topic = new TopicsService().getAllTopicsForUser(userList.get(i).getId());
                userList.get(i).setTopicCount(topic.size());
            }

        }
        if (event.getComponent().getId().equals("runNow")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());

            topicId = (String) hash.get("topicId");
            loadingImg = "/images/internal/connect_active_new_doc.gif";

            doLater = new Runnable() {
                public void run() {
                    loadingImg = "/images/internal/connect_active_new_doc11.gif";
                    crawlingTopic = "The crawling for "
                            + new TopicsService().getTopicInfo(Long.parseLong(topicId)).getName() + "was finished.";
                    vizConnectionStatus = true;
                    closeNotification = true;
                    onDemandRenderer.requestRender();
                }
            };

            facesContextG = FacesContext.getCurrentInstance();
            closeNotificationPanel = true;
            crawlingTopic = "The topic " + new TopicsService().getTopicInfo(Long.parseLong(topicId)).getName()
                    + " is crawling now!";
            Thread th = new Thread() {

                public void run() {
                    FacesContext.setCurrentInstance(facesContextG);

                    new CrawlerService().runAdHocCrawler(Long.parseLong(topicId), doLater);
                }

            };
            th.start();

        }
        if (event.getComponent().getId().equals("menuItemSelected")) {

            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String itemSelected = (String) hash.get("menuItemId");

            log.debug("Option selected " + itemSelected);
            /*
             * below will be called different services in accord with option
             * selected eg. if itemSelected == "topics_myTopics" then call
             * topicList = new TopicsService().getAllTopicsForUser(3L);
             * ..........................
             */
            // topics_scheduler //account_info

            if (itemSelected.equals("account_info")) {
                viewPersonalInfo = true;
                viewChangeEmail = false;
                viewChangePassword = false;
                viewReports = false;
                acccountUserName = userAuthenticated.getName();
                accountEmail = userAuthenticated.getEmail();
                accountRegDate = userAuthenticated.getCreationDate();
                accountUserType = userAuthenticated.getUserType();

            }

            if (itemSelected.equals("account_email")) {
                viewPersonalInfo = false;
                viewChangeEmail = true;
                viewChangePassword = false;

                changeEmailCurrent = userAuthenticated.getEmail();

                // Update user email

            }

            if (itemSelected.equals("account_password")) {
                viewPersonalInfo = false;
                viewChangeEmail = false;
                viewChangePassword = true;

                changePasswordCurrent = userAuthenticated.getPassword();
                log.debug("PASSWORD " + changePasswordCurrent);

            }

            if (itemSelected.equals("topics_myTopics")) {

                viewTopicsBody = true;
                viewFavUrlsBody = false;
                viewMyAccountBody = false;
                viewAdministrationBody = false;
                viewAddTopic = false;
                viewTopicDetails = false;
                viewUpdateTopic = false;
                viewTopicAll = true;
                viewAddTopic = false;
                viewReports = false;
                viewScheduler = false;
                viewReports = false;
                menuSelected = (String) menuHashes.get("topics_myTopics");
                log.debug("MENU SELECTED " + menuSelected);

                // only for tests
                viewTopicAll = true;
                viewLeftMenu = true;

                log.debug("User authenticated " + userAuthenticated.getName());

                topicList = new TopicsService().getAllTopicsForUser(userAuthenticated.getId());

                for (int i = 0; i < topicList.size(); i++) {
                    HashSet<Keyword> keys = new HashSet<Keyword>(topicList.get(i).getKeywords());

                    String keyStr = new String();
                    for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
                        Keyword keyword = (Keyword) iterator.next();
                        keyStr += keyword.getName() + ", ";
                    }

                    Topic topic = topicList.get(i);
                    topic.setKeywordsList(keyStr);

                    topicList.set(i, topic);
                }

            }

            if (itemSelected.equals("topics_scheduler")) {
                viewScheduler = true;
                viewTopicAll = false;
                viewAddTopic = false;
                viewReports = false;
                viewUpdateTopic = false;
                menuSelected = (String) menuHashes.get("topics_scheduler");

                /* for tests */
                // Topic topic = new Topic();
                // topic.setName("Software development");
                // topic.setSchedulerDate(new Date().getTime());
                //
                // topicSelected = topic;
                //
                // SelectItem item = new SelectItem();
                // item.setLabel("topic_id");
                // item.setValue("topic_value");
                //
                // topicSelectItemList.add(item);

                /* tests ends here */
                topicSelectItemList.clear();
                ArrayList<Topic> topicListItem = new TopicsService().getAllTopicsForUser(userAuthenticated.getId());
                for (int i = 0; i < topicListItem.size(); i++) {
                    SelectItem item = new SelectItem();
                    item.setLabel(topicListItem.get(i).getName());
                    item.setValue(topicListItem.get(i).getId());
                    topicSelectItemList.add(item);
                }

            }
            if (itemSelected.equals("topics_addNewTopic")) {
                viewAddTopic = true;
                viewTopicAll = false;
                viewReports = false;
                viewUpdateTopic = false;
                viewTopicDetails = false;
                viewScheduler = false;
                menuSelected = (String) menuHashes.get("topics_addNewTopic");

            }

            if (itemSelected.equals("topics_reports")) {
                viewReports = true;
                viewAddTopic = false;
                viewTopicAll = false;
                viewUpdateTopic = false;
                viewTopicDetails = false;
                viewScheduler = false;
                menuSelected = (String) menuHashes.get("topics_reports");

                topicSelectItemListReport.clear();
                ArrayList<Topic> topicListItem = new TopicsService().getAllTopicsForUser(userAuthenticated.getId());
                for (int i = 0; i < topicListItem.size(); i++) {
                    SelectItem item = new SelectItem();
                    item.setLabel(topicListItem.get(i).getName());
                    item.setValue(topicListItem.get(i).getId());
                    topicSelectItemListReport.add(item);
                }
            }

            if (itemSelected.equals("administration_viewAllUsers")) {
                viewUserAll = true;
                viewAddUser = false;

                userList = new AccountService().getAllUsers();
                for (int i = 0; i < userList.size(); i++) {
                    ArrayList<Topic> topic = new TopicsService().getAllTopicsForUser(userList.get(i).getId());
                    userList.get(i).setTopicCount(topic.size());
                }

                menuSelected = (String) menuHashes.get("administration_viewAllUsers");

            }
            if (itemSelected.equals("administration_addNewUser")) {
                viewAddUser = true;
                viewUserAll = false;
                menuSelected = (String) menuHashes.get("administration_addNewUser");

            }
        }

        // vizAuthorTweets
        if (event.getComponent().getId().equals("vizAuthorTweets")) {
            tweetList.clear();
            tagsFromList.clear();
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String authorID = (String) hash.get("idAuthor");
            AuthorDAO authorDao = new AuthorDAO();
            ArrayList<Author> authList = authorDao.getAllAuthors();

            if (authorID != null) {
                AuthorBean autBean = new AuthorBean();
                Author author = authorDao.findByID(new Long(authorID));

                System.out.println("Display the author tweets for user " + author.getName() + " (ID " + authorID + ")");
                TweetDAO tweetDao = new TweetDAO();
                ArrayList<Tweet> tweetsList = tweetDao.getTweetsByAuthorID(new Long(authorID));
                for (int i = 0; i < tweetsList.size(); i++) {
                    TweetBean tBean = new TweetBean();
                    tBean.setTitle(author.getName() + " " + author.getUsername());
                    tBean.setDescription(tweetsList.get(i).getDescription());
                    tBean.setImage(author.getPoza());
                    tBean.setInternalID(tweetsList.get(i).getInternalID());
                    tweetList.add(tBean);

                }

                TagDAO tagDao = new TagDAO();
                ArrayList<Tag> tagsList = tagDao.getTagsIDByAuthorID(new Long(authorID));
                for (int j = 0; j < tagsList.size(); j++) {
                    TagBean tagBean = new TagBean();
                    tagBean.setId(tagsList.get(j).getId());
                    tagBean.setName(tagsList.get(j).getName());
                    if (!tagsFromList.contains(tagBean)) {
                        tagsFromList.add(tagBean);
                    }
                }

            } else {
                System.out.println("Selected author null...");
            }
        }
        if (event.getComponent().getId().equals("search")) {

            if ("".equals(searchText)) {
            } else {

                log.debug("Searching for " + searchText);

                authorsFromList.clear();
                tagsFromList.clear();
                tweetList.clear();

                menuSelected = "#" + searchText;

                viewTopicsBody = true;
                viewFavUrlsBody = false;
                viewMyAccountBody = false;
                viewUpdateTopic = false;
                viewAdministrationBody = false;
                viewAddTopic = false;
                viewTopicDetails = false;

                viewTopicAll = true;
                viewAddTopic = false;
                viewReports = false;
                viewScheduler = false;

                viewTopicAll = true;
                viewLeftMenu = true;

                log.debug("User authenticated " + userAuthenticated.getName());

                AuthorDAO authorDao = new AuthorDAO();
                TweetDAO tweetDao = new TweetDAO();

                Set<Author> authorsSet = new HashSet<Author>();
                List<Tweet> tweetsList = tweetDao.searchTweets(searchText);
                for (int i = 0; i < tweetsList.size(); i++) {
                    Author author = authorDao.findByID(tweetsList.get(i).getAuthor());
                    authorsSet.add(author);

                    TweetBean tBean = new TweetBean();
                    tBean.setTitle(author.getName() + " " + author.getUsername());
                    tBean.setDescription(tweetsList.get(i).getDescription());
                    tBean.setImage(author.getPoza());
                    tBean.setInternalID(tweetsList.get(i).getInternalID());
                    tweetList.add(tBean);
                }

                AuthorBean autBean = new AuthorBean();
                // autBean.setUsername("All");
                // autBean.setUserID(Long.parseLong("999"));
                // autBean.setCountTweets("18");
                // authorsFromList.add(autBean);
                for (Author author : authorsSet) {
                    autBean = new AuthorBean();
                    List<Tweet> authorTweetsList = authorDao.getAuthorTweets(author);
                    String tweetsNo = new Integer(authorTweetsList.size()).toString();
                    autBean.setCountTweets(tweetsNo);
                    autBean.setUsername(author.getUsername() + " " + tweetsNo);
                    autBean.setUserID(author.getId());

                    authorsFromList.add(autBean);
                }

                vizViewLess1 = false;
                vizViewAll1 = true;
            }

            // for (int i = 0; i < authList.size(); i++) {
            // AuthorBean autBean = new AuthorBean();
            // autBean.setUsername(authList.get(i).getUsername());
            // autBean.setUserID(authList.get(i).getId());
            // autBean.setCountTweets("18");
            //
            // authorsFromList.add(autBean);
            // }

            // topicList = new
            // TopicsService().getAllTopicsForUserAndQuery(userAuthenticated.getId(),
            // searchText);
            //
            // for (int i = 0; i < topicList.size(); i++) {
            // HashSet<Keyword> keys = new
            // HashSet<Keyword>(topicList.get(i).getKeywords());
            //
            // String keyStr = new String();
            // for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
            // Keyword keyword = (Keyword) iterator.next();
            // keyStr += keyword.getName() + ", ";
            // }
            //
            // Topic topic = topicList.get(i);
            // topic.setKeywordsList(keyStr);
            //
            // topicList.set(i, topic);
            // }

        }
        // event initiated when users click on Topics option from the main menu
        // addSchedule
        if (event.getComponent().getId().equals("deleteTopic")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String topicSelected = (String) hash.get("topicId");
            log.debug("Delete for " + topicSelected);

            new TopicsService().deleteTopic(Long.parseLong(topicSelected));
            topicList = new TopicsService().getAllTopicsForUser(userAuthenticated.getId());

            for (int i = 0; i < topicList.size(); i++) {
                HashSet<Keyword> keys = new HashSet<Keyword>(topicList.get(i).getKeywords());

                String keyStr = new String();
                for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
                    Keyword keyword = (Keyword) iterator.next();
                    keyStr += keyword.getName() + ", ";
                }

                Topic topic = topicList.get(i);
                topic.setKeywordsList(keyStr);

                topicList.set(i, topic);
            }

        }
        // changePassword
        if (event.getComponent().getId().equals("changeEmail")) {

            userAuthenticated.setEmail(changeEmailNew);
            new AccountService().changeUserPassword(userAuthenticated);
            popupMsg = "Email succesfully changed!";
            viewChangeAccount = true;

        }
        if (event.getComponent().getId().equals("changePassword")) {

            if (changePasswordConfirm.equals(changePasswordNew)) {
                userAuthenticated.setPassword(changePasswordNew);
                new AccountService().changeUserPassword(userAuthenticated);
                popupMsg = "Password succesfully changed!";
                viewChangeAccount = true;
            } else {
                viewChangeAccount = true;
                popupMsg = "The confirm password and new password doesn't match! Please try again.";
            }

        }
        if (event.getComponent().getId().equals("closePopupAccount")) {
            viewChangeAccount = false;
        }
        if (event.getComponent().getId().equals("addSchedule")) {

        }
        if (event.getComponent().getId().equals("tweets")) {
            log.debug("Topics option selected!");
            menuSelected = "Tweets";
            bodySelected = "";


            // viewTopicsBody = true;
            // viewUpdateTopic = false;
            // viewFavUrlsBody = false;
            // viewMyAccountBody = false;
            // viewAdministrationBody = false;
            // viewAddTopic = false;
            // viewTopicDetails = false;
            // viewReports = false;
            // viewScheduler = false;
            // viewTopicAll = true;
            // viewLeftMenu = true;
            //
            // log.debug("User authenticated " + userAuthenticated.getName());
            //
            // topicList = new
            // TopicsService().getAllTopicsForUser(userAuthenticated.getId());
            //
            // for (int i = 0; i < topicList.size(); i++) {
            // HashSet<Keyword> keys = new
            // HashSet<Keyword>(topicList.get(i).getKeywords());
            //
            // String keyStr = new String();
            // for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
            // Keyword keyword = (Keyword) iterator.next();
            // keyStr += keyword.getName() + ", ";
            // }
            //
            // Topic topic = topicList.get(i);
            // topic.setKeywordsList(keyStr);
            //
            // topicList.set(i, topic);
            // }
            //
            // menuHashes.clear();
            // menuHashes.put("topics_myTopics", "My Topics");
            // menuHashes.put("topics_addNewTopic", "Add new topic");
            // menuHashes.put("topics_reports", "Reports");
            // menuHashes.put("topics_scheduler", "Scheduler");
            //
            // leftMenuList = new
            // GUILeftMenuService().createLeftMenu(menuHashes);

        }

        // event initiated when users clicks on Favorite URLs option from the
        // main menu
        if (event.getComponent().getId().equals("favURLs")) {
            log.debug("Favorite URLs option selected!");
            menuSelected = "Favorite URLs";
            bodySelected = "";

            viewFavUrlsBody = true;
            viewTopicsBody = false;
            viewUpdateTopic = false;
            viewMyAccountBody = false;
            viewAdministrationBody = false;
            viewAddTopic = false;
            viewTopicDetails = false;

            /* hide yopics options */
            viewAddTopic = false;
            viewTopicAll = false;
            viewReports = false;
            viewScheduler = false;

        }
        if (event.getComponent().getId().equals("closeNotify")) {
            closeNotificationPanel = false;
            closeNotification = false;
        }
        if (event.getComponent().getId().equals("viewTopic")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String topicSelected = (String) hash.get("topicId");

            log.debug("S-a selectat " + topicSelected);

            viewTopicDetails = true;
            viewFavUrlsBody = false;
            viewTopicsBody = false;
            viewUpdateTopic = false;
            viewMyAccountBody = false;
            viewAdministrationBody = false;
            viewAddTopic = false;
            viewScheduler = false;

            /*
             * only for tests get the topics details
             */

            Topic topic = new TopicsService().getTopicInfo(Long.parseLong(topicSelected));
            topicKeywordsList.clear();
            topicDetailNameTxt = topic.getName();

            HashSet<Keyword> keywordSet = new HashSet<Keyword>(topic.getKeywords());

            for (Iterator iterator = keywordSet.iterator(); iterator.hasNext();) {
                Keyword keyword = (Keyword) iterator.next();
                topicKeywordsList.add(keyword);
            }

            topicPagesList.clear();

            HashSet<Page> pageSet = new HashSet<Page>(topic.getPages());
            log.debug("Pagesssss " + pageSet.size());

            for (Iterator iterator = pageSet.iterator(); iterator.hasNext();) {
                Page page = (Page) iterator.next();
                topicPagesList.add(page);
            }

        }
        // viewTopic
        // event initiated when users clicks on My Account option from the main
        // menu
        if (event.getComponent().getId().equals("myAccount") || event.getComponent().getId().equals("myAccountUpper")) {
            log.debug("My Account option selected!");
            menuSelected = "My Account";
            bodySelected = "";

            viewMyAccountBody = true;
            viewFavUrlsBody = false;
            viewUpdateTopic = false;
            viewTopicsBody = false;
            viewAdministrationBody = false;
            viewAddTopic = false;
            viewTopicDetails = false;

            /* hide yopics options */
            viewAddTopic = false;
            viewTopicAll = false;
            viewReports = false;
            viewScheduler = false;

            menuHashes.clear();
            menuHashes.put("account_info", "Personal Info");
            menuHashes.put("account_email", "Change email");
            menuHashes.put("account_password", "Change password");

            leftMenuList = new GUILeftMenuService().createLeftMenu(menuHashes);

        }

        // deleteSchedulesaveToXML
        if (event.getComponent().getId().equals("saveToXML")) {

        }
        if (event.getComponent().getId().equals("deleteSchedule")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String scheduleSelected = (String) hash.get("scheduleItemId");

            for (int i = 0; i < schedulerDates.size(); i++) {
                if (schedulerDates.get(i).getId().compareTo(Long.parseLong(scheduleSelected)) == 0) {
                    schedulerDates.remove(i);
                }
            }

        }
        if (event.getComponent().getId().equals("saveSchedulerChangeDate")) {
            log.debug("Save a new date to scheduler for topic " + topicSelected.getName());

            Scheduler schedule = new Scheduler();
            schedule.setDate(new Date(dateScheduler.getTime()));
            schedule.setActive(1);
            schedulerDates.add(schedule);

        }
        // event initiated when users clicks on Administration option from the
        // main menu changeScheduler
        if (event.getComponent().getId().equals("changeScheduler")) {
            changeScheduler = true;

        }
        // updateScheduler deleteUser

        if (event.getComponent().getId().equals("closeSchedulerChangeDate")) {
            changeScheduler = false;
        }
        if (event.getComponent().getId().equals("updateScheduler")) {
            log.debug("The topic " + topicSelected.getName() + " updates the scheduler date");
            topicSelected = new Topic();
            topicSelected = new TopicsService().getTopicInfo(Long.parseLong(topicSel));

            log.debug("dATES " + schedulerDates.size());
            log.debug("TOPPPPPIC " + topicSelected.getName());

            Set setDates = new HashSet(schedulerDates);

            topicSelected.setSchedulers(setDates);
            new TopicsService().updateTopic(topicSelected);
            // new TopicsService().updateTopic(Long.parseLong(topicSel));
            schedulerDates.clear();

            changeScheduler = false;
        }
        if (event.getComponent().getId().equals("administration")) {
            log.debug("Administration option selected!");
            menuSelected = "Administration";
            bodySelected = "";

            viewAdministrationBody = true;
            viewMyAccountBody = false;
            viewFavUrlsBody = false;
            viewUpdateTopic = false;
            viewTopicsBody = false;
            viewAddTopic = false;
            viewTopicDetails = false;
            viewScheduler = false;

            viewUserAll = true;

            menuHashes.clear();
            menuHashes = new LinkedHashMap<String, String>();

            menuHashes.put("administration_viewAllUsers", "View all users");
            menuHashes.put("administration_addNewUser", "Add new user");

            leftMenuList = new GUILeftMenuService().createLeftMenu(menuHashes);

            /* hide yopics options */
            viewAddTopic = false;
            viewTopicAll = false;
            viewReports = false;
            viewScheduler = false;

        }

    }

    public void userTweets() {

       

    }

    public String getAuthenticate() {
        String login = new String();

        User user = new User();
        user.setName(userName);
        user.setPassword(userPassword);

        userAuthenticated = new AccountService().authenticateUser(user);

        log.debug("ID USER " + userAuthenticated.getId());
        if (userAuthenticated.getId() != null) {
            login = "okAuth";
            authFailedMsg = "";
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
            request.getSession().setAttribute("user", userAuthenticated);
            welcomeMsg = "Welcome " + userAuthenticated.getName() + " !";
            if (userAuthenticated.getUserType().equals("Admin")) {
                adminOnly = true;
            }

            // Create fake scheduler if it does not exist
            ArrayList<Scheduler> fakeSchedulers = new SchedulerDAO().findById(1L);
            if (fakeSchedulers == null || fakeSchedulers.size() == 0) {
                Scheduler fakeScheduler = new Scheduler();
                fakeScheduler.setId(1L);
                fakeScheduler.setActive(0);
                fakeScheduler.setDate(Calendar.getInstance().getTime());
                new SchedulerDAO().save(fakeScheduler);
            }

            // facesContextG = FacesContext.getCurrentInstance();
            // Thread th = new Thread() {
            //
            // public void run() {
            // FacesContext.setCurrentInstance(facesContextG);
            //
            // while (true){
            // log.debug("Checking the scheduler...............................");
            // List<Topic> topicsToRun = new
            // TopicsService().getTopicsToRun(userAuthenticated.getId());
            // for (int i=0; i<topicsToRun.size(); i++){
            // log.debug("Topic found for run!!!!!!!!");
            // new CrawlerService().runAdHocCrawler(topicsToRun.get(i).getId(),
            // doLater);
            // }
            //
            // }
            //
            //
            // //new CrawlerService().runAdHocCrawler(Long.parseLong(topicId),
            // doLater);
            // }
            //
            // };
            // th.start();
            // try {
            // th.sleep(5000);
            // } catch (InterruptedException e) {
            // e.printStackTrace();
            // }

        } else {
            login = "notOkAuth";
            log.debug("Invalid user or password!");
            authFailedMsg = "Invalid user or password!";
        }

        Thread th = new Thread() {

            public void run() {
                JobScheduler.run();
            }

        };
        th.start();

        System.out.println(login);
        return login;
    }

    /**
     * Getters and setters section
     * 
     */

    public String getMenuSelected() {
        return menuSelected;
    }

    public void setMenuSelected(String menuSelected) {
        this.menuSelected = menuSelected;
    }

    public String getBodySelected() {
        return bodySelected;
    }

    public void setBodySelected(String bodySelected) {
        this.bodySelected = bodySelected;
    }

    public boolean isViewTopicsBody() {
        return viewTopicsBody;
    }

    public void setViewTopicsBody(boolean viewTopicsBody) {
        this.viewTopicsBody = viewTopicsBody;
    }

    public boolean isViewFavUrlsBody() {
        return viewFavUrlsBody;
    }

    public void setViewFavUrlsBody(boolean viewFavUrlsBody) {
        this.viewFavUrlsBody = viewFavUrlsBody;
    }

    public boolean isViewMyAccountBody() {
        return viewMyAccountBody;
    }

    public void setViewMyAccountBody(boolean viewMyAccountBody) {
        this.viewMyAccountBody = viewMyAccountBody;
    }

    public boolean isViewAdministrationBody() {
        return viewAdministrationBody;
    }

    public void setViewAdministrationBody(boolean viewAdministrationBody) {
        this.viewAdministrationBody = viewAdministrationBody;
    }

    public boolean isViewTopicAll() {
        return viewTopicAll;
    }

    public void setViewTopicAll(boolean viewTopicAll) {
        this.viewTopicAll = viewTopicAll;
    }

    public ArrayList<Topic> getTopicList() {
        return topicList;
    }

    public void setTopicList(ArrayList<Topic> topicList) {
        this.topicList = topicList;
    }

    public ArrayList<LeftMenuItem> getLeftMenuList() {
        return leftMenuList;
    }

    public void setLeftMenuList(ArrayList<LeftMenuItem> leftMenuList) {
        this.leftMenuList = leftMenuList;
    }

    public ArrayList<NotificationMessage> getNotificationMessagesList() {
        return notificationMessagesList;
    }

    public void setNotificationMessagesList(ArrayList<NotificationMessage> notificationMessagesList) {
        this.notificationMessagesList = notificationMessagesList;
    }

    public boolean isViewLeftMenu() {
        return viewLeftMenu;
    }

    public void setViewLeftMenu(boolean viewLeftMenu) {
        this.viewLeftMenu = viewLeftMenu;
    }

    public boolean isViewAddTopic() {
        return viewAddTopic;
    }

    public void setViewAddTopic(boolean viewAddTopic) {
        this.viewAddTopic = viewAddTopic;
    }

    public boolean isViewUserAll() {
        return viewUserAll;
    }

    public void setViewUserAll(boolean viewUserAll) {
        this.viewUserAll = viewUserAll;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

    public boolean isViewAddUser() {
        return viewAddUser;
    }

    public void setViewAddUser(boolean viewAddUser) {
        this.viewAddUser = viewAddUser;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isViewTopicDetails() {
        return viewTopicDetails;
    }

    public void setViewTopicDetails(boolean viewTopicDetails) {
        this.viewTopicDetails = viewTopicDetails;
    }

    public ArrayList<Keyword> getTopicKeywordsList() {
        return topicKeywordsList;
    }

    public void setTopicKeywordsList(ArrayList<Keyword> topicKeywordsList) {
        this.topicKeywordsList = topicKeywordsList;
    }

    public String getTopicDetailNameTxt() {
        return topicDetailNameTxt;
    }

    public void setTopicDetailNameTxt(String topicDetailNameTxt) {
        this.topicDetailNameTxt = topicDetailNameTxt;
    }

    public ArrayList<Page> getTopicPagesList() {
        return topicPagesList;
    }

    public void setTopicPagesList(ArrayList<Page> topicPagesList) {
        this.topicPagesList = topicPagesList;
    }

    public boolean isViewReports() {
        return viewReports;
    }

    public void setViewReports(boolean viewReports) {
        this.viewReports = viewReports;
    }

    public boolean isViewScheduler() {
        return viewScheduler;
    }

    public void setViewScheduler(boolean viewScheduler) {
        this.viewScheduler = viewScheduler;
    }

    public Topic getTopicSelected() {
        return topicSelected;
    }

    public void setTopicSelected(Topic topicSelected) {
        this.topicSelected = topicSelected;
    }

    public boolean isChangeScheduler() {
        return changeScheduler;
    }

    public void setChangeScheduler(boolean changeScheduler) {
        this.changeScheduler = changeScheduler;
    }

    public Date getDateScheduler() {
        return dateScheduler;
    }

    public void setDateScheduler(Date dateScheduler) {
        this.dateScheduler = dateScheduler;
    }

    public ArrayList<Scheduler> getSchedulerDates() {
        return schedulerDates;
    }

    public void setSchedulerDates(ArrayList<Scheduler> schedulerDates) {
        this.schedulerDates = schedulerDates;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getAuthFailedMsg() {
        return authFailedMsg;
    }

    public void setAuthFailedMsg(String authFailedMsg) {
        this.authFailedMsg = authFailedMsg;
    }

    public User getUserAuthenticated() {
        return userAuthenticated;
    }

    public void setUserAuthenticated(User userAuthenticated) {
        this.userAuthenticated = userAuthenticated;
    }

    public String getTopicSelectedStr() {
        return topicSelectedStr;
    }

    public void setTopicSelectedStr(String topicSelectedStr) {
        this.topicSelectedStr = topicSelectedStr;
    }

    public List<SelectItem> getTopicSelectItemList() {
        return topicSelectItemList;
    }

    public void setTopicSelectItemList(List<SelectItem> topicSelectItemList) {
        this.topicSelectItemList = topicSelectItemList;
    }

    public String getDeepnessLevelTxt() {
        return deepnessLevelTxt;
    }

    public void setDeepnessLevelTxt(String deepnessLevelTxt) {
        this.deepnessLevelTxt = deepnessLevelTxt;
    }

    public boolean isViewPersonalInfo() {
        return viewPersonalInfo;
    }

    public void setViewPersonalInfo(boolean viewPersonalInfo) {
        this.viewPersonalInfo = viewPersonalInfo;
    }

    public boolean isViewChangeEmail() {
        return viewChangeEmail;
    }

    public void setViewChangeEmail(boolean viewChangeEmail) {
        this.viewChangeEmail = viewChangeEmail;
    }

    public boolean isViewChangePassword() {
        return viewChangePassword;
    }

    public void setViewChangePassword(boolean viewChangePassword) {
        this.viewChangePassword = viewChangePassword;
    }

    public String getAcccountUserName() {
        return acccountUserName;
    }

    public void setAcccountUserName(String acccountUserName) {
        this.acccountUserName = acccountUserName;
    }

    public String getAccountUserPassword() {
        return accountUserPassword;
    }

    public void setAccountUserPassword(String accountUserPassword) {
        this.accountUserPassword = accountUserPassword;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public Date getAccountRegDate() {
        return accountRegDate;
    }

    public void setAccountRegDate(Date accountRegDate) {
        this.accountRegDate = accountRegDate;
    }

    public String getAccountUserType() {
        return accountUserType;
    }

    public void setAccountUserType(String accountUserType) {
        this.accountUserType = accountUserType;
    }

    public String getChangeEmailCurrent() {
        return changeEmailCurrent;
    }

    public void setChangeEmailCurrent(String changeEmailCurrent) {
        this.changeEmailCurrent = changeEmailCurrent;
    }

    public String getChangeEmailNew() {
        return changeEmailNew;
    }

    public void setChangeEmailNew(String changeEmailNew) {
        this.changeEmailNew = changeEmailNew;
    }

    public String getChangePasswordCurrent() {
        return changePasswordCurrent;
    }

    public void setChangePasswordCurrent(String changePasswordCurrent) {
        this.changePasswordCurrent = changePasswordCurrent;
    }

    public String getChangePasswordNew() {
        return changePasswordNew;
    }

    public void setChangePasswordNew(String changePasswordNew) {
        this.changePasswordNew = changePasswordNew;
    }

    public String getChangePasswordConfirm() {
        return changePasswordConfirm;
    }

    public void setChangePasswordConfirm(String changePasswordConfirm) {
        this.changePasswordConfirm = changePasswordConfirm;
    }

    public String getWelcomeMsg() {
        return welcomeMsg;
    }

    public void setWelcomeMsg(String welcomeMsg) {
        this.welcomeMsg = welcomeMsg;
    }

    public boolean isViewUpdateUser() {
        return viewUpdateUser;
    }

    public void setViewUpdateUser(boolean viewUpdateUser) {
        this.viewUpdateUser = viewUpdateUser;
    }

    public boolean isViewPopupUpdate() {
        return viewPopupUpdate;
    }

    public void setViewPopupUpdate(boolean viewPopupUpdate) {
        this.viewPopupUpdate = viewPopupUpdate;
    }

    @Override
    public void addMessage(String arg0, FacesMessage arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public Application getApplication() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<String> getClientIdsWithMessages() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ExternalContext getExternalContext() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Severity getMaximumSeverity() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<FacesMessage> getMessages() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<FacesMessage> getMessages(String arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RenderKit getRenderKit() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean getRenderResponse() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean getResponseComplete() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ResponseStream getResponseStream() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResponseWriter getResponseWriter() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UIViewRoot getViewRoot() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public void renderResponse() {
        // TODO Auto-generated method stub

    }

    @Override
    public void responseComplete() {
        // TODO Auto-generated method stub

    }

    @Override
    public void setResponseStream(ResponseStream arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setResponseWriter(ResponseWriter arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setViewRoot(UIViewRoot arg0) {
        // TODO Auto-generated method stub

    }

    public String getLoadingImg() {
        return loadingImg;
    }

    public void setLoadingImg(String loadingImg) {
        this.loadingImg = loadingImg;
    }

    public Runnable getDoLater() {
        return doLater;
    }

    public void setDoLater(Runnable doLater) {
        this.doLater = doLater;
    }

    public boolean isVizConnectionStatus() {
        return vizConnectionStatus;
    }

    public void setVizConnectionStatus(boolean vizConnectionStatus) {
        this.vizConnectionStatus = vizConnectionStatus;
    }

    public FacesContext getFacesContextG() {
        return facesContextG;
    }

    public void setFacesContextG(FacesContext facesContextG) {
        this.facesContextG = facesContextG;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public boolean isViewChangeAccount() {
        return viewChangeAccount;
    }

    public void setViewChangeAccount(boolean viewChangeAccount) {
        this.viewChangeAccount = viewChangeAccount;
    }

    public String getPopupMsg() {
        return popupMsg;
    }

    public void setPopupMsg(String popupMsg) {
        this.popupMsg = popupMsg;
    }

    public boolean isAdminOnly() {
        return adminOnly;
    }

    public void setAdminOnly(boolean adminOnly) {
        this.adminOnly = adminOnly;
    }

    public String getCrawlingTopic() {
        return crawlingTopic;
    }

    public void setCrawlingTopic(String crawlingTopic) {
        this.crawlingTopic = crawlingTopic;
    }

    public boolean isCloseNotification() {
        return closeNotification;
    }

    public void setCloseNotification(boolean closeNotification) {
        this.closeNotification = closeNotification;
    }

    public boolean isCloseNotificationPanel() {
        return closeNotificationPanel;
    }

    public void setCloseNotificationPanel(boolean closeNotificationPanel) {
        this.closeNotificationPanel = closeNotificationPanel;
    }

    public ArrayList<Keyword> getTopicKeywordsListReport() {
        return topicKeywordsListReport;
    }

    public void setTopicKeywordsListReport(ArrayList<Keyword> topicKeywordsListReport) {
        this.topicKeywordsListReport = topicKeywordsListReport;
    }

    public ArrayList<Report> getTopicReportsListReport() {
        return topicReportsListReport;
    }

    public void setTopicReportsListReport(ArrayList<Report> topicReportsListReport) {
        this.topicReportsListReport = topicReportsListReport;
    }

    public Resource getPdfResourceForOpen() {
        return pdfResourceForOpen;
    }

    public void setPdfResourceForOpen(Resource pdfResourceForOpen) {
        this.pdfResourceForOpen = pdfResourceForOpen;
    }

    public Resource getXmlResourceForDownload() {
        return xmlResourceForDownload;
    }

    public void setXmlResourceForDownload(Resource xmlResourceForDownload) {
        this.xmlResourceForDownload = xmlResourceForDownload;
    }

    public String getTopicSelectedStrReport() {
        return topicSelectedStrReport;
    }

    public void setTopicSelectedStrReport(String topicSelectedStrReport) {
        this.topicSelectedStrReport = topicSelectedStrReport;
    }

    public List<SelectItem> getTopicSelectItemListReport() {
        return topicSelectItemListReport;
    }

    public void setTopicSelectItemListReport(List<SelectItem> topicSelectItemListReport) {
        this.topicSelectItemListReport = topicSelectItemListReport;
    }

    public static String getResourcePathForDownload1() {
        return RESOURCE_PATH_FOR_DOWNLOAD1;
    }

    public static String getResourcePathForDownload() {
        return RESOURCE_PATH_FOR_DOWNLOAD;
    }

    public boolean isViewUpdateTopic() {
        return viewUpdateTopic;
    }

    public void setViewUpdateTopic(boolean viewUpdateTopic) {
        this.viewUpdateTopic = viewUpdateTopic;
    }

    public String getTopicNameTxtUpdate() {
        return topicNameTxtUpdate;
    }

    public void setTopicNameTxtUpdate(String topicNameTxtUpdate) {
        this.topicNameTxtUpdate = topicNameTxtUpdate;
    }

    public String getKeywordTxtUpdate() {
        return keywordTxtUpdate;
    }

    public void setKeywordTxtUpdate(String keywordTxtUpdate) {
        this.keywordTxtUpdate = keywordTxtUpdate;
    }

    public double getKeywordWeightTxtUpdate() {
        return keywordWeightTxtUpdate;
    }

    public void setKeywordWeightTxtUpdate(double keywordWeightTxtUpdate) {
        this.keywordWeightTxtUpdate = keywordWeightTxtUpdate;
    }

    public ArrayList<Keyword> getKeywordsListUpdate() {
        return keywordsListUpdate;
    }

    public void setKeywordsListUpdate(ArrayList<Keyword> keywordsListUpdate) {
        this.keywordsListUpdate = keywordsListUpdate;
    }

    public int getDeepnessLevelUpdate() {
        return deepnessLevelUpdate;
    }

    public void setDeepnessLevelUpdate(int deepnessLevelUpdate) {
        this.deepnessLevelUpdate = deepnessLevelUpdate;
    }

    public int getCurrentKeywordUpdate() {
        return currentKeywordUpdate;
    }

    public void setCurrentKeywordUpdate(int currentKeywordUpdate) {
        this.currentKeywordUpdate = currentKeywordUpdate;
    }

    public int getMenu1Rows() {
        return menu1Rows;
    }

    public void setMenu1Rows(int menu1Rows) {
        this.menu1Rows = menu1Rows;
    }

    public boolean isVizViewAll1() {
        return vizViewAll1;
    }

    public void setVizViewAll1(boolean vizViewAll1) {
        this.vizViewAll1 = vizViewAll1;
    }

    public boolean isVizViewLess1() {
        return vizViewLess1;
    }

    public void setVizViewLess1(boolean vizViewLess1) {
        this.vizViewLess1 = vizViewLess1;
    }

    public ArrayList<TweetBean> getTweetList() {
        return tweetList;
    }

    public void setTweetList(ArrayList<TweetBean> tweetList) {
        this.tweetList = tweetList;
    }

    public List<AuthorBean> getAuthorsFromList() {
        return authorsFromList;
    }

    public List<TagBean> getTagsFromList() {
        return tagsFromList;
    }

    public void setTagsFromList(List<TagBean> tagsFromList) {
        this.tagsFromList = tagsFromList;
    }

    public void setAuthorsFromList(List<AuthorBean> authorsFromList) {
        this.authorsFromList = authorsFromList;
    }

    public TopTweetsBean() {
        state = PersistentFacesState.getInstance();
    }

    public PersistentFacesState getState() {
        return state;
    }

    public ArrayList<TweetsTopBean> getTopTweetsList() {
        return topTweetsList;
    }

    public void setTopTweetsList(ArrayList<TweetsTopBean> topTweetsList) {
        this.topTweetsList = topTweetsList;
    }

}
