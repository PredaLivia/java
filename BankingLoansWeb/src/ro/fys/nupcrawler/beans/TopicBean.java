/*
 * TopicBean.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ro.fys.nupcrawler.entities.beans.Keyword;
import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.services.AccountService;
import ro.fys.nupcrawler.services.TopicsService;
import ro.fys.nupcrawler.services.constants.ActionResponseType;

/**
 * Replace with your class description.
 */
public class TopicBean {
    
    private String topicNameTxt = new String();
    private String keywordTxt = new String();
    private double keywordWeightTxt;
    private ArrayList<Keyword> keywordsList = new ArrayList<Keyword>();
    private int currentKeyword =0;
    private int currentKeywordUpdate =0;
    private Logger log = Logger.getLogger(TopicBean.class);
    private int deepnessLevel = 0;
    private int deepnessLevelUpdate = 0;
    private boolean viewPopupTopic = false;
    private String popupMsg = new String();
    private String topicNameTxtUpdate = new String();
    private String keywordTxtUpdate = new String();
    private double keywordWeightTxtUpdate;
    private ArrayList<Keyword> keywordsListUpdate = new ArrayList<Keyword>();
    
    
    public TopicBean(){}
    
    public void mainListener(ActionEvent event){
        
        
        if (event.getComponent().getId().equals("addKeyword")) {
            
            Keyword keyword = new Keyword();
            keyword.setName(keywordTxt);
            keyword.setWeight(keywordWeightTxt);
                       
            keywordsList.add(keyword);
            currentKeyword++;
            
        }
        if (event.getComponent().getId().equals("addKeywordUpdate")) {
            
            Keyword keyword = new Keyword();
            keyword.setName(keywordTxtUpdate);
            keyword.setWeight(keywordWeightTxtUpdate);
                       
            keywordsListUpdate.add(keyword);
            currentKeywordUpdate++;
            
        }
        if (event.getComponent().getId().equals("deleteKeywordUpdate")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String itemSelected = (String) hash.get("keywordSelected");
             
            log.debug(keywordsListUpdate.size());
            log.debug("Removed keyword " + itemSelected + " from list before save!");
            removeFromListUdpate(itemSelected);
            log.debug(keywordsListUpdate.size());
            
        }
        if (event.getComponent().getId().equals("deleteKeyword")) {
            Map hash = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            HashSet set = new HashSet(hash.keySet());
            String itemSelected = (String) hash.get("keywordSelected");
             
            log.debug(keywordsList.size());
            log.debug("Removed keyword " + itemSelected + " from list before save!");
            removeFromList(itemSelected);
            log.debug(keywordsList.size());
            
        }
        
        if (event.getComponent().getId().equals("saveTopic")) {
            log.debug("Saving Topic!");

            User user = new User();
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            user = (User) request.getSession().getAttribute("user");
            
            HashSet<Keyword> keys = new HashSet<Keyword>();
            keys.addAll(keywordsList);
            
            Topic topic = new Topic();
            topic.setName(topicNameTxt);
            topic.setDeepnessLevel(deepnessLevel);
            topic.setUser(user);
            topic.setKeywords(keys);
            
            
            topicNameTxt = "";
            keywordTxt = "";
            keywordWeightTxt = 0.00;
            keywordsList.removeAll(keywordsList);
            
            HashMap msg = new TopicsService().addTopic(topic);
            Set mapSet = msg.keySet();
            for (Iterator iterator = mapSet.iterator(); iterator.hasNext();) {
                Object object = (Object) iterator.next();
                log.debug("KEY " + object);
                log.debug("Value " + msg.get(object));
            }
            popupMsg = (String) msg.get(ActionResponseType.ACTION_MESSAGE_KEY);

            if (((Number)msg.get(ActionResponseType.ACTION_RESPONSE_KEY)).intValue() == 1){
                popupMsg += "The topic can be viewed in My Topics area.";
            }
            
           
            viewPopupTopic = true;
        }
        if (event.getComponent().getId().equals("closePopupTopic")) {
            viewPopupTopic = false;
        }
            
      
        if (event.getComponent().getId().equals("cancelTopic")) {
            log.debug("Cancelling Topic!");
            topicNameTxt = "";
            keywordTxt = "";
            keywordWeightTxt = 0.00;
            keywordsList.removeAll(keywordsList);
            
        }
        
        
        
//        TopicsService services = new TopicsService();
//        Topic topic = new Topic();
//        topic.setName("test");
//        User user = new User();
//        user.setName("mst");
//        topic.setUser(user);
//        
//        services.addTopic(topic);
        
    }
    
    public void removeFromListUdpate(String keywordName){
        for (int i=0; i< keywordsListUpdate.size(); i++){
            if (keywordsListUpdate.get(i).getName().equals(keywordName)){
                keywordsListUpdate.remove(i);
            }
        }
        
    }
    
    public void removeFromList(String keywordName){
        for (int i=0; i< keywordsList.size(); i++){
            if (keywordsList.get(i).getName().equals(keywordName)){
                keywordsList.remove(i);
            }
        }
        
    }

    public String getTopicNameTxt() {
        return topicNameTxt;
    }

    public void setTopicNameTxt(String topicNameTxt) {
        this.topicNameTxt = topicNameTxt;
    }

    public String getKeywordTxt() {
        return keywordTxt;
    }

    public void setKeywordTxt(String keywordTxt) {
        this.keywordTxt = keywordTxt;
    }


    public ArrayList<Keyword> getKeywordsList() {
        return keywordsList;
    }

    public void setKeywordsList(ArrayList<Keyword> keywordsList) {
        this.keywordsList = keywordsList;
    }

    public double getKeywordWeightTxt() {
        return keywordWeightTxt;
    }

    public void setKeywordWeightTxt(double keywordWeightTxt) {
        this.keywordWeightTxt = keywordWeightTxt;
    }

    public int getDeepnessLevel() {
        return deepnessLevel;
    }

    public void setDeepnessLevel(int deepnessLevel) {
        this.deepnessLevel = deepnessLevel;
    }

    public int getCurrentKeyword() {
        return currentKeyword;
    }

    public void setCurrentKeyword(int currentKeyword) {
        this.currentKeyword = currentKeyword;
    }

    public boolean isViewPopupTopic() {
        return viewPopupTopic;
    }

    public void setViewPopupTopic(boolean viewPopupTopic) {
        this.viewPopupTopic = viewPopupTopic;
    }

    public String getPopupMsg() {
        return popupMsg;
    }

    public void setPopupMsg(String popupMsg) {
        this.popupMsg = popupMsg;
    }

    public int getCurrentKeywordUpdate() {
        return currentKeywordUpdate;
    }

    public void setCurrentKeywordUpdate(int currentKeywordUpdate) {
        this.currentKeywordUpdate = currentKeywordUpdate;
    }

    public String getTopicNameTxtUpdate() {
        return topicNameTxtUpdate;
    }

    public void setTopicNameTxtUpdate(String topicNameTxtUpdate) {
        this.topicNameTxtUpdate = topicNameTxtUpdate;
    }

    public String getKeywordTxtUpdate() {
        return keywordTxtUpdate;
    }

    public void setKeywordTxtUpdate(String keywordTxtUpdate) {
        this.keywordTxtUpdate = keywordTxtUpdate;
    }

    public double getKeywordWeightTxtUpdate() {
        return keywordWeightTxtUpdate;
    }

    public void setKeywordWeightTxtUpdate(double keywordWeightTxtUpdate) {
        this.keywordWeightTxtUpdate = keywordWeightTxtUpdate;
    }

    public ArrayList<Keyword> getKeywordsListUpdate() {
        return keywordsListUpdate;
    }

    public void setKeywordsListUpdate(ArrayList<Keyword> keywordsListUpdate) {
        this.keywordsListUpdate = keywordsListUpdate;
    }

    public int getDeepnessLevelUpdate() {
        return deepnessLevelUpdate;
    }

    public void setDeepnessLevelUpdate(int deepnessLevelUpdate) {
        this.deepnessLevelUpdate = deepnessLevelUpdate;
    }

 
    
}
