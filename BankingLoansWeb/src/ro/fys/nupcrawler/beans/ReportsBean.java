package ro.fys.nupcrawler.beans;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.icesoft.faces.async.render.OnDemandRenderer;
import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.context.ByteArrayResource;
import com.icesoft.faces.context.Resource;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;

import ro.fys.nupcrawler.dao.impl.ReportDAO;
import ro.fys.nupcrawler.entities.beans.Keyword;
import ro.fys.nupcrawler.entities.beans.Report;
import ro.fys.nupcrawler.entities.beans.Scheduler;
import ro.fys.nupcrawler.entities.beans.Topic;
import ro.fys.nupcrawler.entities.beans.User;
import ro.fys.nupcrawler.services.TopicsService;

public class ReportsBean implements Serializable, Renderable{

    private String topicSelected = new String();
    private ArrayList<Keyword> topicKeywordsListReport = new ArrayList<Keyword>();
    private ArrayList<Report> topicReportsListReport = new ArrayList<Report>();
    private Resource pdfResourceForOpen;
    private static final String RESOURCE_PATH_FOR_DOWNLOAD1 = "/WEB-INF/resources/importFile.xml";
    private Resource xmlResourceForDownload;
    private static final String RESOURCE_PATH_FOR_DOWNLOAD = "/WEB-INF/resources/test2.csv";
    private Logger log = Logger.getLogger(ReportsBean.class);
    private String topicSelectedStrReport = new String();
    private List<SelectItem> topicSelectItemListReport = new ArrayList<SelectItem>();
    private User userAuthenticated = new User();
    
    protected PersistentFacesState state;
    private RenderManager renderManager;
    private OnDemandRenderer onDemandRenderer;
    
    public ReportsBean(){
        
        state = PersistentFacesState.getInstance();
        /* only for tests
         * get the topics details 
         */
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        userAuthenticated = (User) request.getSession().getAttribute("user");
        
        topicSelectItemListReport.clear();
        ArrayList<Topic> topicListItem = new TopicsService().getAllTopicsForUser(userAuthenticated.getId());
        for(int i=0; i<topicListItem.size(); i++){
            SelectItem item = new SelectItem();
            item.setLabel(topicListItem.get(i).getName());
            item.setValue(topicListItem.get(i).getId());
            topicSelectItemListReport.add(item);
        }
        
    }
    
    public PersistentFacesState getState() {

        return state;

    }
    
    public void setRenderManager(RenderManager renderManager) {
        if (onDemandRenderer == null) {
            onDemandRenderer = renderManager.getOnDemandRenderer("onDemand");
            onDemandRenderer.add(this);
        }
    }

    public void renderingException(RenderingException arg0) {
        if (onDemandRenderer != null) {
            onDemandRenderer.remove(this);
            onDemandRenderer = null;
        }
    }
    
    public void topicValueChanged(ValueChangeEvent event) {
        if (!event.getPhaseId().equals(PhaseId.INVOKE_APPLICATION)) {
            event.setPhaseId(PhaseId.INVOKE_APPLICATION);
            event.queue();
        }
    
        topicSelectItemListReport.clear();
        ArrayList<Topic> topicListItem = new TopicsService().getAllTopicsForUser(userAuthenticated.getId());
        for(int i=0; i<topicListItem.size(); i++){
            SelectItem item = new SelectItem();
            item.setLabel(topicListItem.get(i).getName());
            item.setValue(topicListItem.get(i).getId());
            topicSelectItemListReport.add(item);
        }

        Long id = 0L;
        
        if (!topicSelectedStrReport.equals("")){
            
          id = Long.parseLong(topicSelectedStrReport);  
          Topic topic = new TopicsService().getTopicInfo(id);
          
          Set setKeywords = new HashSet(topic.getKeywords());
          topicKeywordsListReport.clear();
          for (Iterator iterator = setKeywords.iterator(); iterator.hasNext();) {
              Keyword object = (Keyword) iterator.next();
              topicKeywordsListReport.add(object);
            
          }
          
          ReportDAO report = new ReportDAO();
          topicReportsListReport.clear();
          topicReportsListReport = new ReportDAO().getAllByUserAndTopic(userAuthenticated.getId(), topic.getId());
          
          for (int i=0; i< topicReportsListReport.size(); i++){
              String filePath = topicReportsListReport.get(i).getFilePath();
              topicReportsListReport.get(i).setCsvFile(buildResourceForDownload("/WEB-INF/resources/" + topicReportsListReport.get(i).getReportName()));
          }
          
        }
    }
    
    public Resource buildResourceForDownload(String resourcePath) {
        Resource xmlResourceForDownload = null;
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            xmlResourceForDownload = new ByteArrayResource(toByteArray(context.getResourceAsStream(resourcePath)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlResourceForDownload;
    }
    /**
     * Convert an InputStream into byteArray representation.
     * 
     * @param input -
     *                stream which is converted into byteArray
     * @return object in byteArray representation
     * @throws IOException
     */
    public static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buf = new byte[4096];
        int len = 0;
        while ((len = input.read(buf)) > -1)
            output.write(buf, 0, len);
        return output.toByteArray();
    }

    public String getTopicSelected() {
        return topicSelected;
    }

    public void setTopicSelected(String topicSelected) {
        this.topicSelected = topicSelected;
    }

    public ArrayList<Keyword> getTopicKeywordsList() {
        return topicKeywordsListReport;
    }

    public void setTopicKeywordsList(ArrayList<Keyword> topicKeywordsList) {
        this.topicKeywordsListReport = topicKeywordsList;
    }

    public ArrayList<Report> getTopicReportsList() {
        return topicReportsListReport;
    }

    public void setTopicReportsList(ArrayList<Report> topicReportsList) {
        this.topicReportsListReport = topicReportsList;
    }

    public Resource getPdfResourceForOpen() {
        return pdfResourceForOpen;
    }

    public void setPdfResourceForOpen(Resource pdfResourceForOpen) {
        this.pdfResourceForOpen = pdfResourceForOpen;
    }

    public Resource getXmlResourceForDownload() {
        return xmlResourceForDownload;
    }

    public void setXmlResourceForDownload(Resource xmlResourceForDownload) {
        this.xmlResourceForDownload = xmlResourceForDownload;
    }


    public String getTopicSelectedStr() {
        return topicSelectedStrReport;
    }


    public void setTopicSelectedStr(String topicSelectedStr) {
        this.topicSelectedStrReport = topicSelectedStr;
    }


    public List<SelectItem> getTopicSelectItemList() {
        return topicSelectItemListReport;
    }


    public void setTopicSelectItemList(List<SelectItem> topicSelectItemList) {
        this.topicSelectItemListReport = topicSelectItemList;
    }


    public User getUserAuthenticated() {
        return userAuthenticated;
    }


    public void setUserAuthenticated(User userAuthenticated) {
        this.userAuthenticated = userAuthenticated;
    }


    
    
}
