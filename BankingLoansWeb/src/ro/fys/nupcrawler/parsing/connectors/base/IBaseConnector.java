/*
 * BaseConnector.java
 *
 * Copyright (c) 1999-2011 ForYouSoftware. All rights reserved.
 *
 * This software is the confidential and proprietary information of ForYouSoftware. 
 * You shall not disclose such Confidential Information and shall use it only in 
 * accordance with the terms of the license agreement you entered into with ForYouSoftware.
 *	
 * Version Info:
 *
 */

package ro.fys.nupcrawler.parsing.connectors.base;

import java.util.ArrayList;
import ro.fys.nupcrawler.entities.connectors.UrlResult;
import ro.fys.nupcrawler.parsing.utils.ParsingEvents;

/**
 * Replace with your class description.
 */
public interface IBaseConnector extends ParsingEvents{

    public ArrayList<UrlResult> parseResults(int resultsNumber, String searchText);
    
}
