package ro.fys.nupcrawler.parsing.connectors;

import java.util.ArrayList;

import ro.fys.nupcrawler.entities.connectors.UrlResult;
import ro.fys.nupcrawler.parsing.connectors.base.IBaseConnector;
import ro.fys.nupcrawler.parsing.utils.*;



public class BingConnector implements IBaseConnector{
    /**
     * @uml.property  name="homePage"
     */
    String homePage = new String("http://www.bing.com//search?q=");
    
    public ArrayList<UrlResult> parseResults(int resultsNumber, String searchText){
        ArrayList<UrlResult> results = new ArrayList<UrlResult>();
        
        String processedSearchText = Utils.replaceSpaces(searchText, "+");
        String page = new SiteParser().downloadPage(homePage + processedSearchText);
        Parser pageParser = new Parser();
        int no = 1;
        
        System.out.println();
        System.out.println();
        System.out.println("================================ Bing ================================");
        
        while (pageParser.seek("<div class=\"sb_tlst\">", page, direction.FORWARD, action.SKIP) != -1 && no <= resultsNumber){
            System.out.println("Rezultat " + no);
            no++;
            UrlResult currentResult = new UrlResult();
            
            String resultUrl = pageParser.extract("<h3><a href=\"", "\"", page, actionFirst.SKIP, actionLast.SKIP);
            currentResult.setUrl(resultUrl);
            System.out.println("URL: " + resultUrl);
            
            String title = pageParser.extract(">", "</a>", page, actionFirst.SKIP, actionLast.SKIP);
            title = Utils.clearTags(title);
            currentResult.setTitle(title);
            System.out.println("Titlu: " + title);
            
//            String description = pageParser.extract("</div><p>", "</p>", page, actionFirst.SKIP, actionLast.SKIP);
//            description=  Utils.clearTags(description);
//            result.setSearchEngine(description);
//            System.out.println("Descriere: " + description);
            
            currentResult.setSearchEngine("Bing");
            System.out.println("=========================================");
            
            results.add(currentResult);
            
        }
        
        return results;
    }

}
