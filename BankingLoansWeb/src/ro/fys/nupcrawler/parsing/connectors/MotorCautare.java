package ro.fys.nupcrawler.parsing.connectors;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ro.fys.nupcrawler.entities.Credit;
import ro.fys.nupcrawler.entities.CriteriuCautare;


public class MotorCautare {
    private ConsoConn conso = new ConsoConn();
    private VreauCreditConn vreauCredit = new VreauCreditConn();
    private String cerereConso = new String();
    private String cerereVreauCredit = new String();
    private int numarRezultate;

    // creare lista tip credit
    public ArrayList<String> creareListaTipCredit() {
        ArrayList<String> listaTipcredit = new ArrayList<String>();
        listaTipcredit.add("");
        listaTipcredit.add("Credite imobiliare");
        listaTipcredit.add("Credite nevoi personale");
        listaTipcredit.add("Credite auto");

        return listaTipcredit;
    }

    // creare lista credite
    public ArrayList<String> creareListaCredite(String tiCredit) {
        ArrayList<String> listaCredite = new ArrayList<String>();
        if ("Credite imobiliare".equals(tiCredit)) {
            listaCredite.add("");
            listaCredite.add("Prima casa");
            listaCredite.add("Cumparare imobil");
            listaCredite.add("Constructie imobil");
            listaCredite.add("Modernizare imobil");
            listaCredite.add("Achizitie teren");
        } else if ("Credite nevoi personale".equals(tiCredit)) {
            listaCredite.add("");
            listaCredite.add("Nevoi personale");
            listaCredite.add("Nevoi personale cu ipoteca");
            listaCredite.add("Studii");
            listaCredite.add("Credit medical");
            listaCredite.add("Vacanta");
        } else if ("Credite auto".equals(tiCredit)) {
            listaCredite.add("");
            listaCredite.add("Masini noi");
            listaCredite.add("Masini second-hand");
        }

        return listaCredite;
    }

    // creare lista moneda
    public ArrayList<String> creareListaMoneda(String marca) {
        ArrayList<String> listaMoneda = new ArrayList<String>();

        listaMoneda.add("RON");
        listaMoneda.add("EUR");
        listaMoneda.add("USD");
        listaMoneda.add("CHF");

        return listaMoneda;
    }

    // creare lista tip credit
    // public ArrayList<String> creareListaTipCredit(String tipCredit) {
    // ArrayList<String> listaCredit = new ArrayList<String>();
    //
    // if ("Credite imobiliare".equals(tipCredit)) {
    // listaCredit.add("");
    // listaCredit.add("Prima casa");
    // listaCredit.add("Cumparare imobil");
    // listaCredit.add("Constructie imobil");
    // listaCredit.add("Modernizare imobil");
    // listaCredit.add("Achizitie teren");
    // listaCredit.add("Refinantare credit imobiliar");
    // } else if ("Credite nevoi personale".equals(tipCredit)) {
    // listaCredit.add("");
    // listaCredit.add("Nevoi personale");
    // listaCredit.add("Nevoi personale cu ipoteca");
    // listaCredit.add("Vacanta");
    // listaCredit.add("Tratamente medicale");
    // listaCredit.add("Studii");
    // listaCredit.add("Refinantare nevoi personale");
    // listaCredit.add("Refinantare nevoi personale cu ipoteca");
    // } else if ("Credite auto".equals(tipCredit)) {
    // listaCredit.add("");
    // listaCredit.add("credit auto noi");
    // listaCredit.add("Credit auto second hand");
    // }
    //
    // return listaCredit;
    // }

    // creare lista moneda
    // public ArrayList<String> creareListaModele(String marca) {
    // ArrayList<String> listaMoneda = new ArrayList<String>();
    //
    // listaMoneda.add("RON");
    // listaMoneda.add("EUR");
    // listaMoneda.add("USD");
    // listaMoneda.add("CHF");
    //
    // return listaMoneda;
    // }

    // efectuare cautare
    public ArrayList<Credit> rulareCautare(CriteriuCautare criteriu, boolean consoCheck, boolean vreauCreditCheck,
            int pag) {
        ArrayList<Credit> rezultateCautare = new ArrayList<Credit>();
        ArrayList<Credit> rezultateConso = new ArrayList<Credit>();
        ArrayList<Credit> rezultateVreauCredit = new ArrayList<Credit>();

        if (consoCheck || (!consoCheck && !vreauCreditCheck)) {
            LinkedHashMap<String, String> listaTipCreditConso = new LinkedHashMap<String, String>();
            listaTipCreditConso.put("Credite imobiliare", "cauti-credit-form-1");
            listaTipCreditConso.put("Credite nevoi personale", "cauti-credit-form-2");
            listaTipCreditConso.put("Credite auto", "cauti-credit-form-3");
            String tipCreditConso = listaTipCreditConso.get(criteriu.getTipCredit());

            ArrayList<LinkedHashMap<String, String>> arrayList = conso
                    .parseCreditListValuesByCreditTypeConso(tipCreditConso);
            LinkedHashMap<String, String> listaCreditConso = arrayList.get(0);
            String creditConso = listaCreditConso.get(criteriu.getCredit());

            LinkedHashMap<String, String> listaMonedaConso = arrayList.get(1);
            String monedaConso = listaMonedaConso.get(criteriu.getMoneda());
            String suma = new String(criteriu.getSuma());
            String perioada = new String(criteriu.getPerioada());

            System.out.println("--------------------------------------CONSO--------------------------------------");
            System.out.println("Tip credit: " + criteriu.getTipCredit() + "   cod: " + tipCreditConso);
            System.out.println("Credit: " + criteriu.getCredit() + "   cod: " + creditConso);
            System.out.println("Moneda: " + criteriu.getMoneda() + "   cod: " + monedaConso);
            System.out.println("Suma: " + suma);
            System.out.println("Perioada: " + perioada);

            System.out.println("Cerere Conso...");
            if ("Credite imobiliare".equals(criteriu.getTipCredit())) {
                rezultateConso = conso.makeSearch("http://www.conso.ro/credite-imobiliare",
                        "http://www.conso.ro/compara/credite-imobiliare",
                        "http://www.conso.ro/compara/credite-imobiliare/dae", "cauta=1&tipcredit=" + creditConso
                                + "&valuta=" + monedaConso + "&perioada=" + perioada + "&suma=" + suma
                                + "&recalculeaza=", criteriu.getTipCredit(), criteriu.getMoneda(), criteriu.getSuma());
            } else if ("Credite nevoi personale".equals(criteriu.getTipCredit())) {
                rezultateConso = conso.makeSearch("http://www.conso.ro/credite-nevoi-personale",
                        "http://www.conso.ro/compara/credite-consum", "http://www.conso.ro/compara/credite-consum/dae",
                        "cauta=1&tipcredit=" + creditConso + "&valuta=" + monedaConso + "&perioada=" + perioada
                                + "&suma=" + suma + "&recalculeaza=", criteriu.getTipCredit(), criteriu.getMoneda(),
                        criteriu.getSuma());
            } else if ("Credite auto".equals(criteriu.getTipCredit())) {
                rezultateConso = conso.makeSearch("http://www.conso.ro/credite-auto-leasing",
                        "http://www.conso.ro/compara/credite-auto", "http://www.conso.ro/compara/credite-auto/dae",
                        "cauta=1&tipcredit=" + creditConso + "&valuta=" + monedaConso + "&perioada=" + perioada
                                + "&suma=" + suma + "&recalculeaza=", criteriu.getTipCredit(), criteriu.getMoneda(),
                        criteriu.getSuma());
            }
        }

        if (vreauCreditCheck || (!consoCheck && !vreauCreditCheck)) {
            String suma = new String(criteriu.getSuma());
            String perioada = new String(criteriu.getPerioada());

            String tipCreditVreauCredit = new String(criteriu.getTipCredit());
            LinkedHashMap<String, String> mapariCrediteVreauCredit = vreauCredit.getCreditMappings();
//            LinkedHashMap<String, String> listaLinkCrediteVreauCredit = vreauCredit.parseCreditLinks();
            LinkedHashMap<String, String> listaMonedaVreauCredit = vreauCredit.getMonedaMappings();

            String creditVreauCredit = mapariCrediteVreauCredit.get(criteriu.getCredit());
            String monedaVreauCredit = listaMonedaVreauCredit.get(criteriu.getMoneda());
            String sumaInEuro = "";
            int sumaTemp = 0;
            if ("USD".equals(criteriu.getMoneda())) {
                sumaTemp = (int) ((new Integer(suma) * 0.781));
                sumaInEuro = "" + sumaTemp;
            } else if ("CHF".equals(criteriu.getMoneda())) {
                sumaTemp = (int) ((new Integer(suma) * 0.804));
                sumaInEuro = "" + sumaTemp;
            } else if ("RON".equals(criteriu.getMoneda())) {
                sumaTemp = (int) ((new Integer(suma) * 0.225));
                sumaInEuro = "" + sumaTemp;
            } else {
                sumaInEuro = suma;
            }

            System.out
                    .println("--------------------------------------VREAUCREDIT--------------------------------------");
            System.out.println("Tip credit: " + criteriu.getTipCredit() + "   cod: " + criteriu.getTipCredit());
            System.out.println("Credit: " + criteriu.getCredit() + "   cod: " + creditVreauCredit);
            System.out.println("Moneda: " + criteriu.getMoneda() + "   cod: " + monedaVreauCredit);
            System.out.println("Suma in euro: " + suma);
            System.out.println("Perioada: " + perioada);

            System.out.println("Cerere VreauCredit...");

            String cerereVreauCredit = "http://www.vreaucredit.ro/credite/?act=filter&focus_results=true&target=1&categorie[]="+creditVreauCredit+"&qBanca=&moneda[]="+monedaVreauCredit+"&valoare_imprumut="+sumaInEuro+"&perioada="+perioada+"+ani&avans=&order[col]=&order[dir]=";

            System.out.println(cerereVreauCredit);

            rezultateVreauCredit = vreauCredit.parseResultsVreauCredit(cerereVreauCredit, criteriu.getTipCredit(),
                    criteriu.getMoneda(), criteriu.getSuma());
        }

        rezultateCautare = new ArrayList<Credit>();

        System.out.println();
        System.out.println("-------------------------------");
        System.out.println("Numar rezultate conso.ro: " + rezultateConso.size());
        System.out.println("Numar rezultate vreaucredit.ro: " + rezultateVreauCredit.size());

        for (int i = 0; i < rezultateConso.size(); i++) {
            rezultateCautare.add(rezultateConso.get(i));
        }
        for (int i = 0; i < rezultateVreauCredit.size(); i++) {
            rezultateCautare.add(rezultateVreauCredit.get(i));
        }

        numarRezultate = rezultateCautare.size();

        return rezultateCautare;

    }

    public int getNumarRezultate() {
        return numarRezultate;
    }

    public void setNumarRezultate(int numarRezultate) {
        this.numarRezultate = numarRezultate;
    }

}