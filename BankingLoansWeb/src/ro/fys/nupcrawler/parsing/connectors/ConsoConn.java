package ro.fys.nupcrawler.parsing.connectors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import parsing.Parser;
import parsing.ParsingEvents;
import ro.fys.nupcrawler.entities.Credit;

public class ConsoConn implements ParsingEvents {

    private String cookie = new String();
    private String nextLocation = new String();
    private String mainPageURL = "http://www.conso.ro";
    private int numarRezultate;

    public ConsoConn() {

    }

    public ArrayList<Credit> makeSearch(String categoryURL, String compareURL, String compareUrlDAE, String query,
            String tipCredit, String moneda, String suma) {
        ArrayList<Credit> rezultate = new ArrayList<Credit>();

        /* main page request */
        String mainPageContent = makeRequest(mainPageURL, null);
        // System.out.println(mainPageContent);

        /* imobiliare page request */
        // String secondPageURL = "http://www.conso.ro/credite-imobiliare";
        String secondPageURL = categoryURL;
        String secondPageContent = makeRequest(secondPageURL, null);
        // System.out.println(secondPageContent);

        /* imobiliare search page request */
        // String searchPageURL =
        // "http://www.conso.ro/compara/credite-imobiliare";credite-consum
        // perioada=5&online=0&tipcredit=1&valuta=0&suma=10000&submit=&cauta=1
        String searchPageURL = compareURL;
        String searchPageContent = makeRequest(searchPageURL, query);
        // System.out.println(searchPageContent);

        // String resultPageURL =
        // "http://www.conso.ro/compara/credite-imobiliare/dae";
        String resultPageURL = compareUrlDAE;
        String resultPageContent = makeRequest(resultPageURL, null);
        // System.out.println(resultPageContent);

        ArrayList<Credit> temp = parseResultsConso(resultPageContent, compareUrlDAE, tipCredit, moneda, suma);
        for (int i = 0; i < temp.size(); i++) {
            rezultate.add(temp.get(i));
        }

        Parser parser = new Parser();
        while (parser.seek("next\" title=\"Pagina urmatoare\"", resultPageContent, direction.FORWARD, action.SKIP) != -1) {
            parser.seek("href", resultPageContent, direction.BACKWARD, action.SKIP);
            String nextUrl = parser.extract("\"", "\"", resultPageContent, actionFirst.SKIP, actionLast.SKIP);
            parser.first = -1;

            resultPageContent = makeRequest(nextUrl, null);

            // System.out.println("Page " + nextUrl);
            temp.clear();
            temp = parseResultsConso(resultPageContent, compareUrlDAE, tipCredit, moneda, suma);
            for (int i = 0; i < temp.size(); i++) {
                rezultate.add(temp.get(i));
            }
        }

        return rezultate;

    }

    public String makeRequest(String url, String postFields) {
        String content = new String();
        String setCookie = new String();

        try {

            URL conn = new URL(url);
            HttpURLConnection request = (HttpURLConnection) conn.openConnection();
            request.setFollowRedirects(false);
            // set the User Agent
            request.setRequestProperty("User-Agent",
                    "Mozilla/5.0 (Windows NT 5.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1");

            // set Cookie
            // if (cookie.length()>0){
            System.out.println(cookie);
            request.setRequestProperty("Cookie", cookie);
            // }

            // set the Post fields if the request is POST
            if (postFields != null) {
                String encodedPostString = postFields;
                request.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(request.getOutputStream());
                wr.write(encodedPostString);
                wr.flush();
                wr.close();
            } else {
                request.connect();
            }

            if (cookie.length() == 0) {
                setCookie = request.getHeaderField(7);
            }

            if (setCookie.length() > 0) {
                cookie = setCookie;
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String inputLine = new String();

            while ((inputLine = in.readLine()) != null) {
                content += inputLine;
            }

            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }

    public ArrayList<Credit> parseResultsConso(String resultPage, String url, String tipCredit, String moneda,
            String suma) {
        ArrayList<Credit> rezultate = new ArrayList<Credit>();
//        System.out.println(resultPage);
        Parser parser = new Parser();

        while (parser.seek("<tr class=", resultPage, direction.FORWARD, action.SKIP) != -1) {
            Credit credit = new Credit();
            credit.setCategorieCredit(tipCredit);
            credit.setMoneda(moneda);
            credit.setSuma(suma);
            credit.setUrlRecord(url);

            String bancaSection = "";
            // if ("Credite auto".equals(tipCredit)){
            // bancaSection = parser.extract(
            // "<td style=\" width:107px;\" class=\"left \"><div style=\" width:107px;\">",
            // "</div>", resultPage,
            // actionFirst.SKIP, actionLast.SKIP);
            // } else if ("Credite imobiliare".equals(tipCredit) ||
            // "Credite nevoi personale".equals(tipCredit)){
            bancaSection = parser.extract("<td style=\" width:18px;  \"><div style=\" width:18px;\">", "Detalii</a>",
                    resultPage, actionFirst.SKIP, actionLast.SKIP);
            // }

            Parser bancaParser = new Parser();
            // nume banca
            String banca = bancaParser.extract("<div class=\"logoBanca\">", "</div>", bancaSection, actionFirst.SKIP,
                    actionLast.SKIP);
            if (banca.contains("img")) {
                banca = bancaParser.extract("class=\"logo-banca\" alt=\"", "\"", bancaSection, actionFirst.SKIP,
                        actionLast.SKIP);
            }
            credit.setBanca(banca);

            // nume credit
            String numeCredit = bancaParser.extract("<div class=\"detaliiBanca\">", "</div>", bancaSection,
                    actionFirst.SKIP, actionLast.NOTHING);
            if (numeCredit.contains("<img")) {
                numeCredit = bancaParser.extract("</p>", "</div>", bancaSection, actionFirst.SKIP, actionLast.SKIP);
                numeCredit = numeCredit.trim();
            }
            credit.setNumeCredit(numeCredit);

            // dae
            String dae = bancaParser.extract(
                    "<td style=\"width:40px; \"  class=\"active\"><div style=\" width:40px;\">", "</div>",
                    bancaSection, actionFirst.SKIP, actionLast.SKIP);
            dae = dae.trim();
            credit.setDae(dae);

            // cost total
            String costTotal = bancaParser.extract("Cost total:", "<br />", bancaSection, actionFirst.SKIP,
                    actionLast.SKIP);
            costTotal = costTotal.trim();
            credit.setCostTotal(costTotal);

            // prima rata
            String primaRata = bancaParser.extract("Prima rata:", "<br />", bancaSection, actionFirst.SKIP,
                    actionLast.SKIP);
            primaRata = primaRata.trim();
            credit.setPrimaRata(primaRata.replace("&nbsp;", ""));

            bancaParser.seek("cb2", bancaSection, direction.FORWARD, action.SKIP);
            String postParam = bancaParser.extract("value=\"", "\"", bancaSection, actionFirst.SKIP, actionLast.SKIP);
            String postDetails = "?ordoneaza_dupa=dae&cb0=" + postParam + "&ordoneaza_dupa=dae";
            String pageDetails = makeRequest("http://www.conso.ro/compara/credite-imobiliare/2", postDetails);

//            System.out.println(pageDetails);
            Parser detailsParser = new Parser();

            // tip dobanda
            detailsParser.seek("<td align=\"left\">Tip dobanda</td>", pageDetails, direction.FORWARD, action.SKIP);
            String tipDobanda = detailsParser.extract("<td align=\"center\" width=\"80%\">", "</td>", pageDetails,
                    actionFirst.SKIP, actionLast.SKIP);
            tipDobanda=tipDobanda.replace("&nbsp;", "");
            credit.setTipDobanda(tipDobanda);

            // rata dobanda
            detailsParser.seek("<td align=\"left\">Mod de fluctuatie dobanda</td>", pageDetails, direction.FORWARD,
                    action.SKIP);
            String rataDobanda = detailsParser.extract("<td align=\"center\" width=\"80%\">", "</td>", pageDetails,
                    actionFirst.SKIP, actionLast.SKIP);
            rataDobanda = rataDobanda.replace("&nbsp;", "");
            credit.setRataDobanda(rataDobanda);

            // // comision de acordare
            // parser.seek("<div class=\"bank-details\" align=\"left\">",
            // resultPage, direction.FORWARD, action.SKIP);
            // parser.seek("COMISION DE ACORDARE:", resultPage,
            // direction.FORWARD, action.SKIP);
            // String comision = "Comision de acordare: " +
            // parser.extract("<span>", "</span>", resultPage, actionFirst.SKIP,
            // actionLast.SKIP) + "\n";
            // parser.seek("COMISION ANUAL:", resultPage, direction.FORWARD,
            // action.SKIP);
            // comision += "Comision anual: " + parser.extract("<span>",
            // "</span>", resultPage, actionFirst.SKIP, actionLast.SKIP);
            // credit.setComisioane(comision);
            //
            // // venit minim
            // parser.seek("VENIT MINIM", resultPage, direction.FORWARD,
            // action.SKIP);
            // String venit = parser.extract("<span>", "</span>", resultPage,
            // actionFirst.SKIP, actionLast.SKIP);
            // credit.setVenitMinim(venit);

            rezultate.add(credit);
        }

        return rezultate;
    }

    // populare liste de valori conform tipului de credit ales
    // nume credit si moneda aleasa
    public ArrayList<LinkedHashMap<String, String>> parseCreditListValuesByCreditTypeConso(String creditType) {
        ArrayList<LinkedHashMap<String, String>> resultArray = new ArrayList<LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> creditHash = new LinkedHashMap<String, String>();
        String page = makeRequest(mainPageURL, null);
        Parser pageParser = new Parser();
        String id = "";
        String value = "";

        pageParser.seek(creditType, page, direction.FORWARD, action.SKIP);
        String tipCreditSection = pageParser.extract("<select name=\"tipcredit\"", "</select>", page, actionFirst.SKIP,
                actionLast.SKIP);
        Parser tipCreditParser = new Parser();
        while (tipCreditParser.seek("option value=", tipCreditSection, direction.FORWARD, action.SKIP) != -1) {
            id = tipCreditParser.extract("\"", "\"", tipCreditSection, actionFirst.SKIP, actionLast.SKIP);
            value = tipCreditParser.extract(">", "<", tipCreditSection, actionFirst.SKIP, actionLast.SKIP);
            System.out.println("Id credit: " + id + "   Nume credit: " + value);
            creditHash.put(value, id);
        }
        resultArray.add(creditHash);

        System.out.println();

        LinkedHashMap<String, String> monedaHash = new LinkedHashMap<String, String>();
        String monedaSection = pageParser.extract("<select name=\"valuta\"", "</select>", page, actionFirst.SKIP,
                actionLast.SKIP);
        Parser monedatParser = new Parser();
        while (monedatParser.seek("option value=", monedaSection, direction.FORWARD, action.SKIP) != -1) {
            id = monedatParser.extract("\"", "\"", monedaSection, actionFirst.SKIP, actionLast.SKIP);
            value = monedatParser.extract(">", "<", monedaSection, actionFirst.SKIP, actionLast.SKIP);
            System.out.println("Id moneda: " + id + "   Nume moneda: " + value);
            monedaHash.put(value, id);
        }
        resultArray.add(monedaHash);

        return resultArray;
    }

}
