package ro.fys.nupcrawler.parsing.connectors;

import java.util.ArrayList;

import ro.fys.nupcrawler.entities.connectors.UrlResult;
import ro.fys.nupcrawler.parsing.connectors.base.IBaseConnector;
import ro.fys.nupcrawler.parsing.utils.*;

public class YahooConnector implements IBaseConnector {
    /**
     * @uml.property  name="homePage"
     */
    String homePage = new String("http://search.yahoo.com/search?p=");

    public ArrayList<UrlResult> parseResults(int resultsNumber, String searchText) {
        ArrayList<UrlResult> results = new ArrayList<UrlResult>();

        String processedSearchText = Utils.replaceSpaces(searchText, "+");
        String page = new SiteParser().downloadPage(homePage + processedSearchText);
        Parser pageParser = new Parser();
        int no = 1;

        System.out.println();
        System.out.println();
        System.out.println("================================ Yahoo ================================");

        while (pageParser.seek("<h3><a class=", page, direction.FORWARD, action.SKIP) != -1 && no <= resultsNumber) {
            System.out.println("Rezultat " + no);
            no++;
            UrlResult currentResult = new UrlResult();

            pageParser.seek("http%3a", page, direction.FORWARD, action.SKIP);
            String resultUrl = pageParser.extract("//", "\"", page, actionFirst.SKIP, actionLast.SKIP);
            currentResult.setUrl("http://" + resultUrl);
            System.out.println("URL: " + resultUrl);

            String title = pageParser.extract(">", "</a>", page, actionFirst.SKIP, actionLast.SKIP);
            title = Utils.clearTags(title);
            currentResult.setTitle(title);
            System.out.println("Titlu: " + title);

            currentResult.setSearchEngine("Yahoo");
            System.out.println("=========================================");
            
            results.add(currentResult);

        }

        return results;
    }

}
