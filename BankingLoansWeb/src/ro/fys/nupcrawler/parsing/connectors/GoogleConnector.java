package ro.fys.nupcrawler.parsing.connectors;

import java.util.ArrayList;

import ro.fys.nupcrawler.entities.connectors.UrlResult;
import ro.fys.nupcrawler.parsing.connectors.base.IBaseConnector;
import ro.fys.nupcrawler.parsing.utils.*;

public class GoogleConnector implements IBaseConnector {
    /**
     * @uml.property  name="homePage"
     */
    String homePage = new String("http://www.google.ro/search?q=");

    public ArrayList<UrlResult> parseResults(int resultsNumber, String searchText) {
        ArrayList<UrlResult> results = new ArrayList<UrlResult>();

        String processedSearchText = Utils.replaceSpaces(searchText, "%20");
        String page = new SiteParser().downloadPage(homePage + processedSearchText);
        System.out.println(page);
        Parser pageParser = new Parser();
        int no = 1;

        System.out.println();
        System.out.println();
        System.out.println("================================ Google ================================");

        while (pageParser.seek("<h3 class=", page, direction.FORWARD, action.SKIP) != -1 && no <= resultsNumber) {
            System.out.println("Rezultat " + no);
            no++;
            UrlResult currentResult = new UrlResult();

            String resultUrl = pageParser.extract("href=\"", "\"", page, actionFirst.SKIP, actionLast.SKIP);
            currentResult.setUrl(resultUrl);
            System.out.println("URL: " + resultUrl);

            String title = pageParser.extract(">", "</a>", page, actionFirst.SKIP, actionLast.SKIP);
            currentResult.setTitle(title);
            title = Utils.clearTags(title);
            System.out.println("Titlu: " + title);

            // String description = pageParser.extract("<div class=\"s\">",
            // "<br>", page, actionFirst.SKIP, actionLast.SKIP);
            // description= Utils.clearTags(description);
            // result.setSearchEngine(description);
            // System.out.println("Descriere: " + description);

            currentResult.setSearchEngine("Google");
            System.out.println("=========================================");
            
            results.add(currentResult);

        }

        return results;
    }
}
