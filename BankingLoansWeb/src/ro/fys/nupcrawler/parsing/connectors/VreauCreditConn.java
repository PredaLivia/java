package ro.fys.nupcrawler.parsing.connectors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import parsing.Parser;
import parsing.ParsingEvents;
import ro.fys.nupcrawler.entities.Credit;

public class VreauCreditConn implements ParsingEvents {

    public static String homePage = "http://www.vreaucredit.ro";

    public VreauCreditConn() {

    }

    // legatura intre numele creditelor din interfata si creditele din site-ul
    // nativ
    public LinkedHashMap<String, String> getMonedaMappings() {
        LinkedHashMap<String, String> moneda = new LinkedHashMap<String, String>();
        moneda.put("RON", "1");
        moneda.put("EUR", "2");
        moneda.put("USD", "3");
        moneda.put("CHF", "4");

        return moneda;
    }

    // legatura intre numele creditelor din interfata si creditele din site-ul
    // nativ
    public LinkedHashMap<String, String> getCreditMappings() {
        LinkedHashMap<String, String> credit = new LinkedHashMap<String, String>();
        credit.put("Prima casa", "13");
        credit.put("Cumparare imobil", "14");
        credit.put("Constructie imobil", "16");
        credit.put("Modernizare imobil", "17");
        credit.put("Achizitie teren", "15");
        credit.put("Nevoi personale", "6");
        credit.put("Nevoi personale cu ipoteca", "5");
        credit.put("Vacanta", "9");
        credit.put("Tratamente medicale", "10");
        credit.put("Studii", "8");
        credit.put("Masini noi", "28");
        credit.put("Masini second-hand", "29");

        return credit;
    }

    // obtinere continut pagina web
    public String getPageContent(String url) {
        StringBuffer pageBuffer = new StringBuffer();

        try {

            URL pageConnect = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) pageConnect.openConnection();
            connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

            BufferedReader in0 = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String inputLine = new String();

            while ((inputLine = in0.readLine()) != null) {
                pageBuffer.append(inputLine);

            }

        } catch (MalformedURLException me) {
            System.out.println("MalformedURLException: " + me);
        } catch (IOException ie) {
            System.out.println("IO exception: " + ie);
        }
        return pageBuffer.toString();
    }

    public LinkedHashMap<String, String> parseCreditLinks() {
        LinkedHashMap<String, String> creditLinks = new LinkedHashMap<String, String>();
        String page = getPageContent(homePage);
        Parser parser = new Parser();
        parser.seek("Alege cum vrei sa gasesti creditul dorit", page, direction.FORWARD, action.SKIP);

        String creditSection = parser
                .extract("<div class=\"cont\">", "</div>", page, actionFirst.SKIP, actionLast.SKIP);
        Parser creditParser = new Parser();
        String creditLink = "";
        String credit = "";
        while (creditParser.seek("<li>", creditSection, direction.FORWARD, action.SKIP) != -1) {
            creditLink = creditParser.extract("href=\"", "\"", creditSection, actionFirst.SKIP, actionLast.SKIP);
            credit = creditParser.extract(">", "</a>", creditSection, actionFirst.SKIP, actionLast.SKIP);
            creditLinks.put(credit, creditLink);
        }

        return creditLinks;
    }

    public String parserCreditLinkByNameAndCurrency(String urlCredit, String currency) {
        String page = getPageContent(urlCredit);
        Parser parser = new Parser();

        parser.seek("Selectati moneda", page, direction.FORWARD, action.SKIP);

        String creditSection = parser.extract("<div>", "</div>", page, actionFirst.SKIP, actionLast.SKIP);
        Parser creditParser = new Parser();
        String link = "";
        String moneda = "";
        while (creditParser.seek("<li>", creditSection, direction.FORWARD, action.SKIP) != -1) {
            link = creditParser.extract("href=\"", "\"", creditSection, actionFirst.SKIP, actionLast.SKIP);
            moneda = creditParser.extract(">", "</a>", creditSection, actionFirst.SKIP, actionLast.SKIP);
            if (moneda.contains(currency)) {
                return link;
            }
        }
        return "";
    }

    public ArrayList<Credit> parseResultsVreauCredit(String urlPage, String tipCredit, String moneda, String suma) {
        String resultPage = getPageContent(urlPage);
//        System.out.println(resultPage);
        ArrayList<Credit> rezultate = new ArrayList<Credit>();
        Parser parser = new Parser();
        parser.seek("class=\"add-all-favorite\"", resultPage, direction.FORWARD, action.SKIP);

        while (parser.seek("item clearfix nopromo", resultPage, direction.FORWARD, action.SKIP) != -1) {
            Credit credit = new Credit();
            credit.setCategorieCredit(tipCredit);
            credit.setMoneda(moneda);
            credit.setSuma(suma);
            String bancaSection = parser.extract("\"", "Sumar</a>", resultPage, actionFirst.SKIP, actionLast.SKIP);
//            System.out.println(bancaSection);

            if (!bancaSection.contains("<span class=\"icon-pentru\"></span>")) {
                Parser bancaParser = new Parser();

                // identificare link detalii credit
                bancaParser.seek("<div class=\"c2 \">", bancaSection, direction.FORWARD, action.SKIP);
                String adresaDetalii = bancaParser.extract("href=\"", "\"", bancaSection, actionFirst.SKIP,
                        actionLast.SKIP);
                credit.setUrlRecord(adresaDetalii);

                // rata dobanda
                bancaParser.seek("<div class=\"c3", bancaSection, direction.FORWARD, action.SKIP);
                String rataDobanda = bancaParser.extract("<dd>", "</dd>", bancaSection, actionFirst.SKIP,
                        actionLast.SKIP);
                rataDobanda = clearTags(rataDobanda);
                credit.setRataDobanda(rataDobanda);

                // dae
                bancaParser.seek("<div class=\"c4", bancaSection, direction.FORWARD, action.SKIP);
                String dae = bancaParser.extract("</div>", "</div>", bancaSection, actionFirst.SKIP, actionLast.SKIP);
                credit.setDae(dae);

                // prima rata
                bancaParser.seek("<div class=\"c5", bancaSection, direction.FORWARD, action.SKIP);
                String primaRata = bancaParser.extract("</div>", "</div>", bancaSection, actionFirst.SKIP,
                        actionLast.SKIP);
                credit.setPrimaRata(primaRata);

                // cost total
                bancaParser.seek("<div class=\"c6", bancaSection, direction.FORWARD, action.SKIP);
                String costTotal = bancaParser.extract("</div>", "</div>", bancaSection, actionFirst.SKIP,
                        actionLast.SKIP);
                credit.setCostTotal(costTotal);

                String pageDetalii = getPageContent(adresaDetalii);
                if (pageDetalii.contains("301 Moved Permanently")){
                    Parser tempParser = new Parser();
                    adresaDetalii = tempParser.extract("href=\"", "\"", pageDetalii, actionFirst.SKIP, actionLast.SKIP);
                }
                pageDetalii = getPageContent(adresaDetalii);
                Parser detaliiParser = new Parser();
                detaliiParser.seek("<h2>Informatii generale</h2>", pageDetalii, direction.FORWARD, action.SKIP);

                // nume credit
                String numeCredit = "";
                detaliiParser.seek("<dt>Denumire credit</dt>", pageDetalii, direction.FORWARD, action.NOTHING);
                numeCredit = detaliiParser.extract("<dd>", "</dd>", pageDetalii, actionFirst.SKIP, actionLast.SKIP);
                credit.setNumeCredit(numeCredit);

                // nume banca
                String banca = "";
                detaliiParser.seek("<dt>Banca</dt>", pageDetalii, direction.FORWARD, action.SKIP);
                banca = detaliiParser.extract("\">", "</a>", pageDetalii, actionFirst.SKIP, actionLast.SKIP);
                credit.setBanca(banca);

                // tip dobanda
                detaliiParser.seek("<h2>Dobanzi</h2>", pageDetalii, direction.FORWARD, action.SKIP);
                String tipDobanda = detaliiParser.extract("<dt>", "</dt>", pageDetalii, actionFirst.SKIP,
                        actionLast.SKIP);
                credit.setTipDobanda(tipDobanda);

                // venit minim
                // detaliiParser.seek("<h2>Conditii de acordare</h2>",
                // pageDetalii, direction.FORWARD, action.SKIP);
                // detaliiParser.seek("<dt>Venit net lunar minim imprumutat</dt>",
                // pageDetalii, direction.FORWARD, action.SKIP);
                // String venit = detaliiParser.extract("<dd>", "</dd>",
                // pageDetalii, actionFirst.SKIP, actionLast.SKIP);
                // credit.setVenitMinim(venit);

                rezultate.add(credit);
            }

            // comisioane
            // String comisioaneSection = detaliiParser.extract("<ul>", "</ul>",
            // pageDetalii, actionFirst.SKIP,
            // actionLast.SKIP);
            // Parser comisioaneParser = new Parser();
            // String comisioane = "";
            // while (comisioaneParser.seek("<li", comisioaneSection,
            // direction.FORWARD, action.SKIP) != -1) {
            // comisioane += comisioaneParser.extract(">", "</",
            // comisioaneSection, actionFirst.SKIP, actionLast.SKIP)
            // + "\n";
            // }
            // credit.setComisioane(comisioane);

        }

        return rezultate;
    }

    // eliminare tag-uri HTML
    private String clearTags(String tags) {

        int begin, end;
        String tag = tags;
        while (tag.indexOf("<") != -1) {
            begin = tag.indexOf("<");
            end = tag.indexOf(">", begin);
            if (end != -1) {
                tags = tag.substring(0, begin) + "" + tag.substring(end + 1, tag.length());
                tag = tags;
            }
        }
        return tags;
    }

}
