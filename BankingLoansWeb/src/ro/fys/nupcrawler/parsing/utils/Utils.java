package ro.fys.nupcrawler.parsing.utils;

public class Utils {
    
 // remove HTML tags from text
    public static String clearTags(String tags) {

        int begin, end;
        String tag = tags;
        while (tag.indexOf("<") != -1) {
            begin = tag.indexOf("<");
            end = tag.indexOf(">", begin);
            if (end != -1) {
                tags = tag.substring(0, begin) + "" + tag.substring(end + 1, tag.length());
                tag = tags;
            }
        }
        return tags;
    }
    
    public static String replaceSpaces(String text, String replacement){
        return text.replace(" ", replacement);
    }
}
