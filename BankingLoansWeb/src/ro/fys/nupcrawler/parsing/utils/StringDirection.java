package ro.fys.nupcrawler.parsing.utils;

public class StringDirection {
    /**
     * @                         The direction in which is started to search a substring into a string.  FORWARD - starting to RIGHT from stringToMatch
     * @uml.property  name="fORWARD"
     * @uml.associationEnd  multiplicity="(1 1)"
     */
    public StringDirectionMod FORWARD = new StringDirectionMod("forward");

    /**
     * @                         The direction in which is started to search a substring into a string.  BACKWARD - starting to LEFT from stringToMatch
     * @uml.property  name="bACKWARD"
     * @uml.associationEnd  multiplicity="(1 1)"
     */
    public StringDirectionMod BACKWARD = new StringDirectionMod("backward");

    public StringDirection() {
    }
}
