package ro.fys.nupcrawler.parsing.utils;

public interface ParsingEvents {

	public StringDirection direction = new StringDirection();
	public StringAction action = new StringAction();
	public StringAction actionFirst = new StringAction();
	public StringAction actionLast = new StringAction();
	
}
