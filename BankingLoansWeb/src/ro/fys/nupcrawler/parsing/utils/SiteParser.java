package ro.fys.nupcrawler.parsing.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SiteParser {
    /**
     * @uml.property  name="resultsVector"
     */
    public Set<HashMap<String, Object>> resultsVector = null;
    private final static int RETRIES_NO = 5;
    private static HttpURLConnection connection = null;
    private static String cookieString = "";

    public SiteParser() {
        resultsVector = new HashSet<HashMap<String, Object>>();
    }

    public String downloadPage(String url) {
        return this.downloadPage(url, null);
    }

    protected String downloadPage(String url, String cookies) {
        int retries = 0;
        StringBuffer sb = new StringBuffer();

        boolean connected = false;
        while (retries <= RETRIES_NO && !connected) {
            try {
                URL connect0 = new URL(url);
                connection = (HttpURLConnection) connect0.openConnection();
                connection.setReadTimeout(20000);
                connection.setConnectTimeout(20000);
                connection
                        .addRequestProperty("User-Agent",
                                "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 (.NET CLR 3.5.30729)");

                if (cookies != null) {
                    connection.setRequestProperty("Cookie", cookies);
                }

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

                String inputLine = new String();
                while ((inputLine = reader.readLine()) != null) {
                    sb.append(inputLine);
                }
                reader.close();
                connected = true;

                Map<String, List<String>> lastHeaders = connection.getHeaderFields();
                List<String> cookieList = lastHeaders.get("Set-Cookie");

                cookieString = "";
                if (cookieList != null) {
                    for (String key : cookieList) {
                        cookieString += key;
                    }
                }

            } catch (Exception e) {
                System.out.println("Exception reading page: " + e.getMessage());
                retries++;
                connected = false;
            }
        }
        return sb.toString();
    }

    protected String downloadPostPage(String url, String postData, String cookies) {
        int retries = 0;
        StringBuffer sb = new StringBuffer();

        while (retries <= RETRIES_NO) {
            try {
                URL connect0 = new URL(url);
                connection = (HttpURLConnection) connect0.openConnection();
                connection
                        .addRequestProperty("User-Agent",
                                "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 (.NET CLR 3.5.30729)");

                if (cookies != null) {
                    connection.setRequestProperty("Cookie", cookies);
                    connection.setRequestProperty("Keep-Alive", "300");
                }

                connection.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                postData = URLEncoder.encode(postData, "UTF-8");
                // postData = postData.replace("%3D", "=").replace("%26",
                // "&").replace("%2F", "/");
                wr.write(postData);
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String inputLine = new String();

                while ((inputLine = reader.readLine()) != null) {
                    sb.append(inputLine);
                }
                wr.close();
                reader.close();

                Map<String, List<String>> lastHeaders = connection.getHeaderFields();
                List<String> cookieList = lastHeaders.get("Set-Cookie");

                cookieString = "";
                if (cookieList != null) {
                    for (String key : cookieList) {
                        cookieString += key;
                    }
                }

                break;
            } catch (Exception e) {
                System.out.println("Exception reading page:" + e.getMessage());
                retries++;
            }
        }
        return sb.toString();
    }

    protected String getCookieString() {
        return cookieString;
    }

    /**
     * @param path
     * @param picName
     * @param urlName
     */
    protected void downloadImage(String path, String picName, String urlName) {
        urlName = urlName.replace(" ", "%20");

        try {
            URL url = new URL(urlName);
            HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setConnectTimeout(20000);
            InputStream in = url.openStream();
            in = urlConn.getInputStream();

            // make folders
            File f = new File(path);
            f.mkdirs();

            String location = path + File.separator + picName;

            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(location));
            byte[] buf = new byte[4 * 1024]; // 4K buffer
            int bytesRead;
            while ((bytesRead = in.read(buf)) != -1) {
                out.write(buf, 0, bytesRead);
            }
            out.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Get the results extracted so far...
     * 
     * @return
     */
    public Set<HashMap<String, Object>> getResultsVector() {
        return resultsVector;
    }
}