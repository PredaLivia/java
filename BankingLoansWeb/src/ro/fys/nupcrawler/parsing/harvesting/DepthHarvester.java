package ro.fys.nupcrawler.parsing.harvesting;

import java.net.*;
import java.util.ArrayList;

import ro.fys.nupcrawler.entities.connectors.UrlEntity;
import ro.fys.nupcrawler.parsing.utils.*;

public class DepthHarvester implements ParsingEvents {

    /**
     * @uml.property  name="allChildrenUrls"
     * @uml.associationEnd  multiplicity="(0 -1)" elementType="java.lang.String"
     */
    private ArrayList<String> allChildrenUrls = new ArrayList<String>();

    public ArrayList<UrlEntity> parseInDepth(String url, int maxDepthLevel) {
        System.out.println("MAX ESTE: " + maxDepthLevel);

        return parseInDepth(url, maxDepthLevel, 1);
    }

    private ArrayList<UrlEntity> parseInDepth(String url, int maxDepthLevel, int currentDepthLevel) {
        ArrayList<UrlEntity> result = new ArrayList<UrlEntity>();
        
        if (currentDepthLevel == 1) {
            allChildrenUrls = new ArrayList<String>();
        }
        

        if (currentDepthLevel <= maxDepthLevel) {
            currentDepthLevel += 1;
            String host = extractHost(url); 

            String page = new SiteParser().downloadPage(url);
            if(page == null || page.trim().length() == 0){
                return result;
            }
            Parser parser = new Parser();
            UrlEntity entity = new UrlEntity();
            entity.setUrl(url);
            entity.setContent(page);

            entity.setHost(host);

            ArrayList<String> childrenUrls = new ArrayList<String>();
            int count = 0;
            while (parser.seek(" href=\"", page, direction.FORWARD, action.NOTHING) != -1 && count < 100) {
                count ++;
                String tempUrl = parser.extract("\"", "\"", page, actionFirst.SKIP, actionLast.SKIP);
                if (tempUrl!= null && host!= null && tempUrl.contains(host) && isUsableURL(tempUrl, url) && !allChildrenUrls.contains(tempUrl) && !childrenUrls.contains(tempUrl)
                        && !tempUrl.endsWith(host) && !tempUrl.endsWith(host + "/")) {
                    
                    // for (int i=0; i<previousUrls.size(); i++){
                    // if
                    // (!previousUrls.get(i).getChildrenUrls().contains(tempUrl)){
                    childrenUrls.add(tempUrl);
                    allChildrenUrls.add(tempUrl);

                    // }
                    // }
                }
            }
            entity.setChildrenUrls(childrenUrls);
            result.add(entity);
            System.out.println("Am adaugat:" + entity.getUrl());

            for (int i = 0; i < childrenUrls.size(); i++) {
                result.addAll(parseInDepth(childrenUrls.get(i), maxDepthLevel, currentDepthLevel));
            }

        }

        return result;
    }


    /**
     * 
     * @param url
     * @return
     */
    private String extractHost(String url) {
        if(url == null){
            return null;
        }
        String host = null;

        int index = url.indexOf("http://");
        if(index == -1){
            return null;
        }
        index += "http://".length();
        url = url.substring(index);

        index = url.indexOf("/");
        if(index != -1){
            url = url.substring(0, index);
        }

        ArrayList<String> entities = split(url, '.');
        int size = entities.size();
        if(size <= 2){
            return url;
        }
        if(entities.get(size - 2).length() >= 4){
            return entities.get(size-2) + "." + entities.get(size-1);
        }
        
        return entities.get(size-2) + "." + entities.get(size-2) + "." + entities.get(size-1);
    }

    private ArrayList<String> split(String target, char delimiter){
        if(target == null){
            return null;
        }
        ArrayList<String> result = new ArrayList<String> ();

        String item = "";
        for (int i = 0; i < target.length(); i++) {
            if (target.charAt(i) == delimiter){
                result.add(item);
                item = "";
                continue;
            }
            item += target.charAt(i);
        }

        result.add(item);

        return result;
    }


    private final static String[] EXTENSION_REJECTION_LIST = new String[]{
        "png",
        "jpg",
        "bmp",
        "js",
        "gif",
        "flw",
        "fla",
        "css",
        "ico",
        "pdf"

    };

    /**
     * 
     * @param url
     * @return
     */
    private boolean isUsableURL(String url, String parent) {
        if(url == null || (!url.startsWith("http://") && !url.startsWith(".") && !url.startsWith("/"))){
            return false;
        }
        if(url.startsWith(".") || url.startsWith("/")){
            URL parentURL;
            try {
                parentURL = new URL(parent);
                url = new URL( parentURL, url).toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            }
        }


        for (int i = 0; i < EXTENSION_REJECTION_LIST.length; i++) {
            if(url.endsWith(EXTENSION_REJECTION_LIST[i])){
                return false;
            }
        }

        return true;
    }

}
