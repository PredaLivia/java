package parsing;

public class Parser implements ParsingMethods {

    public int first = -1;
    public int last = -1;
    protected boolean okMatch = false;
    protected boolean failed = false;
    protected String match = new String();

    public Parser() {
    }

    public int seek(String stringToSeek, String stringSource, StringDirectionMod direction, StringActionMod action) {
        // TODO Auto-generated method stub

        if (direction.mod.equals("forward")) {
            first = stringSource.indexOf(stringToSeek, first);
            if (first != -1) {
                if (action.mod.equals("skip")) {
                    first = first + stringToSeek.length();
                }
            }
        } else if (direction.mod.equals("backward")) {
            first = stringSource.lastIndexOf(stringToSeek, first);
            if (first != -1) {
                if (action.mod.equals("skip")) {
                    first = first + stringToSeek.length();
                }
            }
        }
        return first;
    }

    public int seek(String stringToSeek, StringBuffer stringSource, StringDirectionMod direction, StringActionMod action) {
        // TODO Auto-generated method stub
        if (direction.mod.equals("forward")) {
            first = stringSource.indexOf(stringToSeek, first);
            if (first != -1) {
                if (action.mod.equals("skip")) {
                    first = first + stringToSeek.length();
                }
            }
        } else if (direction.mod.equals("backward")) {
            first = stringSource.lastIndexOf(stringToSeek, first);
            if (first != -1) {

                if (action.mod.equals("skip")) {
                    first = first + stringToSeek.length();
                }
            }
        }
        return first;
    }

    public String extract(String startMatch, String endMatch, StringBuffer content, StringActionMod actionFirst,
            StringActionMod actionLast) {
        match = new String();
        failed = false;
        first = content.indexOf(startMatch, first);
        if (first != -1) {
            if (actionFirst.mod.equals("skip")) {
                first = first + startMatch.length();
            }
            last = content.indexOf(endMatch, first);
            if (last != -1) {
                if (actionLast.mod.equals("nothing")) {
                    last = last + endMatch.length();
                }
                okMatch = true;
                match = content.substring(first, last);
                match = match.trim();
            } else {
                okMatch = false;

            }
        } else {
            failed = true;

        }
        return match;
    }

    public String extract(String startMatch, String endMatch, String content, StringActionMod actionFirst,
            StringActionMod actionLast) {
        match = new String();
        failed = false;
        first = content.indexOf(startMatch, first);
        if (first != -1) {
            if (actionFirst.mod.equals("skip")) {
                first = first + startMatch.length();
            }
            last = content.indexOf(endMatch, first);
            if (last != -1) {
                if (actionLast.mod.equals("nothing")) {
                    last = last + endMatch.length();
                }
                okMatch = true;
                match = content.substring(first, last);
                match = match.trim();
            } else {
                okMatch = false;

            }
        } else {
            failed = true;

        }
        return match;
    }

    public String clearTags(String tags) {

        int begin, end;
        String tag = tags;
        while (tag.indexOf("<") != -1) {
            begin = tag.indexOf("<");
            end = tag.indexOf(">", begin);
            if (end != -1) {
                tags = tag.substring(0, begin) + " " + tag.substring(end + 1, tag.length());
                tag = tags;
            }
        }
        return tags;
    }

    // this method replaces clearEnters, clearWhiteSP and clearDescription
    private String clearString(String tags) {
        tags = tags.replaceAll("\\s", " ");
        return tags;
    }

    private String clearEnters(String tags) {
//        while (tags.indexOf("\\\\n") != -1) {
            tags = tags.replaceAll("\\\\n", "");
//        }
        return tags;
    }

    private String clearNBSP(String tags) {
        tags = tags.replaceAll("&nbsp;", "&");
        tags = tags.trim();
        if (tags.indexOf("&") == 0) {
            tags = tags.replace("&", "").trim();
        }
        return tags;
    }

    private String clearWhiteSP(String tags) {

        tags = tags.replaceAll("  ", " ");
        while (tags.indexOf("  ") != -1) {
            tags = tags.replace("  ", " ").trim();
        }
        return tags;
    }

    private String clearDescription(String tags) {

        tags = tags.replaceAll("    		 	", "");
        tags = tags.replaceAll("    		 ", "");

        return tags;
    }

    public String replaceSpecialChars(String str) {
        str = str.replace("&euro;", "�");
        str = str.replace("acute;", "");
        // str = str.replace("m&sup2;", "mp");
        // str = str.replace("m&sup2", "mp");
        str = str.replace("&quot;", "\"");

        str = str.replace("&ccedil;", "�");
        str = str.replace("&atilde;", "�");

        str = str.replace("&agrave", "�");
        str = str.replace("&aacute;", "�");
        str = str.replace("&otilde;", "�");

        str = str.replace("&nbsp;", " ");
        str = str.replace("&amp;", "&");
        str = str.replace("&#174;", "�");
        str = str.replace("&#039;", "'");
        str = str.replace("&#39;", "'");

        str = str.replace("\u00A0", "");
        str = str.replace("\n", " ");
        str = str.replace("\t", "");
        str = str.replace("   ", " ");
        str = str.replace("  ", " ");

        return str;
    }

    public String replaceAsciCharacters(String text) {
        text = text.replace("\\\"", "\"");
        text = text.replace("\\u003c", "<");
        text = text.replace("\\u003C", "<");
        text = text.replace("\\u003e", ">");
        text = text.replace("\\u003E", ">");
        text = text.replace("\\u2026", "...");
        text = text.replace("\\/", "/");
        text = text.replace("\\u201c", "\"");
        text = text.replace("\\u201C", "\"");
        text = text.replace("\\u201d", "\"");
        text = text.replace("\\u201D", "\"");

        return text;
    }

    private String clearEnd(String tags) {

        if (tags.trim().endsWith(",")) {
            tags = tags.substring(0, tags.lastIndexOf(","));
        }
        return tags;
    }

    public String formatField(String tags) {
        tags = replaceAsciCharacters(tags);
        tags = clearEnters(tags);
        tags = clearWhiteSP(tags);
        // tags = replaceSpecialChars(tags);
        // tags = clearTags(tags);
        // tags = clearNBSP(tags);
        // tags = clearString(tags);
        // tags = clearEnd(tags);
        // tags = clearDescription(tags);

        return tags;
    }

}
